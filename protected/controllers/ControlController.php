<?php

class ControlController extends Controller
{
    /**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}
    public function accessRules()
{
return array(
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('index','produccionRango','recepcion','cocinas','decantacion','prensado','refinacion','tratamiento','presecado','secado','empaque','crearnuevo','recepciones','actualizarvalores','actualizarvalorescocina','Actualizarprensado','actualizarvaloresrefinacion','actualizarvalorestratamiento','actualizarvaloresdecantacion'
,'actualizarvalorespresecado','actualizarvaloressecado','actualizarvaloresharina','agregaregistro','bloquear','idrecepcion','verpdfRango','generarExcelprod','horaActual','addFila','secuencia','getEspecie','eliminarregistro'),
'users'=>array('@'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}
	public function actionIndex()
	{
            if(isset($_GET['fecha']))
                $this->render('index',array('fecha'=>$_GET['fecha']));
            else
                $this->render('index');
	}
        public function actionProduccionRango()
	{//.' 23:59'
            if(isset($_GET['fechaini']) && isset($_GET['fechafin'])&& isset($_GET['horaini'])&& isset($_GET['horafin'])){
                $fechaini=$_GET['fechaini'];
                $fechafin=$_GET['fechafin'];
                $horaini=$_GET['horaini'];
                $horafin=$_GET['horafin'];
            }else{
                $fechaini=date('Y-m-d');
                $fechafin=date('Y-m-d');
                $horafin = '23:59';
                $horaini = '00:00';
                
            }
            $model = new Recepcion('search');
            $model->unsetAttributes();  // clear any default values
            if(isset($_GET['Recepcion']))
                $model->attributes=$_GET['Recepcion'];
            $this->render('Produccion_rango',array('model'=>$model,'f1'=>$fechaini,'f2'=>$fechafin,'horafin'=>$horafin,'horaini'=>$horaini));
	}
        function idrol(){
            return Yii::app()->user->idrol;
        }
        
	public function recepcion($fecha){
            
            $falsa = -1;
            if($fecha=='') {$fecha= date('Y-m-d');}
            $model = Recepcion::model()->findAll("Fecha='".$fecha."' order by Hora, ID");
            $rt = '<table id="tabla_inicio" class="items table table-striped table-bordered table-condensed">';
            $rt = $rt.'<tr>';
            $rt =$rt.'<th>Grupo Horario</th><th>Hora</th><th>Secuencia</th><th>ID Recepción</th><th>Especie</th><th>Peso(T)</th><th>Descuento(T)</th><th>Peso Neto(T)</th><th>TBVN</th><th>Entrada a tolva</th><th>Tolva</th>';
            $rt = $rt.'</tr>';
            foreach($model as $row){
                $status =$row->IdUsuario;
                if ($status == Yii::app()->user->id){ $ban = 1; }  else { $ban =0; }
            if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                $img = 'images/candado.png';
            }else {$img = 'images/candadoabierto.jpeg';}
            $horag = strftime ('%H:00',strtotime($row->Hora));
            $horanum = strftime ('%H',strtotime($row->Hora));
            
            $rt =$rt.'<tr class ="odd" style="font-size:11px;height:7px;">';
            if($horanum != $falsa){
            
            $modelRecep = Recepcion::model()->findAll("Fecha = '".$row->Fecha."' and HOUR(Hora) =".$horanum);
            $c=count($modelRecep);
            $gp = $row->ID;
            $rt =$rt.'<th style="vertical-align:middle;" id="gruphora'.$gp.'" rowspan="'.$c.'" >'.$horag.'</th>';
            $falsa = $horanum;
            }
            $hora=  explode(':', $row->Hora);
            $rt =$rt.'<th ><input onblur="cambiohora('.$row->ID.',this.value);" id="hora'.$row->ID.'"  class="hora horaprincipal" size="4" value="'.$hora[0].'"> : <input id="minutos'.$row->ID.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->ID.'" onclick="horaActual(this,1)" title="Obtener hora actual" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
            $rt =$rt.'<th><select id="secu'.$row->ID.'" onchange="getEspecies('.$row->ID.',this.value)" >'.$this->Secuencia($hora[0]?$hora[0]:'00',$fecha,$row->Secuencia).'</select></th>';
            $rt =$rt.'<th onchange="recepcion(this);" id="idrecep'.$row->ID.'" rel="'.$row->ID.'">'.$row->RecepcionID.'</th>';
            $rt =$rt.'<th id= "especie'.$row->ID.'">'.$row->EspecieID.'</th>';
            $rt =$rt.'<th id="peso'.$row->ID.'">'.(float)$row->PesoTonelada.'</th>';
            $rt =$rt.'<td id="desc'.$row->ID.'" rel="'.$row->ID.'" onchange="descuento(this);" >'.(float)$row->Descuento.'</td>';
            $rt =$rt.'<th id="pesoneto'.$row->ID.'">'.(float)$row->Pesoneto.'</th>';
            $rt =$rt.'<td id="tbvn'.$row->ID.'">'.$row->TBVN.'</td>';
            $hora=  explode(':', $row->Horatolva);
            $rt =$rt.'<th ><input id="horatolva'.$row->ID.'"  class="hora horaprincipal" size="4" value="'.$hora[0].'"> : <input id="minutostolva'.$row->ID.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->ID.'" onclick="horaActualTolva(this)" title="Obtener hora actual" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
	    $rt =$rt.'<th>'.CHtml::dropDownList('Lista',$row->TolvaID,CHtml::listData(Tolva::model()->findAll('Estado=1'),'ID','Nombre'),array('empty'=>'','class'=>'form-control','style'=>"height:18px; font-size:9px;padding:1px;margin:1px;",'id'=>'tolva'.$row->ID,'onchange'=>'this.setAttribute("camb","si");')).'</th>';
            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardar(this)','rel'=>$row->ID,'style'=>'margin:0px;')).'</th>';
            if($this->idrol()==2):
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,1);','rel'=>$row->ID,'style'=>'margin:0px;')).'</th>';
            endif;
            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,1);','rel'=>$row->ID, 'grupo'=>$gp,'style'=>'margin:0px;')).'</th>';
            $rt =$rt.'</tr>';
		}
            $rt =$rt.'</table>';
            if($this->idrol()!=3)
                $rt = $rt.'<br>'.CHtml::link('+ Agregar Medición','javascript:agregar(1,"'.$fecha.'")',array('class'=>'btn btn-success'));
            return $rt;
	}
	public function actionCocinas($fecha){
		$count= 0;
                $model = Cocina::model()->findAll('Estado=1 order by ID');
		$rt = '<table class="items table table-striped table-bordered table-condensed" id="tabla_cocina"><tr style="font-size:11px;">'
                        . '<th style="font-size:11px;" rowspan="4"><center>Hora de Medición</center></th>';
		foreach($model as $row){
                    $count++;$rt =$rt.'<th colspan="4" ><center>'.$count.'</center></th>';
		}
		$rt = $rt.'</tr><tr style="font-size:11px;">';
		for($y = 0 ;$y<$count;$y++){
                    $rt =$rt.'<th rowspan="2"><center>RPM</center></th><th colspan="2" ><center>Presion PSI</center></th><th rowspan="2"><center>T °C</center></th>';
		}
                $rt = $rt.'</tr><tr style="font-size:11px;">';
		for($y = 0 ;$y<$count;$y++){
                    $rt =$rt.'<th><center>Eje</center></th><th><center>Chaq</center></th>';
		}
                $rt = $rt.'</tr>';
                /*Consultas*/
                $result = Coccion::model()->findAll("Fecha='".$fecha."' order by Hora, codigofila, CocinaID");
                $contadortabla=1;
                /*Contenido*/
                $rt = $rt.'</tr><tr style="font-size:11px;">';
		foreach($model as $p){
                    $rt =$rt.'<th><center>'.(float)$p->CoccionMinRmp.' _ '.(float)$p->CoccionMaxRmp.'</center></th><th><center>'.(float)$p->CoccionMinPresionEje.' _ '.(float)$p->CoccionMaxPresionEje.'</center></th><th><center>'.(float)$p->CoccionMinPresionChaqueta.' _ '.(float)$p->CoccionMaxPresionChaqueta.'</center></th><th><center>'.(float)$p->CoccionMinTemperatura.' _ '.(float)$p->CoccionMaxTemperatura.'</center></th>';
		}
                $rt = $rt.'</tr>';
                foreach($result as $row){
                    $p=$model[$contadortabla-1];
                    $status = $row->IdUsuario;
                    if ($status == Yii::app()->user->id){ $ban = 1; }  else { $ban =0; }
                    if ($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                        $img = 'images/candado.png';
                    }else {$img = 'images/candadoabierto.jpeg';}
                    if($contadortabla==1){
                        $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                        $hora=  explode(':', $row->Hora);
                        $rt =$rt.'<th ><input  id="horacocc'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutoscocc'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,2)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';   
                    }
                    $rt =$rt.'<td camb="no" id="rpm'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->RPM,$p->CoccionMinRmp,$p->CoccionMaxRmp,$p->CoccionRpmAlertaNaranja).' >'.(float)$row->RPM.'</td>';
                    $rt =$rt.'<td camb="no" id="presioneje'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PresionEje,$p->CoccionMinPresionEje,$p->CoccionMaxPresionEje,$p->CoccionPresionEjeAlertaNaranja).'>'.(float)$row->PresionEje.'</td>';
                    $rt =$rt.'<td camb="no" id="presionchaqueta'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PresionChaqueta,$p->CoccionMinPresionChaqueta,$p->CoccionMaxPresionChaqueta,$p->CoccionPresionChaquetaAlertaNaranja).'>'.(float)$row->PresionChaqueta.'</td>';
                    $rt =$rt.'<td camb="no" id="temperatura'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Temperatura,$p->CoccionMinTemperatura,$p->CoccionMaxTemperatura,$p->CoccionPresionTemperaturaAlertaNaranja).'>'.(float)$row->Temperatura.'</td>';
                    $contadortabla++;
                    if($contadortabla>$count){
                        $contadortabla=1;
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardarcocinas(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                        if($this->idrol()==2):
                            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,2);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                        endif;
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,2);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                        $rt =$rt.'</tr>';
                    }
                }
		$rt =$rt.'</table>';
                $rt = $rt.'<br>'.CHtml::link('+ Agregar revisión','javascript:agregar(2, "'.$fecha.'")',array('class'=>'btn btn-success'));
		print $rt;
	}
public function actionPrensado($fecha){
	$count= 0;
	$model = Prensa::model()->findAll('Estado=1 order by ID');
	$rt = '<table class="items table table-striped table-bordered table-condensed" id="tabla_prensas"><tr style="font-size:10px;">'
                . '<th style="font-size:11px;" rowspan="3"><center>Hora de Medición</center></th>';
	foreach($model as $row){
	$count++;
	$rt =$rt.'<th colspan="2" ><center>'.$count.'</center></th>';
	}
	$rt =$rt.'<th colspan="2" ><center>LICOR</center></th></tr><tr style="font-size:11px;">';
	for($y = 0 ;$y<$count;$y++){
	$rt =$rt.'<th><center>Amp</center></th><th><center>%H</center></th>';
	}
	$rt = $rt.'<th><center>%S</center></th><th><center>%G</center></th></tr>';
	$result = Prensado::model()->findAll("Fecha ='".$fecha."' order by Hora, codigofila, PrensaID" );
        $sql = "SELECT LicorMinPorcentajeSolidos, LicorMaxPorcentajeSolidos, LicorPorcentajeSolidosAlertaNaranja,LicorMinPorcentajeGrasas, LicorMaxPorcentajeGrasas, LicorPorcentajeGrasasAlertaNaranja from Parametros LIMIT 1";
        $p = Yii::app()->db->createCommand($sql)->queryRow();
	$contadortabla=1;
        $rt = $rt.'<tr style="font-size:11px;">';
	foreach($model as $pp){
	$rt =$rt.'<th><center>'.(float)$pp->PrensadoMinAmperaje.' _ '.(float)$pp->PrensadoMaxAmperaje.'</center></th><th><center>'.(float)$pp->PrensadoMinPorcentajeHumedad.' _ '.(float)$pp->PrensadoMaxPorcentajeHumedad.'</center></th>';
	}
	$rt =$rt.'<th><center>'.(float)$p['LicorMinPorcentajeSolidos'].' _ '.(float)$p['LicorMaxPorcentajeSolidos'].'</center></th><th><center>'.(float)$p['LicorMinPorcentajeGrasas'].' _'.(float)$p['LicorMaxPorcentajeGrasas'].'</center></th>';
	$rt = $rt.'</tr>';
	foreach($result as $row){
        $pp=$model[$contadortabla-1];
            $status = $row->IdUsuario;
            if ($status == Yii::app()->user->id){$ban = 1; }  else {$ban =0;}
            if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                $img = 'images/candado.png';
            }else {$img = 'images/candadoabierto.jpeg';}
		if($contadortabla==1){
                	$rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                        $hora=  explode(':', $row->Hora);
                        $rt =$rt.'<th ><input  id="horaprensa'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutosprensa'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,4)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
		}
	$rt =$rt.'<td id="preAmp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Amperaje, $pp->PrensadoMinAmperaje, $pp->PrensadoMaxAmperaje, $pp->PrensadoAmperajeAlertaNaranja).' >'.(float)$row->Amperaje.'</td>';
	$rt =$rt.'<td id="preHum'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PorcentajeHumedad,$pp->PrensadoMinPorcentajeHumedad, $pp->PrensadoMaxPorcentajeHumedad, $pp->PrensadoPorcentajeHumedadAlertaNaranja).' >'.(float)$row->PorcentajeHumedad.'</td>';
	$contadortabla++;
	if($contadortabla>$count){
		$contadortabla=1;
                $licor = Licor::model()->find('codigofila ='.$row->codigofila);
		$rt =$rt.'<td id="preLicH'.$row->codigofila.'" '.$this->clases($licor->LicorPorcentajeSolidos, $p['LicorMinPorcentajeSolidos'], $p['LicorMaxPorcentajeSolidos'], $p['LicorPorcentajeSolidosAlertaNaranja']).' >'.(float)$licor->LicorPorcentajeSolidos.'</td>';
		$rt =$rt.'<td id="preLicG'.$row->codigofila.'" '.$this->clases($licor->LicorPorcentajeGrasas, $p['LicorMinPorcentajeGrasas'], $p['LicorMaxPorcentajeGrasas'], $p['LicorPorcentajeGrasasAlertaNaranja']).' >'.(float)$licor->LicorPorcentajeGrasas.'</td>';
		$rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Bloquear', array ('onclick'=>'guardarprensado(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                if($this->idrol()==2):
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Actualizar', array ('onclick'=>'bloquear(this,3);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                endif;
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,3);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                $rt =$rt.'</tr>';
        }
	}
	$rt =$rt.'</table>';
        $rt = $rt.'<br>'.CHtml::link('+ Agregar revisión','javascript:agregar(3, "'.$fecha.'")',array('class'=>'btn btn-success'));
	print $rt;
}

public function actionDecantacion($fecha){
	$count= 0;
	$model =  Tricanter::model()->findAll('Estado=1 order by ID');
	$rt = '<table class="items table table-striped table-bordered table-condensed" id="tabla_decantasion"><tr style="font-size:11px;">'
               .'<th style="font-size:11px;" rowspan="4"><center>Hora de Medición</center></th>';
	foreach($model as $row){
            $count++;
            $rt =$rt.'<th colspan="4" ><center>'.$count.'</center></th>';
	}
        $rt = $rt.'</tr><tr>';
        for($y = 0 ;$y<$count;$y++){
            $rt =$rt.'<th colspan="2" ><center>ALIM</center></th><th colspan="2" ><center>Agua de cola</center></th>';
	}
	$rt = $rt.'</tr>';
	$rt = $rt.'<tr style="font-size:11px;">';
	for($y = 0 ;$y<$count;$y++){
            $rt =$rt.'<th><center>T °C</center></th><th ><center>%H</center></th><th><center>%S</center></th><th><center>%G</center></th>';
	}
        $result = Decantacion::model()->findAll("Fecha='".$fecha."' order by Hora , codigofila, TricanterID");
	$contadortabla=1;
        $rt = $rt.'</tr><tr style="font-size:11px;">';
	foreach($model as $p){
            $rt =$rt.'<th><center>'.(float)$p->DecantacionMinAlimentacionTemperatura.' _ '.(float)$p->DecantacionMaxAlimentacionTemperatura.'</center></th><th><center>'.(float)$p->DecantacionMinAlimentacionPorcentajeHumedad.' _ '.(float)$p->DecantacionMaxAlimentacionPorcentajeHumedad.'</center></th><th><center>'.(float)$p->DecantacionMinAguaColaPorcentajeSolidos.' _ '.(float)$p->DecantacionMaxAguaColaPorcentajeSolidos.'</center></th><th><center>'.(float)$p->DecantacionMinAguaColaPorcentajeGrasas.' _ '.(float)$p->DecantacionMaxAguaColaPorcentajeGrasas.'</center></th>';
        }
        $rt = $rt.'</tr>';
	foreach($result as $row){
            $p = $model[$contadortabla-1];
            $status = $row->IdUsuario;
            if ($status == Yii::app()->user->id){ $ban = 1; }  else {$ban =0; }
            if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                $img = 'images/candado.png';
            }else {$img = 'images/candadoabierto.jpeg';}
            if($contadortabla==1){
		$rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                $hora=  explode(':', $row->Hora);
                $rt =$rt.'<th ><input  id="horadeca'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutosdeca'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,3)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
            }
            $rt =$rt.'<td id="Dtem'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AlimentacionTemperatura,$p->DecantacionMinAlimentacionTemperatura,$p->DecantacionMaxAlimentacionTemperatura,$p->DecantacionAlimentacionTemperaturaAlertaNaranja).' >'.(float)$row->AlimentacionTemperatura.'</td>';
            $rt =$rt.'<td id="Dhum'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AlimentacionPorcentajeHumedad, $p->DecantacionMinAlimentacionPorcentajeHumedad, $p->DecantacionMaxAlimentacionPorcentajeHumedad, $p->DecantacionAlimentacionPorcentajeHumedadAlertaNaranja).' >'.(float)$row->AlimentacionPorcentajeHumedad.'</td>';
            $rt =$rt.'<td id="Dsol'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AguaColaPorcentajeSolidos,$p->DecantacionMinAguaColaPorcentajeSolidos, $p->DecantacionMaxAguaColaPorcentajeSolidos, $p->DecantacionAguaColaPorcentajeSolidosAlertaNaranja).' >'.(float)$row->AguaColaPorcentajeSolidos.'</td>';
            $rt =$rt.'<td id="Dgra'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AguaColaPorcentajeGrasas, $p->DecantacionMinAguaColaPorcentajeGrasas, $p->DecantacionMaxAguaColaPorcentajeGrasas, $p->DecantacionAguaColaPorcentajeGrasasAlertaNaranja).' >'.(float)$row->AguaColaPorcentajeGrasas.'</td>';
            $contadortabla++;
            if($contadortabla>$count){
                $contadortabla=1;
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardardecantacion(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                if($this->idrol()==2):
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,4);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                endif;
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,4);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                $rt =$rt.'</tr>';
            }
	}
        $rt =$rt.'</table>';
        $rt = $rt.'<br>'.CHtml::link('+ Agregar revisión','javascript:agregar(4, "'.$fecha.'")',array('class'=>'btn btn-success'));
	print $rt;
}

public function actionRefinacion($fecha){
        $count= 0;
	$model = Pulidora::model()->findAll('Estado = 1 order by ID');
        $rt = '<table class="items table table-striped table-bordered table-condensed" id="tabla_refinacion">';
	$rt = $rt.'<tr style="font-size:11px;">'
                . '<th style="font-size:11px;" rowspan="4"><center>Hora de Medición</center></th>';
	foreach($model as $row){
            $count++;
            $rt =$rt.'<th colspan="4" ><center>'.$count.'</center></th>';
	}
        $rt = $rt.'<tr style="font-size:11px;">';
	for($y = 0 ;$y<$count;$y++){
	$rt =$rt.'<th colspan="1" ><center>ALIM</center></th><th colspan="3" ><center>ACEITE</center></th>';
	}
        $rt = $rt.'</tr>';
	$rt = $rt.'<tr style="font-size:11px;">';
	for($y = 0 ;$y<$count;$y++){
	$rt =$rt.'<th><center>T °C</center></th><th ><center>%H</center></th><th><center>%S</center></th><th><center>%A</center></th>';
	}
        $rt = $rt.'</tr>';
        $result = Refinacion::model()->findAll("Fecha='".$fecha."' order by Hora , codigofila, PulidoraID");
	$contadortabla=1;
        $rt = $rt.'<tr style="font-size:11px;">';
	foreach($model as $p){
            $rt =$rt.'<th><center>'.(float)$p->RefinacionMinAlimentacionTemperatura.' _ '.(float)$p->RefinacionMaxAlimentacionTemperatura.'</center></th><th ><center>'.(float)$p->RefinacionMinAceitePorcentajeHumedad.'_'.(float)$p->RefinacionMaxAceitePorcentajeHumedad.'</center></th><th><center>'.(float)$p->RefinacionMinAceitePorcentajeSolidos.' _ '.(float)$p->RefinacionMaxAceitePorcentajeSolidos.'</center></th><th><center>'.(float) $p->RefinacionMinAceitePorcentajeAcidez.' _'.(float)$p->RefinacionMaxAceitePorcentajeAcidez.'</center></th>';
	}
        $rt = $rt.'</tr>';
	foreach($result as $row){
            $status = $row->IdUsuario;
            $p= $model[$contadortabla-1];
            if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
            if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                $img = 'images/candado.png';
            }else {$img = 'images/candadoabierto.jpeg';}
            if($contadortabla==1){
                $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                $hora=  explode(':', $row->Hora);                   
                $rt =$rt.'<th ><input  id="horarefi'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutosrefi'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,5)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';                        
            }
            $rt =$rt.'<td id="retem'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AlimentacionTemperatura, $p->RefinacionMinAlimentacionTemperatura, $p->RefinacionMaxAlimentacionTemperatura, $p->RefinacionTemperaturaAlertaNaranja).' >'.(float)$row->AlimentacionTemperatura.'</td>';
            $rt =$rt.'<td id="rehum'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AceitePorcentajeHumedad, $p->RefinacionMinAceitePorcentajeHumedad, $p->RefinacionMaxAceitePorcentajeHumedad, $p->RefinacionAceitePorcentajeHumedadAlertaNaranja).' >'.(float)$row->AceitePorcentajeHumedad.'</td>';
            $rt =$rt.'<td id="resol'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AceitePorcentajeSolidos, $p->RefinacionMinAceitePorcentajeSolidos, $p->RefinacionMaxAceitePorcentajeSolidos, $p->RefinacionAceitePorcentajeSolidosAlertaNaranja).' >'.(float)$row->AceitePorcentajeSolidos.'</td>';
            $rt =$rt.'<td id="reAcid'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AceitePorcentajeAcidez, $p->RefinacionMinAceitePorcentajeAcidez, $p->RefinacionMaxAceitePorcentajeAcidez, $p->RefinacionAceitePorcentajeAcidezAlertaNaranja).' >'.(float)$row->AceitePorcentajeAcidez.'</td>';
            $contadortabla++;
            if($contadortabla>$count){
                $contadortabla=1;
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardarrefinacion(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                if($this->idrol()==2):
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,5);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                endif;
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,5);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                $rt =$rt.'</tr>';
            }
	}
	$rt =$rt.'</table>';
        $rt = $rt.'<br>'.CHtml::link('+ Agregar revisión','javascript:agregar(5, "'.$fecha.'")',array('class'=>'btn btn-success'));
	print $rt;
}

public function actionTratamiento($fecha){
        $count= 0;
	$model = Planta::model()->findAll('Estado=1 order by ID');
	$rt = '<table class="items table table-striped table-bordered table-condensed" id="tabla_tratamiento"><tr style="font-size:11px;">'
                . '<th style="font-size:11px;" rowspan="4"><center>Hora de Medición</center></th>';
	foreach($model as $row){
            $count++;
            $rt =$rt.'<th colspan="8" ><center>'.$count.'</center></th>';
	}
        $rt = $rt.'<tr style="font-size:11px;">';
	for($y = 0 ;$y<$count;$y++){
		$rt =$rt.'<th colspan="2" ><center>ALIM A.Cola</center></th><th colspan="1" ><center>VAH</center></th><th colspan="1" ><center>Presión</center></th><th colspan="1" ><center>1°E</center></th><th colspan="1" ><center>2°E</center></th><th colspan="1" ><center>3°E</center></th><th colspan="1" ><center>CON</center></th>';
	}
	$rt = $rt.'</tr>';
	$rt = $rt.'<tr style="font-size:11px;">';
	for($y = 0 ;$y<$count;$y++){
		$rt =$rt.'<th><center>T °C</center></th><th ><center>%S</center></th><th><center>T°C</center></th><th><center>p</center></th><th><center>T °C</center></th><th ><center>__</center></th><th><center>T°C</center></th><th><center>%S</center></th>';
	}
    $rt = $rt.'</tr>';
    $result = Tratamiento::model()->findAll("Fecha='".$fecha."' order by Hora, codigofila, PlantaID ");
    $contadortabla=1;
    $rt = $rt.'<tr style="font-size:11px;">';
    foreach($model as $p){
	$rt =$rt.'<th><center>'.(float)$p->TratamientoMinAlimentacionAguaColaTemperatura.' _ '.(float)$p->TratamientoMaxAlimentacionAguaColaTemperatura.'</center></th><th ><center>'.(float)$p->TratamientoMinAlimentacionAguaColaPorcentajeSolidos.' _ '.(float)$p->TratamientoMaxAlimentacionAguaColaPorcentajeSolidos.'</center></th><th><center>'.(float)$p->TratamientoMinVahTemperatura.' _ '.(float)$p->TratamientoMaxVahTemperatura.'</center></th><th><center>'.(float)$p->TratamientoMinVacPeso.' _ '.(float)$p->TratamientoMaxVacPeso.'</center></th><th><center>'.(float)$p->TratamientoMinTemperatura1.' _ '.(float)$p->TratamientoMaxTemperatura1.'</center></th><th ><center>'.(float)$p->TratamientoMinTemperatura2.' _ '.(float)$p->TratamientoMaxTemperatura2.'</center></th><th><center>'.(float)$p->TratamientoMinTemperatura3.' _ '.(float)$p->TratamientoMaxTemperatura3.'</center></th><th><center>'.(float)$p->TratamientoMinSalidaPorcentajeSolidos.' _ '.(float)$p->TratamientoMaxSalidaPorcentajeSolidos.'</center></th>';
    }
    $rt = $rt.'</tr>';
	foreach($result as $row){
            $p= $model[$contadortabla-1];
            $status = $row->IdUsuario;
            if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
            if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                $img = 'images/candado.png';
            }else {$img = 'images/candadoabierto.jpeg';}
            if($contadortabla==1){
            	$rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                $hora=  explode(':', $row->Hora);                        
                $rt =$rt.'<th ><input  id="horatrata'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutostrata'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,6)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';   
            }
		$rt =$rt.'<td id="tratemp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AlimentacionAguaColaTemperatura,$p->TratamientoMinAlimentacionAguaColaTemperatura,$p->TratamientoMaxAlimentacionAguaColaTemperatura,$p->TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja).' >'.(float)$row->AlimentacionAguaColaTemperatura.'</td>';
		$rt =$rt.'<td id="trasoli'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AlimentacionAguaColaPorcentajeSolidos,$p->TratamientoMinAlimentacionAguaColaPorcentajeSolidos,$p->TratamientoMaxAlimentacionAguaColaPorcentajeSolidos,$p->TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja).' >'.(float)$row->AlimentacionAguaColaPorcentajeSolidos.'</td>';
		$rt =$rt.'<td id="tratemp2'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->VahTemperatura,$p->TratamientoMinVahTemperatura,$p->TratamientoMaxVahTemperatura,$p->TratamientoVahTemperaturaAlertaNaranja).' >'.(float)$row->VahTemperatura.'</td>';
		$rt =$rt.'<td id="trapre'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->VacPeso,$p->TratamientoMinVacPeso,$p->TratamientoMaxVacPeso,$p->TratamientoVacPesoAlertaNaranja).' >'.(float)$row->VacPeso.'</td>';
                $rt =$rt.'<td id="tratemp3'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Temperatura1,$p->TratamientoMinTemperatura1,$p->TratamientoMaxTemperatura1,$p->TratamientoTemperatura1AlertaNaranja).' >'.(float)$row->Temperatura1.'</td>';
		$rt =$rt.'<td id="tratemp4'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Temperatura2,$p->TratamientoMinTemperatura2,$p->TratamientoMaxTemperatura2,$p->TratamientoTemperatura2AlertaNaranja).' >'.(float)$row->Temperatura2.'</td>';
		$rt =$rt.'<td id="tratemp5'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Temperatura3,$p->TratamientoMinTemperatura3,$p->TratamientoMaxTemperatura3,$p->TratamientoTemperatura3AlertaNaranja).' >'.(float)$row->Temperatura3.'</td>';
		$rt =$rt.'<td id="trasoli2'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->SalidaPorcentajeSolidos,$p->TratamientoMinSalidaPorcentajeSolidos,$p->TratamientoMaxSalidaPorcentajeSolidos,$p->TratamientoSalidaPorcentajeSolidosAlertaNaranja).' >'.(float)$row->SalidaPorcentajeSolidos.'</td>';
		$contadortabla++;
		if($contadortabla>$count){
                    $contadortabla=1;
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardartratamiento(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                    if($this->idrol()==2):
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,6);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                    endif;
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,6);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                    $rt =$rt.'</tr>';
		}
	}
	$rt =$rt.'</table>';
        $rt = $rt.'<br>'.CHtml::link('+ Agregar revisión','javascript:agregar(6, "'.$fecha.'")',array('class'=>'btn btn-success'));
	print $rt;
}
public function actionPresecado($fecha){
	$count= 0;
	$model = Rotadisk::model()->findAll('Estado=1 order by ID');
        /*Comienzo de tabla*/
        $rt = '<table class="items table table-striped table-bordered table-condensed" id="tabla_presecado">';
	/*FILA 1*/
        $rt = $rt.'<tr style="font-size:11px;">'
                . '<th style="font-size:11px;" rowspan="4"><center>Hora de Medición</center></th>';
	foreach($model as $row){
            $count++;
            $rt =$rt.'<th ><center>ALIM MIN</center></th>';/*Columna Alimentacion*/
            $rt =$rt.'<th ><center>ALIM MAX</center></th>';/*Columna Alimentacion*/
            $rt =$rt.'<th colspan="5" ><center>'.$count.'</center></th>';
	}
        $rt = $rt.'</tr>';
        /*FILA 2*/
        $rt = $rt.'<tr style="font-size:11px;">';
	for($y = 0 ;$y<$count;$y++){
            $rt =$rt.'<th rowspan="2" ><center>% H</center></th>';/*Columna Alimentacion*/
            $rt =$rt.'<th rowspan="2"><center>Presión</center></th>';/*Columna Alimentacion*/
            $rt =$rt.'<th rowspan="2"><center>Amp</center></th><th colspan="2"><center>Presión PSI</center></th><th rowspan="2" ><center>Temperatura</center></th><th rowspan="2" ><center>%H</center></th>';
	}
	$rt = $rt.'</tr>';
        /*FILA 3*/
	$rt = $rt.'<tr style="font-size:11px;">';
/*Columnas Alimentación*/
	for($y = 0 ;$y<$count;$y++){
	$rt =$rt.'<th><center>EJE</center></th><th><center>CHQ</center></th>';
	}
	$rt = $rt.'</tr>';
        /*Contenido de la tabla*/
        $result = Presecado::model()->findAll("Fecha='".$fecha."' order by Hora , codigofila, RotadiskID");
	/*Consulta de parametros de validación*/
	$contadortabla=1;
        $rt = $rt.'<tr style="font-size:11px;">';
	foreach($model as $p){
            $rt = $rt.'<th><center>'.(float)$p->PresecadoMinMinimoPorcentajeHumedad.' _ '.(float)$p->PresecadoMaxMinimoPorcentajeHumedad.'</center></th><th ><center>'.(float)$p->PresecadoMinMaximoPresion.' _ '.(float)$p->PresecadoMaxMaximoPresion.'</center></th>';/*Columnas Alimentación*/
            $rt =$rt.'<th><center>'.(float)$p->PresecadoMinAmperaje.' _ '.(float)$p->PresecadoMaxAmperaje.'</center></th><th><center>'.(float)$p->PresecadoMinPresionEje.' _ '.(float)$p->PresecadoMaxPresionEje.'</center></th><th><center>'.(float)$p->PresecadoMinPresionChaqueta.' _ '.(float)$p->PresecadoMaxPresionChaqueta.'</center></th><th ><center>'.(float)$p->PresecadoMinSCR.' _ '.(float)$p->PresecadoMaxSCR.'</center></th><th><center>'.(float)$p->PresecadoMinPorcentajeHumedad.' _ '.(float)$p->PresecadoMaxPorcentajeHumedad.'</center></th>';
	}
	$rt = $rt.'</tr>';
	foreach($result as $row){
            $p = $model[$contadortabla-1];
            $status = $row->IdUsuario;
            if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
        if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
        $img = 'images/candado.png';
        }else {$img = 'images/candadoabierto.jpeg';}
		if($contadortabla==1){
                    $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                    $hora=  explode(':', $row->Hora);
                    $rt =$rt.'<th ><input  id="horaprese'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutosprese'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,7)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
                }
                $rt =$rt.'<td id="preseAlimMin'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->MinimoPorcentajeHumedad,$p->PresecadoMinMinimoPorcentajeHumedad,$p->PresecadoMaxMinimoPorcentajeHumedad,$p->PresecadoMinimoPorcentajeHumedadAlertaNaranja).' >'.(float)$row->MinimoPorcentajeHumedad.'</td>';/*Columna Alimentacion*/
                $rt =$rt.'<td id="preseAlimMax'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->MaximoPresion,$p->PresecadoMinMaximoPresion,$p->PresecadoMaxMaximoPresion,$p->PresecadoMaximoPresionAlertaNaranja).' >'.(float)$row->MaximoPresion.'</td>';/*Columna Alimentacion*/
                $rt =$rt.'<td id="preseAmp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Amperaje,$p->PresecadoMinAmperaje,$p->PresecadoMaxAmperaje,$p->PresecadoAmperajeAlertaNaranja).' >'.(float)$row->Amperaje.'</td>';
                $rt =$rt.'<td id="presePreEje'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PresionEje,$p->PresecadoMinPresionEje,$p->PresecadoMaxPresionEje,$p->PresecadoPresionEjeAlertaNaranja).' >'.(float)$row->PresionEje.'</td>';
                $rt =$rt.'<td id="presePreChaq'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PresionChaqueta,$p->PresecadoMinPresionChaqueta,$p->PresecadoMaxPresionChaqueta,$p->PresecadoPresionChaquetaAlertaNaranja).' >'.(float)$row->PresionChaqueta.'</td>';
                $rt =$rt.'<td id="preseTemp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->SCR,$p->PresecadoMinSCR,$p->PresecadoMaxSCR,$p->PresecadoSCRAlertaNaranja).' >'.(float)$row->SCR.'</td>';
                $rt =$rt.'<td id="preseHum'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PorcentajeHumedad,$p->PresecadoMinPorcentajeHumedad,$p->PresecadoMaxPorcentajeHumedad,$p->PresecadoPorcentajeHumedadAlertaNaranja).' >'.(float)$row->PorcentajeHumedad.'</td>';
                $contadortabla++;
                if($contadortabla>$count){
                        $contadortabla=1;
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardarpresecado(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                        if($this->idrol()==2):
                            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,7);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                        endif;
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,7);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                        $rt =$rt.'</tr>';
                }
	}
	$rt =$rt.'</table>';
        $rt = $rt.'<br>'.CHtml::link('+ Agregar revisión','javascript:agregar(7, "'.$fecha.'")',array('class'=>'btn btn-success'));
	print $rt;
}

public function actionSecado($fecha){
	$count= 0;
	$model = Rotatubo::model()->findAll('Estado = 1 order by ID');
        /*Comienzo de tabla*/
        $rt = '<table class="items table table-striped table-bordered table-condensed" id="tabla_secado">';
	/*FILA 1*/
        $rt = $rt.'<tr style="font-size:11px;">'
        . '<th style="font-size:11px;" rowspan="3"><center>Hora de Medición</center></th>';
	foreach($model as $row){
            $count++;
            $rt =$rt.'<th colspan="4" ><center>'.$count.'</center></th>';
	}
        $rt = $rt.'</tr>';
        /*FILA 2*/
        $rt = $rt.'<tr style="font-size:11px;">';
	for($y = 0 ;$y<$count;$y++){
            $rt =$rt.'<th ><center>Amp</center></th><th colspan="1"><center>Presión PSI</center></th><th colspan="1" ><center>Temperatura</center></th><th colspan="1" ><center>%H</center></th>';
	}
	$rt = $rt.'</tr>';

        /*Contenido de la tabla*/
	$result = Secado::model()->findAll("Fecha='".$fecha."' order by Hora,codigofila, RotatuboID");
	$contadortabla=1;
        /*FILA 3*/
	$rt = $rt.'<tr style="font-size:11px;">';
	foreach($model as $p){
            $rt =$rt.'<th><center>'.(float)$p->RotatuboMinAmperaje.' _ '.(float)$p->RotatuboMaxAmperaje.'</center></th><th><center>'.(float)$p->RotatuboMinPresionEje.' _ '.(float)$p->RotatuboMaxPresionEje.'</center></th><th><center>'.(float)$p->RotatuboMinSRC.' _ '.(float)$p->RotatuboMaxSRC.'</center></th><th ><center>'.(float)$p->RotatuboMinHumedad.' _ '.(float)$p->RotatuboMaxHumedad.'</center></th>';
	}
	$rt = $rt.'</tr>';
	foreach($result as $row){
            $p = $model[$contadortabla-1];
            $status = $row->IdUsuario;
            if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
            if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                $img = 'images/candado.png';
            }else {$img = 'images/candadoabierto.jpeg';}
            if($contadortabla==1){
                 $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                 $hora=  explode(':', $row->Hora);
                 $rt =$rt.'<th ><input  id="horasecado'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutossecado'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,8)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
            }
                $rt =$rt.'<td id="seAmp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Amperaje,$p->RotatuboMinAmperaje,$p->RotatuboMaxAmperaje,$p->RotatuboAmperajeAlertaNaranja).' >'.(float)$row->Amperaje.'</td>';
                $rt =$rt.'<td id="sePreEje'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PresionEje,$p->RotatuboMinPresionEje,$p->RotatuboMaxPresionEje,$p->RotatuboPresionEjeAlertaNaranja).' >'.(float)$row->PresionEje.'</td>';
                $rt =$rt.'<td id="seTemp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->SRC,$p->RotatuboMinSRC,$p->RotatuboMaxSRC,$p->RotatuboSRCAlertaNaranja).' >'.(float)$row->SRC.'</td>';
                $rt =$rt.'<td id="seHum'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PorcentajeHumedad,$p->RotatuboMinHumedad,$p->RotatuboMaxHumedad,$p->RotatuboHumedadAlertaNaranja).' >'.(float)$row->PorcentajeHumedad.'</td>';
                $contadortabla++;
                if($contadortabla>$count){
                        $contadortabla=1;
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardarsecado(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                        if($this->idrol()==2):
                            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,8);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                        endif;
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,8);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                        $rt =$rt.'</tr>';
                }
	}
	$rt =$rt.'</table>';
        $rt = $rt.'<br>'.CHtml::link('+ Agregar revisión','javascript:agregar(8, "'.$fecha.'")',array('class'=>'btn btn-success'));
	print $rt;
}
public function actionEmpaque($fecha){
	$result = Harina::model()->findAll("Fecha='".$fecha."' order by Hora , ID");
    	$rt = '<table id="tabla_fin" class="items table table-striped table-bordered table-condensed">';
        /*Fila 1*/
        $rt = $rt.'<tr style="font-size:10px;">'
                . '<th style="font-size:11px;" rowspan="3"><center>Hora de Medición</center></th>';
	$rt =$rt.'<th colspan=9><center>HARINA</center></th><th colspan="1">OBSERVACIONES</th>';
	$rt = $rt.'</tr>';
        /*Fila 2*/
        $rt = $rt.'<tr style="font-size:10px;">';
	$rt =$rt.'<th >Temp</th><th >%H</th><th >A/O</th><th >Peso</th><th >TBVN</th><th >Prot</th><th ># de Lote</th><th rowspan="2" ><center>Sacos</center></th><th >Clasif</th>';
	$rt =$rt.'<th rowspan="2" >Observación</th>';
        $rt = $rt.'</tr>';
        /*Fila 3*/
        $rt = $rt.'<tr style="font-size:10px;">';
	$rt =$rt.'<th ><center>°C</center></th><th ><center>_</center></th><th ><center>ppm</center></th><th></th><th><center>mg/100g</center></th><th ><center>%</center></th><th ></th><th ></th>';
	$rt = $rt.'</tr>';
        /*CONTENIDO DE LA TABLA*/
        foreach($result as $row){
            $status = $row->IdUsuario;
            if ($status == Yii::app()->user->id){$ban = 1; }  else { $ban =0;  }
            if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                $img = 'images/candado.png';
            }else {$img = 'images/candadoabierto.jpeg';}
            $rt = $rt.'<tr class ="odd" style="font-size:10px;">';
            $hora=  explode(':', $row->Hora);
            $rt =$rt.'<th ><input  id="horahari'.$row->ID.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutoshari'.$row->ID.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->ID.'" onclick="horaActual(this,9)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
            $rt =$rt.'<td id="hariTem'.$row->ID.'" >'.(float)$row->Temperatura.'</td>';
            $rt =$rt.'<td id="hariPorH'.$row->ID.'" >'.(float)$row->PorcentajeHumedad.'</td>';
            $rt =$rt.'<td id="hariAO'.$row->ID.'" >'.(float)$row->AO.'</td>';
            $rt =$rt.'<td id="hariPeso'.$row->ID.'" >'.(float)$row->Peso.'</td>';
            $rt =$rt.'<td id="hariTBVN'.$row->ID.'" >'.(float)$row->TBVN.'</td>';
            $rt =$rt.'<td id="hariPorP'.$row->ID.'" >'.(float)$row->PorcentajeProteina.'</td>';
            $rt =$rt.'<td id="hariNumLot'.$row->ID.'" >'.(float)$row->NumeroLote.'</td>';
            $rt =$rt.'<td id="hariSacos'.$row->ID.'" >'.(float)$row->SacoProducidos.'</td>';
            $rt =$rt.'<th>'.CHtml::dropDownList('Lista',$row->ClasificacionID,CHtml::listData(Clasificacion::model()->findAll('Estado=1'),'ID','Nombre'),array('empty'=>' ','class'=>'form-control','style'=>"height:25px; font-size:12px;padding:4px;",'id'=>'hariClasif'.$row->ID,'onchange'=>'this.setAttribute("camb","si");')).'</th>';
            $rt =$rt.'<th>'.CHtml::dropDownList('Lista',$row->ObservacionID,CHtml::listData(Observacion::model()->findAll('Estado=1'),'ID','Descripcion'),array('empty'=>'','class'=>'form-control','style'=>"height:25px; font-size:12px;padding:4px;",'id'=>'hariObser'.$row->ID,'onchange'=>'this.setAttribute("camb","si");')).'</th>';
            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardarharina(this)','rel'=>$row->ID,'style'=>'margin:0px;')).'</th>';
            if($this->idrol()==2):
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,9);','rel'=>$row->ID,'style'=>'margin:px;')).'</th>';
            endif;
            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,9);','rel'=>$row->ID,'style'=>'margin:0px;')).'</th>';
            $rt = $rt.'</tr>';
        }
        /*FIN DE LA TABLA*/
        $rt =$rt.'</table>';
        $rt = $rt.'<tr>';
        $rt = $rt.'<br>'.CHtml::link('+ Agregar revisión','javascript:agregar(9, "'.$fecha.'")',array('class'=>'btn btn-success'));
	print $rt;

}
public function actionRecepciones(){
    print $this->recepcion($_GET['fecha']);
}
public function actionActualizarvalores(){
    $retorno = 0;
    $tabla = "Recepcion";
    $campo ="ID";
    $id=$_GET['id'];
    $tolva=$_GET['tolva']=='nulo' ? 'NULL':$_GET['tolva'];
    $peso=$_GET['peso']=='nulo' ? 'NULL':$_GET['peso'];
    $descuento=$_GET['desc']=='nulo' ? 'NULL':$_GET['desc'];
    $pesoneto=$_GET['pesoneto']=='nulo' ? 'NULL':$_GET['pesoneto'];
    $especie=$_GET['especie']=='nulo' ? 'NULL':"'".$_GET['especie']."'";
    $tbvn=$_GET['tbvn']=='nulo' ? 'NULL':$_GET['tbvn'];
    $recepcion=$_GET['recepcion']=='nulo' ? 'NULL':$_GET['recepcion'];
    $secuencia=$_GET['secuencia']=='nulo' ? 'NULL':$_GET['secuencia'];
    $hora=$_GET['hora']=='nulo' ? '00:00':$_GET['hora'];
    $horatolva=$_GET['horatolva']=='nulo' ? '00:00':$_GET['horatolva'];
    $status =Yii::app()->db->createCommand('select IdUsuario from Recepcion where ID ='.$id)->queryScalar();
    $rol =$this->idrol();
    if ($status == Yii::app()->user->id){ $ban = 1;}  else {$ban =0; }
    if($status and $rol==2 and $ban==0){
       $retorno = 2;
    }else{
        $sql = "update Recepcion set Secuencia=".$secuencia." ,EspecieID=".$especie." ,Hora = '".$hora."',Horatolva = '".$horatolva."', TolvaID=".$tolva.",TBVN=".$tbvn.",PesoTonelada=".$peso.", Descuento=".$descuento.", Pesoneto=".$pesoneto.", RecepcionID=".$recepcion." where ID=".$id.";";
        Yii::app()->db->createCommand("call FechaRealMod('".$tabla."',".$id.",'".$campo."');")->execute();
        $r=Yii::app()->db->createCommand($sql)->execute();
        if($r>0) {$retorno = 1;}
    }
    print $retorno;
}
public function actionActualizarvalorescocina(){
    $sql= "select ID as CocinaID from Cocina where Estado=1 order by CocinaID;";
    $tabla = "Coccion";
    $campo="codigofila";
    $Cocinas = Yii::app()->db->createCommand($sql)->queryColumn();
    $rpms = $_GET["rpms"];
    $ejes = $_GET["ejes"];
    $chaqs = $_GET["chaqs"];
    $temps = $_GET["temperaturas"];
    $total = $_GET["total"];
    $revision = $_GET['revision'];
    $hora=$_GET['hora']=='nulo' ? '00:00':$_GET['hora'];
    $count=0;
    $status =Yii::app()->db->createCommand('select IdUsuario from Coccion where codigofila ='.$revision)->queryScalar();
    $rol =$this->idrol();
    if ($status == Yii::app()->user->id){ $ban = 1; }  else { $ban =0; }
    if($status and $rol!=1 and $ban==0){
       $count = 2;
    }else{
        for($i=0;$i<$total;$i++){
            $rpm = $rpms[$i]=='nulo'?'NULL':$rpms[$i];
            $eje = $ejes[$i]=='nulo'?'NULL':$ejes[$i];
            $chaq = $chaqs[$i]=='nulo'?'NULL':$chaqs[$i];
            $tem = $temps[$i]=='nulo'?'NULL':$temps[$i];
            $sql='update Coccion set RPM='.$rpm.',PresionEje='.$eje.',PresionChaqueta='.$chaq.',Temperatura='.$tem.',Hora="'.$hora.'" where CocinaID='.$Cocinas[$i].' and codigofila='.$revision;
            Yii::app()->db->createCommand("call FechaRealMod('".$tabla."',".$revision.",'".$campo."');")->execute();
            $count =$count +Yii::app()->db->createCommand($sql)->execute();
        }
        if($count>0){
            $count=1;
        }
    }
    echo $count;
}
public function actionActualizarprensado(){
        $campo="codigofila";	
        $tabla = "Prensado";
        $Prensas = Prensa::model()->findAll('Estado =1 order by ID');
	$amps = $_GET["amps"];/*Array*/
	$hums = $_GET["hums"]; /*Array*/
	$solis = $_GET["lich"]=='nulo'?'NULL':$_GET["lich"];
	$grasas = $_GET["licg"]=='nulo'?'NULL':$_GET["licg"];
        $hora=$_GET['hora']=='nulo' ? '00:00':$_GET['hora'];
	$total = $_GET["total"];
	$revision = $_GET['revision'];
	$count=0;

    $status =Yii::app()->db->createCommand('select IdUsuario from Prensado where codigofila ='.$revision)->queryScalar();
    $rol =$this->idrol();
    if ($status == Yii::app()->user->id){$ban = 1; }  else { $ban =0; }
    if($status and $rol!=1 and $ban==0){
       $count = 2;
    }else{

	for($i=0;$i<$total;$i++){
		$amp = $amps[$i]=='nulo'?'NULL':$amps[$i];
		$hum = $hums[$i]=='nulo'?'NULL':$hums[$i];
		$sql='update Prensado set Amperaje='.$amp.', PorcentajeHumedad='.$hum.',Hora="'.$hora.'" where PrensaID='.$Prensas[$i]->ID.' and codigofila='.$revision;
		$count =$count +Yii::app()->db->createCommand($sql)->execute();
	}
	$sql='update Licor set LicorPorcentajeSolidos='.$solis.',LicorPorcentajeGrasas='.$grasas.' where codigofila='.$revision;
	$count =$count +Yii::app()->db->createCommand($sql)->execute();
        Yii::app()->db->createCommand("call FechaRealMod('".$tabla."',".$revision.",'".$campo."');")->execute();
       if($count>0){
        $count=1;
    }}
    echo $count;
}
public function actionActualizarvaloresrefinacion(){
    $campo="codigofila";
    $tabla = "Refinacion";
    $Refinaci= Pulidora::model()->findAll('Estado=1 order by ID');
    $Reftemp = $_GET["Reftemp"];
    $Refhum = $_GET["Refhum"];
    $Refsol = $_GET["Refsol"];
    $Refaci = $_GET["Refaci"];
    $total = $_GET["total"];
    $revision = $_GET['revision'];
    $hora=$_GET['hora']=='nulo' ? '00:00':$_GET['hora'];
    $count=0;

    $status =Yii::app()->db->createCommand('select IdUsuario from Refinacion where codigofila ='.$revision)->queryScalar();
    $rol = $this->idrol();
    if ($status == Yii::app()->user->id){$ban = 1; }  else {$ban =0;}
    if($status and $rol!=1 and $ban==0){
       $count = 2;
    }else{
    for($i=0;$i<$total;$i++){
        $Reftempf = $Reftemp[$i]=='nulo'?'NULL':$Reftemp[$i];
        $Refhumf = $Refhum[$i]=='nulo'?'NULL':$Refhum[$i];
        $Refsolf = $Refsol[$i]=='nulo'?'NULL':$Refsol[$i];
        $Refacif = $Refaci[$i]=='nulo'?'NULL':$Refaci[$i];
        $sql='update Refinacion set AlimentacionTemperatura='.$Reftempf.',AceitePorcentajeHumedad='.$Refhumf.',AceitePorcentajeSolidos='.$Refsolf.',AceitePorcentajeAcidez='.$Refacif.',Hora="'.$hora.'" where PulidoraID='.$Refinaci[$i]->ID.' and codigofila='.$revision;
        $count =$count +Yii::app()->db->createCommand($sql)->execute();
    }
    Yii::app()->db->createCommand("call FechaRealMod('".$tabla."',".$revision.",'".$campo."');")->execute();
   if($count>0){
        $count=1;
    }}
     echo $count;
}

public function actionActualizarvalorestratamiento(){
    $campo="codigofila";
    $tabla = "Tratamiento";
    $Tratamiento= Planta::model()->findAll('Estado=1 order by ID');
    $Tratemps = $_GET["Tratemp"];
    $Trasolis = $_GET["Trasoli"];
    $Tratemp2s= $_GET["Tratemp2"];
    $Trapres = $_GET["Trapre"];
    $Tratemp3s= $_GET["Tratemp3"];
    $Tratemp4s= $_GET["Tratemp4"];
    $Tratemp5s= $_GET["Tratemp5"];
    $Trasoli2s= $_GET["Trasoli2"];
    $hora=$_GET['hora']=='nulo' ? '00:00':$_GET['hora'];
    $total = $_GET["total"];
    $revision = $_GET['revision'];
    $count=0;

    $status =Yii::app()->db->createCommand('select IdUsuario from Tratamiento where codigofila ='.$revision)->queryScalar();
    $rol = $this->idrol();
        if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
    if($status and $rol!=1 and $ban==0){
       $count = 2;
    }else{
    for($i=0;$i<$total;$i++){
        $Tratemp = $Tratemps[$i]=='nulo'?'NULL':$Tratemps[$i];
        $Trasoli = $Trasolis[$i]=='nulo'?'NULL':$Trasolis[$i];
        $Tratemp2 = $Tratemp2s[$i]=='nulo'?'NULL':$Tratemp2s[$i];
        $Trapre = $Trapres[$i]=='nulo'?'NULL':$Trapres[$i];
        $Tratemp3 = $Tratemp3s[$i]=='nulo'?'NULL':$Tratemp3s[$i];
        $Tratemp4 = $Tratemp4s[$i]=='nulo'?'NULL':$Tratemp4s[$i];
        $Tratemp5 = $Tratemp5s[$i]=='nulo'?'NULL':$Tratemp5s[$i];
        $Trasoli2 = $Trasoli2s[$i]=='nulo'?'NULL':$Trasoli2s[$i];
        $sql='update Tratamiento set AlimentacionAguaColaTemperatura='.$Tratemp.',AlimentacionAguaColaPorcentajeSolidos='.$Trasoli.',VahTemperatura='.$Tratemp2.',VacPeso='.$Trapre.',Temperatura1='.$Tratemp3.',Temperatura2='.$Tratemp4.',Temperatura3='.$Tratemp5.',SalidaPorcentajeSolidos='.$Trasoli2.',Hora="'.$hora.'" where PlantaID='.$Tratamiento[$i]->ID.' and codigofila='.$revision;
        $count =$count +Yii::app()->db->createCommand($sql)->execute();
    }
    Yii::app()->db->createCommand("call FechaRealMod('".$tabla."',".$revision.",'".$campo."');")->execute();
    if($count>0){
        $count=1;
    }}
     echo $count;
}

public function actionActualizarvaloresdecantacion(){
    $campo="codigofila";
    $tabla = "Decantacion";
    $Tricanters = Tricanter::model()->findAll('Estado = 1 order by ID');
    $Detems = $_GET["Detems"];
    $Dehums = $_GET["Dehums"];
    $Desols = $_GET["Desols"];
    $Degras = $_GET["Degras"];
    $total = $_GET["total"];
    $hora=$_GET['hora']=='nulo' ? '00:00':$_GET['hora'];
    $revision = $_GET['revision'];
    $count=0;

    $status =Yii::app()->db->createCommand('select IdUsuario from Decantacion where codigofila ='.$revision)->queryScalar();
    $rol =$this->idrol();
    if ($status == Yii::app()->user->id){ $ban = 1;}  else {$ban =0;}
    if($status and $rol!=1 and $ban==0){
       $count = 2;
    }else{
    for($i=0;$i<$total;$i++){
        $Detem = $Detems[$i]=='nulo'?'NULL':$Detems[$i];
        $Dehum = $Dehums[$i]=='nulo'?'NULL':$Dehums[$i];
        $Desol = $Desols[$i]=='nulo'?'NULL':$Desols[$i];
        $Degra = $Degras[$i]=='nulo'?'NULL':$Degras[$i];
        $sql='update Decantacion set AlimentacionTemperatura='.$Detem.',AlimentacionPorcentajeHumedad='.$Dehum.',AguaColaPorcentajeSolidos='.$Desol.',AguaColaPorcentajeGrasas='.$Degra.',Hora="'.$hora.'" where TricanterID='.$Tricanters[$i]->ID.' and codigofila='.$revision;
        $count =$count +Yii::app()->db->createCommand($sql)->execute();
    }    
    Yii::app()->db->createCommand("call FechaRealMod('".$tabla."',".$revision.",'".$campo."');")->execute();
    if($count>0){
        $count=1;
    }}
     echo $count;
}

public function actionActualizarvalorespresecado(){
    $Rotadisks = Rotadisk::model()->findAll('Estado =1 order by ID');
    $campo="codigofila";
    $tabla = "Presecado";
    $PreAlimMins = $_GET["PreAlimMins"];
    $PreAlimMaxs = $_GET["PreAlimMaxs"];
    $PreAmps = $_GET["PreAmps"];
    $PrePreEjes = $_GET["PrePreEjes"];
    $hora=$_GET['hora']=='nulo' ? '00:00':$_GET['hora'];
    $PreChaqs = $_GET["PreChaqs"];
    $PreTemps = $_GET["PreTemps"];
    $PreHums = $_GET["PreHums"];
    $total = $_GET["total"];
    $revision = $_GET['revision'];
    $count=0;

    $status =Yii::app()->db->createCommand('select IdUsuario from Presecado where codigofila ='.$revision)->queryScalar();
    $rol =$this->idrol();
    if ($status == Yii::app()->user->id){ $ban = 1;}  else {$ban =0;}
    if($status and $rol!=1 and $ban==0){
       $count = 2;
    }else{
    for($i=0;$i<$total;$i++){
        $PreAlimMin = $PreAlimMins[$i]=='nulo'?'NULL':$PreAlimMins[$i];
        $PreAlimMax = $PreAlimMaxs[$i]=='nulo'?'NULL':$PreAlimMaxs[$i];
        $PreAmp = $PreAmps[$i]=='nulo'?'NULL':$PreAmps[$i];
        $PrePreEje = $PrePreEjes[$i]=='nulo'?'NULL':$PrePreEjes[$i];
        $PreChaq = $PreChaqs[$i]=='nulo'?'NULL':$PreChaqs[$i];
        $PreTemp = $PreTemps[$i]=='nulo'?'NULL':$PreTemps[$i];
        $PreHum = $PreHums[$i]=='nulo'?'NULL':$PreHums[$i];
        $sql='update Presecado set MinimoPorcentajeHumedad='.$PreAlimMin.',MaximoPresion='.$PreAlimMax.',Amperaje='.$PreAmp.',PresionEje='.$PrePreEje.',PresionChaqueta='.$PreChaq.',SCR='.$PreTemp.',PorcentajeHumedad='.$PreHum.',Hora="'.$hora.'" where RotadiskID='.$Rotadisks[$i]->ID.' and codigofila='.$revision;
        $count =$count +Yii::app()->db->createCommand($sql)->execute();
    }
    Yii::app()->db->createCommand("call FechaRealMod('".$tabla."',".$revision.",'".$campo."');")->execute();
     if($count>0){
        $count=1;
    }}
     echo $count;
}

public function actionActualizarvaloressecado(){
    $Rotatubos =  Rotatubo::model()->findAll('Estado=1 order by ID ');
    $campo="codigofila";
    $tabla = "Secado";
    $SeAmps = $_GET["SeAmps"];
    $SePreEjes = $_GET["SePreEjes"];
    $hora=$_GET['hora']=='nulo' ? '00:00':$_GET['hora'];
    $SeTemps = $_GET["SeTemps"];
    $SeHums = $_GET["SeHums"];
    $total = $_GET["total"];
    $revision = $_GET['revision'];
    $count=0;
    $status =Yii::app()->db->createCommand('select IdUsuario from Secado where codigofila ='.$revision)->queryScalar();
    $rol =$this->idrol();
    if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
    if($status and $rol!=1 and $ban==0){
       $count = 2;
    }else{
    for($i=0;$i<$total;$i++){
        $SeAmp = $SeAmps[$i]=='nulo'?'NULL':$SeAmps[$i];
        $SePreEje = $SePreEjes[$i]=='nulo'?'NULL':$SePreEjes[$i];
        $SeTemp = $SeTemps[$i]=='nulo'?'NULL':$SeTemps[$i];
        $SeHum = $SeHums[$i]=='nulo'?'NULL':$SeHums[$i];
        $sql='update Secado set Amperaje='.$SeAmp.',PresionEje='.$SePreEje.',SRC='.$SeTemp.',PorcentajeHumedad='.$SeHum.',Hora="'.$hora.'" where RotatuboID='.$Rotatubos[$i]->ID.' and codigofila='.$revision;
        $count =$count +Yii::app()->db->createCommand($sql)->execute();
    }
    Yii::app()->db->createCommand("call FechaRealMod('".$tabla."',".$revision.",'".$campo."');")->execute();
     if($count>0){
        $count=1;
    }}
     echo $count;
}
public function actionActualizarvaloresharina(){
    $campo="ID";
    $tabla = "Harina";
    $temp = $_GET["temp"]!="nulo" ? $_GET["temp"]:'NULL';
    $hum = $_GET["hum"]!="nulo" ? $_GET["hum"]:'NULL';
    $ao = $_GET["ao"]!="nulo" ? $_GET["ao"]:'NULL';
    $peso = $_GET["peso"]!="nulo" ? $_GET["peso"]:'NULL';
    $tbvn = $_GET["tbvn"]!="nulo" ? $_GET["tbvn"]:'NULL';
    $prot = $_GET["proteina"]!="nulo" ? $_GET["proteina"]:'NULL';
    $lote = $_GET["lote"]!="nulo" ? $_GET["lote"]:'NULL';
    $clasif = $_GET["clasif"]!="nulo" ? $_GET["clasif"]:'NULL';
    $obser = $_GET["obser"]!="nulo" ? $_GET["obser"]:'NULL';
    $sacos = $_GET["sacos"]!="nulo" ? $_GET["sacos"]:'NULL';
    $revision = $_GET['revision']!="nulo" ? $_GET["revision"]:'0';
    $hora=$_GET['hora']=='nulo' ? '00:00':$_GET['hora'];
    $count=0;

    $status =Yii::app()->db->createCommand('select IdUsuario from Presecado where ID ='.$revision)->queryScalar();
    $rol = $this->idrol();
    if ($status == Yii::app()->user->id){ $ban = 1;}  else {$ban =0;}
    if($status and $rol!=1 and $ban==0){
       $count = 2;
    }else{
    $sql='update Harina set NumeroLote='.$lote.', Temperatura='.$temp.', PorcentajeHumedad='.$hum.', AO='.$ao.', Peso='.$peso.', TBVN='.$tbvn.', PorcentajeProteina='.$prot.', ClasificacionID='.$clasif.', SacoProducidos='.$sacos.',Hora="'.$hora.'" ,ObservacionID='.$obser.' where ID='.$revision;
    $count =$count +Yii::app()->db->createCommand($sql)->execute();
    Yii::app()->db->createCommand("call FechaRealMod('".$tabla."',".$revision.",'".$campo."');")->execute();
     if($count>0){
        $count=1;
    }}
     echo $count;
}
public function clases($val,$min,$max,$naranja){
   $retoro_clase="";
   $p_naramin = ($min*$naranja)/100;
   $p_naramax = ($max*$naranja)/100;
   if($val>($min+$p_naramax) && $val<($max-$p_naramax) || $val==0){
       $retoro_clase ="pnormal";
   }else if($val>=$min && $val<=$max) {
       $retoro_clase ="pnaranja";
   }else{
       $retoro_clase ="projo";
   }

   return ' min="'.$min.'" max="'.$max.'" nara="'.$naranja.'" class="'.$retoro_clase.'" ';
}
public function actionAgregaregistro($pes, $fecha){
    switch($pes){
        case 1:
            $sql ="insert into Recepcion (Fecha, FechaHoraReal) VALUES ('".$fecha."', now());";
        break;
        case 2:
            $sql = "insert into Coccion(Fecha,CocinaID) select '".$fecha."',ID from Cocina where Estado=1 order by ID";
            break;
    }
    Yii::app()->db->createCommand($sql)->execute();
    $this->redirect(array('index','fecha'=>$fecha));
}

public function actionBloquear($recepcion,$tabla){

    switch($tabla){
        case 1:
                $status =Yii::app()->db->createCommand('select IdUsuario from Recepcion where ID ='.$recepcion)->queryScalar();
              if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
		if($status and $ban==1){
                    $sql = 'update Recepcion set IdUsuario= Null where ID ='.$recepcion;
                    $img = "images/candadoabierto.jpeg";
                }
                else if($status and $ban==0){
                    $sql= null;
                    $img = "images/candadocerrado.png";
                }else {
                    $sql = 'update Recepcion set IdUsuario= '.Yii::app()->user->id.' where ID ='.$recepcion;
                    $img ="images/candado.png";
                }
            break;
        case 2:
            $status =Yii::app()->db->createCommand('select IdUsuario from Coccion where RevisionID ='.$recepcion)->queryScalar();
              if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
		if($status and $ban==1){
                    $sql = 'update Coccion set IdUsuario= Null where RevisionID ='.$recepcion;
                    $img = "images/candadoabierto.jpeg";
                }
                else if($status and $ban==0){
                    $sql= null;
                    $img = "images/candadocerrado.png";
                }else {
                    $sql = 'update Coccion set IdUsuario= '.Yii::app()->user->id.' where RevisionID ='.$recepcion;
                    $img ="images/candado.png";
                }
            break;
        case 3:
            $status =Yii::app()->db->createCommand('select IdUsuario from Prensado where RevisionID ='.$recepcion)->queryScalar();
              if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
		if($status and $ban==1){
                    $sql = 'update Prensado set IdUsuario= Null where RevisionID ='.$recepcion;
                    $img = "images/candadoabierto.jpeg";
                }
                else if($status and $ban==0){
                    $sql= null;
                    $img = "images/candadocerrado.png";
                }else {
                    $sql = 'update Prensado set IdUsuario= '.Yii::app()->user->id.' where RevisionID ='.$recepcion;
                    $img ="images/candado.png";
                }
            break;
        case 4:
            $status =Yii::app()->db->createCommand('select IdUsuario from Decantacion where RevisionID ='.$recepcion)->queryScalar();
              if ($status == Yii::app()->user->id){ $ban = 1;}  else {$ban =0;}
		if($status and $ban==1){
                    $sql = 'update Decantacion set IdUsuario= Null where RevisionID ='.$recepcion;
                    $img = "images/candadoabierto.jpeg";
                }
                else if($status and $ban==0){
                    $sql= null;
                    $img = "images/candadocerrado.png";
                }else {
                    $sql = 'update Decantacion set IdUsuario= '.Yii::app()->user->id.' where RevisionID ='.$recepcion;
                    $img ="images/candado.png";
                }
            break;
        case 5:
            $status =Yii::app()->db->createCommand('select IdUsuario from Refinacion where RevisionID ='.$recepcion)->queryScalar();
              if ($status == Yii::app()->user->id){ $ban = 1; }  else { $ban =0;}
		if($status and $ban==1){
                    $sql = 'update Refinacion set IdUsuario= Null where RevisionID ='.$recepcion;
                    $img = "images/candadoabierto.jpeg";
                }
                else if($status and $ban==0){
                    $sql= null;
                    $img = "images/candadocerrado.png";
                }else {
                    $sql = 'update Refinacion set IdUsuario= '.Yii::app()->user->id.' where RevisionID ='.$recepcion;
                    $img ="images/candado.png";
                }
            break;
        case 6:
            $status =Yii::app()->db->createCommand('select IdUsuario from Tratamiento where RevisionID ='.$recepcion)->queryScalar();
              if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
		if($status and $ban==1){
                    $sql = 'update Tratamiento set IdUsuario= Null where RevisionID ='.$recepcion;
                    $img = "images/candadoabierto.jpeg";
                }
                else if($status and $ban==0){
                    $sql= null;
                    $img = "images/candadocerrado.png";
                }else {
                    $sql = 'update Tratamiento set IdUsuario= '.Yii::app()->user->id.' where RevisionID ='.$recepcion;
                    $img ="images/candado.png";
                }
            break;
        case 7:
            $status =Yii::app()->db->createCommand('select IdUsuario from Presecado where RevisionID ='.$recepcion)->queryScalar();
              if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
		if($status and $ban==1){
                    $sql = 'update Presecado set IdUsuario= Null where RevisionID ='.$recepcion;
                    $img = "images/candadoabierto.jpeg";
                }
                else if($status and $ban==0){
                    $sql= null;
                    $img = "images/candadocerrado.png";
                }else {
                    $sql = 'update Presecado set IdUsuario= '.Yii::app()->user->id.' where RevisionID ='.$recepcion;
                    $img ="images/candado.png";
                }
            break;
        case 8:
            $status =Yii::app()->db->createCommand('select IdUsuario from Secado where RevisionID ='.$recepcion)->queryScalar();
              if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
		if($status and $ban==1){
                    $sql = 'update Secado set IdUsuario= Null where RevisionID ='.$recepcion;
                    $img = "images/candadoabierto.jpeg";
                }
                else if($status and $ban==0){
                    $sql= null;
                    $img = "images/candadocerrado.png";
                }else {
                    $sql = 'update Secado set IdUsuario= '.Yii::app()->user->id.' where RevisionID ='.$recepcion;
                    $img ="images/candado.png";
                }
            break;
        case 9:
            $status =Yii::app()->db->createCommand('select IdUsuario from Harina where RevisionID ='.$recepcion)->queryScalar();
              if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
		if($status and $ban==1){
                    $sql = 'update Harina set IdUsuario= Null where RevisionID ='.$recepcion;
                    $img = "images/candadoabierto.jpeg";
                }
                else if($status and $ban==0){
                    $sql= null;
                    $img = "images/candadocerrado.png";
                }else {
                    $sql = 'update Harina set IdUsuario= '.Yii::app()->user->id.' where RevisionID ='.$recepcion;
                    $img ="images/candado.png";
                }
            break;
    }
if($sql != null)		{
Yii::app()->db->createCommand($sql)->execute();}
		echo $img;
}
public function actionIdrecepcion(){
    if(isset($_GET['id'])){
        $id= $_GET['id'];
    }else{
        echo '';
        return;
    }
    $modelo = InforFishIngreMatPri::model()->find('ImgresoMatPriId = '.$id);
    if ($modelo!=null):
        print $modelo->CantidadSG.'_'.$modelo->TipoMateriaPrima;
    else:
        print '.';
    endif;
    
}
public function actionVerpdfRango(){
        $fechaini = $_GET['fechaini'];
        $fechafin = $_GET['fechafin'];
        $horaini = $_GET['horaini'];
        if(strlen($horaini)<=4){$horaini='0'.$horaini;}
        $horafin = $_GET['horafin'];
        if(strlen($horafin)<=4){$horafin = '0'.$horafin;}
        
        //devuelve [pag]= cuantas paginacion voy a dar, [mayor] el valor mayor de las pentaña en ese dia
        
        /*Comenzar a cargar datos*/
        
        $cocina = Cocina::model()->findAll('Estado = 1 order by ID');
        $numerococina = count($cocina);

        /*Prensado Prensas*/
        $prensas = Prensa::model()->findAll('Estado = 1');
        $numeroprensas = count($prensas);

        /*Decantasion Tricanter*/
        $tricanter = Tricanter::model()->findAll('Estado = 1');
        $numerotricanter = count($tricanter);

        /*Refinacion Pulidora*/
        $pulidora = Pulidora::model()->findAll('Estado = 1');
        $numeropulidora = count($pulidora);
        /*Tratamiento Planta*/

        $planta = Planta::model()->findAll('Estado = 1');
        $numeroplantas=count($planta);

        $rotadisk = Rotadisk::model()->findAll('Estado = 1');
        $numerorotadisk = count($rotadisk);

        /*Secado Rotatubo*/
        $rotatubo = Rotatubo::model()->findAll('Estado = 1');
        $numerorotatubo = count($rotatubo);
        /*Parametros*/
        $p = Parametros::model()->find();
        /*Fin de cargar datos*/
        $PAGINADO = 12;
        $mPDF1 = Yii::app()->ePdf->mpdf('utf-8','A4-L','','',5,5,10,10,5,5,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
        $mPDF1->useOnlyCoreFonts = true;
        $mPDF1->SetTitle("Reporte");
        $mPDF1->SetAuthor("TADEL");
        //$mPDF1->SetWatermarkText("Pacientes");
        
        $mPDF1->showWatermarkText = true;
        $mPDF1->watermark_font = 'DejaVuSansCondensed';
        $mPDF1->watermarkTextAlpha = 0.1;
        $mPDF1->SetDisplayMode('fullpage');
//        $fechaaux=$model[0]->Fecha.' '.($model[0]->Hora?$model[0]->Hora:'00:00');
//        $cont=0;
//        $fechainicial = $fechaini.' '.$horaini;
//        $fechafinal = $fechafin.' '.$horafin;
        
        while($fechaini<=$fechafin):
            $fechainicial = $fechaini.' 00:00';
            $fechafinal = $fechaini.' 23:59';
            $r = Yii::app()->db->createCommand("CALL valormayor ('".$fechainicial."','".$fechafinal."')")->queryRow();
            $total=$r['mayor'];
            for ($i = 0 ; $i < $r['pag'];$i++):
                $total = $total-($i*$PAGINADO);
                if($r['pag']>0):
                    $mPDF1->AddPage();
                    $mPDF1->WriteHTML($this->renderPartial('pdfRango', array('fechaini'=>$fechainicial,'cont'=>$i,'limit'=>$total,'fechafin'=>$fechafinal,'p'=>$p,
                        'numerococina'=>$numerococina,'cocinas'=>$cocina,'numeroprensas'=>$numeroprensas,'prensas'=>$prensas,'numerotricanter'=>$numerotricanter,'tricanter'=>$tricanter,
                        'numeropulidora'=>$numeropulidora,'pulidora'=>$pulidora,'numeroplantas'=>$numeroplantas,'planta'=>$planta,'numerorotadisk'=>$numerorotadisk,'rotadisk'=>$rotadisk,'numerorotatubo'=>$numerorotatubo,'rotatubo'=>$rotatubo), true));
                endif;
            endfor;
            $nuevafecha = strtotime ( '+1 day' , strtotime ( $fechaini ) ) ;
            $fechaini = date ( 'Y-m-d' , $nuevafecha );
        endwhile;
         $mPDF1->Output('pdfRango'.date('Y-m-d H:i'),'I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
         exit;
}

public function actionGenerarExcelprod($fechaini,$fechafin){
        $fechaini = $_GET['fechaini'];
        $fechafin = $_GET['fechafin'];
        $horaini = $_GET['horaini'];
        if(strlen($horaini)<=4){$horaini='0'.$horaini;}
        $horafin = $_GET['horafin'];
        if(strlen($horafin)<=4){$horafin = '0'.$horafin;}
        
        //devuelve [pag]= cuantas paginacion voy a dar, [mayor] el valor mayor de las pentaña en ese dia
        
        /*Comenzar a cargar datos*/
        
        $cocina = Cocina::model()->findAll('Estado = 1 order by ID');
        $numerococina = count($cocina);

        /*Prensado Prensas*/
        $prensas = Prensa::model()->findAll('Estado = 1');
        $numeroprensas = count($prensas);

        /*Decantasion Tricanter*/
        $tricanter = Tricanter::model()->findAll('Estado = 1');
        $numerotricanter = count($tricanter);

        /*Refinacion Pulidora*/
        $pulidora = Pulidora::model()->findAll('Estado = 1');
        $numeropulidora = count($pulidora);
        /*Tratamiento Planta*/

        $planta = Planta::model()->findAll('Estado = 1');
        $numeroplantas=count($planta);

        $rotadisk = Rotadisk::model()->findAll('Estado = 1');
        $numerorotadisk = count($rotadisk);

        /*Secado Rotatubo*/
        $rotatubo = Rotatubo::model()->findAll('Estado = 1');
        $numerorotatubo = count($rotatubo);
        /*Parametros*/
        $p = Parametros::model()->find();
            $contenido =$this->renderPartial("ExcelRango", array('fechainicio'=>$fechaini,'fechafinal'=>$fechafin,'p'=>$p,
                'numerococina'=>$numerococina,'cocinas'=>$cocina,'numeroprensas'=>$numeroprensas,'prensas'=>$prensas,'numerotricanter'=>$numerotricanter,'tricanter'=>$tricanter,
                'numeropulidora'=>$numeropulidora,'pulidora'=>$pulidora,'numeroplantas'=>$numeroplantas,'planta'=>$planta,'numerorotadisk'=>$numerorotadisk,'rotadisk'=>$rotadisk,'numerorotatubo'=>$numerorotatubo,'rotatubo'=>$rotatubo), true);
        Yii::app()->request->sendFile('ControlDeProcesoDeProduccion'.date('Y-m-d').'.xls', $contenido);
        exit;
            }
public function actionHoraActual(){
    echo date('H:i');
}
public function actionAddFila($fecha,$indice){
    $img = 'images/candadoabierto.jpeg';
    $rt='';
    switch($indice):
        case 1:
            $row = new Recepcion;
            $row->FechaHoraReal = Yii::app()->db->createCommand('select now();')->queryScalar();
            $row->Fecha = $fecha;
            if ($row->save()):
                $rt =$rt.'<tr class ="odd" style="font-size:11px;height:7px;">';
                $rt =$rt.'<th id="gruphora'.$row->ID.'" rowspan="1"></th>';
                $hora=  explode(':', $row->Hora);
                $rt =$rt.'<th ><input onblur="cambiohora('.$row->ID.',this.value);" id="hora'.$row->ID.'"  value="'.$hora[0].'" class="hora" size="4" > : <input id="minutos'.$row->ID.'" value="'.$hora[1].'" class="minutos" size="4" ><button rel ="'.$row->ID.'" onclick="horaActual(this,1)" style=";margin-left:4px;" ><span class="glyphicon glyphicon-time"></span></button></th>';
                $rt =$rt.'<th><select id="secu'.$row->ID.'" onchange="getEspecies('.$row->ID.',this.value)" >'.$this->Secuencia('00',$fecha,$row->Secuencia).'</select></th>';
                $rt =$rt.'<th onchange="recepcion(this);" id="idrecep'.$row->ID.'" rel="'.$row->ID.'">'.$row->RecepcionID.'</th>';
                $rt =$rt.'<th id= "especie'.$row->ID.'">'.$row->EspecieID.'</th>';
                $rt =$rt.'<th id="peso'.$row->ID.'">'.$row->PesoTonelada.'</th>';
                $rt =$rt.'<td id="desc'.$row->ID.'" rel="'.$row->ID.'" onchange="descuento(this);" >'.$row->Descuento.'</td>';
                $rt =$rt.'<th id="pesoneto'.$row->ID.'">'.$row->Pesoneto.'</th>';
                $rt =$rt.'<td id="tbvn'.$row->ID.'">'.$row->TBVN.'</td>';
                $hora=  explode(':', $row->Horatolva);
                $rt =$rt.'<th ><input id="horatolva'.$row->ID.'"  class="hora horaprincipal" size="4" value="'.$hora[0].'"> : <input id="minutostolva'.$row->ID.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->ID.'" onclick="horaActualTolva(this)" title="Obtener hora actual" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
                $rt =$rt.'<th>'.CHtml::dropDownList('Lista',$row->TolvaID,CHtml::listData(Tolva::model()->findAll('Estado=1'),'ID','Nombre'),array('empty'=>'','class'=>'form-control','style'=>"height:18px; font-size:9px;padding:1px;margin:1px;",'id'=>'tolva'.$row->ID)).'</th>';
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardar(this)','rel'=>$row->ID,'style'=>'margin:0px;')).'</th>';
                if($this->idrol()==2):
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,1);','rel'=>$row->ID,'style'=>'margin:0px;')).'</th>';
                endif;
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,1);','rel'=>$row->ID,'style'=>'margin:0px;')).'</th>';
                $rt =$rt.'</tr>';
            endif;
            break;
        case 2:
            $cf =date('ymdHms'); //codigo de fila
            $sql = "insert into Coccion(Fecha,codigofila,CocinaID) select '".$fecha."','".$cf."',ID from Cocina where Estado=1 order by ID";
            $r = Yii::app()->db->createCommand($sql)->execute();
            if ($r > 0):
                $m = Coccion::model()->findAll('codigofila='.$cf);
                $contadortabla=1;
                $count= count($m);
                $cocinas = Cocina::model()->findAll('Estado =1 order by ID');
                foreach($m as $row){
                    $p= $cocinas[$contadortabla-1];
                    $status = $row->IdUsuario;
                    if ($status == Yii::app()->user->id){$ban = 1; }  else { $ban =0; }//verificar si el Idusuario es el mismo de el user logeado
                    if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                        $img = 'images/candado.png';
                    }else {$img = 'images/candadoabierto.jpeg';}
                    if($contadortabla==1){
                        $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                        $hora=  explode(':', $row->Hora);
                        $rt =$rt.'<th ><input  id="horacocc'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutoscocc'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,2)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';                        
                    }
                    $rt =$rt.'<td id="rpm'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->RPM,$p->CoccionMinRmp,$p->CoccionMaxRmp,$p->CoccionRpmAlertaNaranja).' >'.$row->RPM.'</td>';
                    $rt =$rt.'<td id="presioneje'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PresionEje,$p->CoccionMinPresionEje,$p->CoccionMaxPresionEje,$p->CoccionPresionEjeAlertaNaranja).'>'.$row->PresionEje.'</td>';
                    $rt =$rt.'<td id="presionchaqueta'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PresionChaqueta,$p->CoccionMinPresionChaqueta,$p->CoccionMaxPresionChaqueta,$p->CoccionPresionChaquetaAlertaNaranja).'>'.$row->PresionChaqueta.'</td>';
                    $rt =$rt.'<td id="temperatura'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Temperatura,$p->CoccionMinTemperatura,$p->CoccionMaxTemperatura,$p->CoccionPresionTemperaturaAlertaNaranja).'>'.$row->Temperatura.'</td>';
                    $contadortabla++;
                }
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardarcocinas(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                        if($this->idrol()==2):
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,2);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                        endif;
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,2);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                        $rt =$rt.'</tr>';
            endif;
            break;
        case 3:
            $contadortabla=1;
            $cf =date('ymdHms'); //codigo de fila
            $prensas= Prensa::model()->findAll('Estado=1 order by ID');
            $sql = "insert into Licor (codigofila) values ('".$cf."');";
            $r = Yii::app()->db->createCommand($sql)->execute();
            $sql = "insert into Prensado(Fecha,codigofila,PrensaID) select '".$fecha."','".$cf."',ID from Prensa where Estado=1 order by ID";
            $r += Yii::app()->db->createCommand($sql)->execute();
            if($r >0):
                $result= Prensado::model()->findAll("codigofila=".$cf);
                $licor = Licor::model()->find("codigofila=".$cf);
                $sql = "SELECT LicorMinPorcentajeSolidos, LicorMaxPorcentajeSolidos, LicorPorcentajeSolidosAlertaNaranja,LicorMinPorcentajeGrasas, LicorMaxPorcentajeGrasas, LicorPorcentajeGrasasAlertaNaranja from Parametros LIMIT 1";
                $p = Yii::app()->db->createCommand($sql)->queryRow();
                $count= count($result);
                foreach($result as $row){
                    $pp =$prensas[$contadortabla-1];
                    $status = $row->IdUsuario;
                    if ($status == Yii::app()->user->id){$ban = 1; }  else {$ban =0;}
                    if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                        $img = 'images/candado.png';
                    }else {$img = 'images/candadoabierto.jpeg';}
                        if($contadortabla==1){
                                $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                                $hora=  explode(':', $row->Hora);
                                $rt =$rt.'<th ><input  id="horaprensa'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutosprensa'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,4)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
                        }
                $rt =$rt.'<td id="preAmp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Amperaje, $pp->PrensadoMinAmperaje, $pp->PrensadoMaxAmperaje, $pp->PrensadoAmperajeAlertaNaranja).' >'.$row->Amperaje.'</td>';
                $rt =$rt.'<td id="preHum'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PorcentajeHumedad,$pp->PrensadoMinPorcentajeHumedad, $pp->PrensadoMaxPorcentajeHumedad, $pp->PrensadoPorcentajeHumedadAlertaNaranja).' >'.$row->PorcentajeHumedad.'</td>';
                $contadortabla++;
                if($contadortabla>$count){
                        $contadortabla=1;
                        $rt =$rt.'<td id="preLicH'.$row->codigofila.'" '.$this->clases($licor->LicorPorcentajeSolidos, $p['LicorMinPorcentajeSolidos'], $p['LicorMaxPorcentajeSolidos'], $p['LicorPorcentajeSolidosAlertaNaranja']).' >'.$licor->LicorPorcentajeSolidos.'</td>';
                        $rt =$rt.'<td id="preLicG'.$row->codigofila.'" '.$this->clases($licor->LicorPorcentajeGrasas, $p['LicorMinPorcentajeGrasas'], $p['LicorMaxPorcentajeGrasas'], $p['LicorPorcentajeGrasasAlertaNaranja']).' >'.$licor->LicorPorcentajeGrasas.'</td>';
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Bloquear', array ('onclick'=>'guardarprensado(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                        if($this->idrol()==2):
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Actualizar', array ('onclick'=>'bloquear(this,3);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                        endif;
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,3);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                        $rt =$rt.'</tr>';
                }
                }
            endif;
            break;
        case 4:
            $cf =date('ymdHms'); //codigo de fila
            $sql = "insert into Decantacion (Fecha,codigofila,TricanterID) select '".$fecha."','".$cf."',ID from Tricanter where Estado=1 order by ID";
            $r = Yii::app()->db->createCommand($sql)->execute();
            if($r >0):
            $tricanter = Tricanter::model()->findAll('Estado=1 order by ID');
            $result = Decantacion::model()->findAll('codigofila='.$cf);
            $count= count($result);
            $contadortabla=1;
            foreach($result as $row){
                $p =$tricanter[$contadortabla-1];
                $status = $row->IdUsuario;
                if ($status == Yii::app()->user->id){ $ban = 1; }  else {$ban =0; }
                if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                    $img = 'images/candado.png';
                }else {$img = 'images/candadoabierto.jpeg';}
                if($contadortabla==1){
                    $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                    $hora=  explode(':', $row->Hora);
                    $rt =$rt.'<th ><input  id="horadeca'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutosdeca'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,3)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
                }
                $rt =$rt.'<td id="Dtem'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AlimentacionTemperatura,$p->DecantacionMinAlimentacionTemperatura,$p->DecantacionMaxAlimentacionTemperatura,$p->DecantacionAlimentacionTemperaturaAlertaNaranja).' >'.$row->AlimentacionTemperatura.'</td>';
                $rt =$rt.'<td id="Dhum'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AlimentacionPorcentajeHumedad, $p->DecantacionMinAlimentacionPorcentajeHumedad, $p->DecantacionMaxAlimentacionPorcentajeHumedad, $p->DecantacionAlimentacionPorcentajeHumedadAlertaNaranja).' >'.$row->AlimentacionPorcentajeHumedad.'</td>';
                $rt =$rt.'<td id="Dsol'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AguaColaPorcentajeSolidos,$p->DecantacionMinAguaColaPorcentajeSolidos, $p->DecantacionMaxAguaColaPorcentajeSolidos, $p->DecantacionAguaColaPorcentajeSolidosAlertaNaranja).' >'.$row->AguaColaPorcentajeSolidos.'</td>';
                $rt =$rt.'<td id="Dgra'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AguaColaPorcentajeGrasas, $p->DecantacionMinAguaColaPorcentajeGrasas, $p->DecantacionMaxAguaColaPorcentajeGrasas, $p->DecantacionAguaColaPorcentajeGrasasAlertaNaranja).' >'.$row->AguaColaPorcentajeGrasas.'</td>';
                $contadortabla++;
            }
            $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardardecantacion(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                    if($this->idrol()==2):
                        $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,4);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                    endif;
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,4);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                    $rt =$rt.'</tr>';
            endif;
            break;
        case 5:
            $cf =date('ymdHms'); //codigo de fila
            $sql = "insert into Refinacion (Fecha,codigofila,PulidoraID) select '".$fecha."','".$cf."',ID from Pulidora where Estado=1 order by ID";
            $r = Yii::app()->db->createCommand($sql)->execute();
            if($r >0):
                $result = Refinacion::model()->findAll('codigofila='.$cf);
                $contadortabla=1;
                $pulidora = Pulidora::model()->findAll('Estado=1 order by ID');
                $count = count($result);
                foreach($result as $row){
                    $p = $pulidora[$contadortabla-1];
                    $status = $row->IdUsuario;
                    if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
                    if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                        $img = 'images/candado.png';
                    }else {$img = 'images/candadoabierto.jpeg';}
                    if($contadortabla==1){
                        $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                        $hora=  explode(':', $row->Hora);                   
                        $rt =$rt.'<th ><input  id="horarefi'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutosrefi'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,5)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';                        
                    }
                    $rt =$rt.'<td id="retem'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AlimentacionTemperatura, $p->RefinacionMinAlimentacionTemperatura, $p->RefinacionMaxAlimentacionTemperatura, $p->RefinacionTemperaturaAlertaNaranja).' >'.$row->AlimentacionTemperatura.'</td>';
                    $rt =$rt.'<td id="rehum'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AceitePorcentajeHumedad, $p->RefinacionMinAceitePorcentajeHumedad, $p->RefinacionMaxAceitePorcentajeHumedad, $p->RefinacionAceitePorcentajeHumedadAlertaNaranja).' >'.$row->AceitePorcentajeHumedad.'</td>';
                    $rt =$rt.'<td id="resol'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AceitePorcentajeSolidos, $p->RefinacionMinAceitePorcentajeSolidos, $p->RefinacionMaxAceitePorcentajeSolidos, $p->RefinacionAceitePorcentajeSolidosAlertaNaranja).' >'.$row->AceitePorcentajeSolidos.'</td>';
                    $rt =$rt.'<td id="reAcid'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AceitePorcentajeAcidez, $p->RefinacionMinAceitePorcentajeAcidez, $p->RefinacionMaxAceitePorcentajeAcidez, $p->RefinacionAceitePorcentajeAcidezAlertaNaranja).' >'.$row->AceitePorcentajeAcidez.'</td>';
                    $contadortabla++;
                }
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardarrefinacion(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                if($this->idrol()==2):
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,5);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                endif;
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,5);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                $rt =$rt.'</tr>';
            endif;
            break;
        case 6:
            $cf =date('ymdHms'); //codigo de fila
            $sql = "insert into Tratamiento (Fecha,codigofila,PlantaID) select '".$fecha."','".$cf."',ID from Planta where Estado=1 order by ID";
            $r = Yii::app()->db->createCommand($sql)->execute();
            if($r >0):
                $result = Tratamiento::model()->findAll('codigofila='.$cf);
                $platas = Planta::model()->findAll('Estado = 1 order by ID');
                $contadortabla=1;
                $count = count($result);
                foreach($result as $row){
                    $p = $platas[$contadortabla-1];
                    $status = $row->IdUsuario;
                    if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
                    if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                        $img = 'images/candado.png';
                    }else {$img = 'images/candadoabierto.jpeg';}
                    if($contadortabla==1){
                        $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                        $hora=  explode(':', $row->Hora);                        
                        $rt =$rt.'<th ><input  id="horatrata'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutostrata'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,6)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';   
                    }
                    $rt =$rt.'<td id="tratemp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AlimentacionAguaColaTemperatura,$p->TratamientoMinAlimentacionAguaColaTemperatura,$p->TratamientoMaxAlimentacionAguaColaTemperatura,$p->TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja).' >'.$row->AlimentacionAguaColaTemperatura.'</td>';
                    $rt =$rt.'<td id="trasoli'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->AlimentacionAguaColaPorcentajeSolidos,$p->TratamientoMinAlimentacionAguaColaPorcentajeSolidos,$p->TratamientoMaxAlimentacionAguaColaPorcentajeSolidos,$p->TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja).' >'.$row->AlimentacionAguaColaPorcentajeSolidos.'</td>';
                    $rt =$rt.'<td id="tratemp2'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->VahTemperatura,$p->TratamientoMinVahTemperatura,$p->TratamientoMaxVahTemperatura,$p->TratamientoVahTemperaturaAlertaNaranja).' >'.$row->VahTemperatura.'</td>';
                    $rt =$rt.'<td id="trapre'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->VacPeso,$p->TratamientoMinVacPeso,$p->TratamientoMaxVacPeso,$p->TratamientoVacPesoAlertaNaranja).' >'.$row->VacPeso.'</td>';
                    $rt =$rt.'<td id="tratemp3'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Temperatura1,$p->TratamientoMinTemperatura1,$p->TratamientoMaxTemperatura1,$p->TratamientoTemperatura1AlertaNaranja).' >'.$row->Temperatura1.'</td>';
                    $rt =$rt.'<td id="tratemp4'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Temperatura2,$p->TratamientoMinTemperatura2,$p->TratamientoMaxTemperatura2,$p->TratamientoTemperatura2AlertaNaranja).' >'.$row->Temperatura2.'</td>';
                    $rt =$rt.'<td id="tratemp5'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Temperatura3,$p->TratamientoMinTemperatura3,$p->TratamientoMaxTemperatura3,$p->TratamientoTemperatura3AlertaNaranja).' >'.$row->Temperatura3.'</td>';
                    $rt =$rt.'<td id="trasoli2'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->SalidaPorcentajeSolidos,$p->TratamientoMinSalidaPorcentajeSolidos,$p->TratamientoMaxSalidaPorcentajeSolidos,$p->TratamientoSalidaPorcentajeSolidosAlertaNaranja).' >'.$row->SalidaPorcentajeSolidos.'</td>';
                    $contadortabla++;
                }
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardartratamiento(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                if($this->idrol()==2):
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,6);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                endif;
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,6);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                $rt =$rt.'</tr>';
            endif;
            break;
        case 7:
            $cf =date('ymdHms'); //codigo de fila
            $sql = "insert into Presecado (Fecha,codigofila,RotadiskID) select '".$fecha."','".$cf."',ID from Rotadisk where Estado=1 order by ID";
            $r = Yii::app()->db->createCommand($sql)->execute();
            if($r >0):
                $result = Presecado::model()->findAll('codigofila='.$cf);
                $rotadisk = Rotadisk::model()->findAll('Estado =1 order by ID');
                $contadortabla=1;
                $count = count($result);
                foreach($result as $row){
                    $p = $rotadisk[$contadortabla-1];
                    $status = $row->IdUsuario;
                    if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
                    if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                        $img = 'images/candado.png';
                    }else {$img = 'images/candadoabierto.jpeg';}
                        if($contadortabla==1){
                            $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                            $hora=  explode(':', $row->Hora);
                            $rt =$rt.'<th ><input  id="horaprese'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutosprese'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,7)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
                        }
                        $rt =$rt.'<td id="preseAlimMin'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->MinimoPorcentajeHumedad,$p->PresecadoMinMinimoPorcentajeHumedad,$p->PresecadoMaxMinimoPorcentajeHumedad,$p->PresecadoMinimoPorcentajeHumedadAlertaNaranja).' >'.$row->MinimoPorcentajeHumedad.'</td>';/*Columna Alimentacion*/
                        $rt =$rt.'<td id="preseAlimMax'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->MaximoPresion,$p->PresecadoMinMaximoPresion,$p->PresecadoMaxMaximoPresion,$p->PresecadoMaximoPresionAlertaNaranja).' >'.$row->MaximoPresion.'</td>';/*Columna Alimentacion*/
                        $rt =$rt.'<td id="preseAmp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Amperaje,$p->PresecadoMinAmperaje,$p->PresecadoMaxAmperaje,$p->PresecadoAmperajeAlertaNaranja).' >'.$row->Amperaje.'</td>';
                        $rt =$rt.'<td id="presePreEje'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PresionEje,$p->PresecadoMinPresionEje,$p->PresecadoMaxPresionEje,$p->PresecadoPresionEjeAlertaNaranja).' >'.$row->PresionEje.'</td>';
                        $rt =$rt.'<td id="presePreChaq'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PresionChaqueta,$p->PresecadoMinPresionChaqueta,$p->PresecadoMaxPresionChaqueta,$p->PresecadoPresionChaquetaAlertaNaranja).' >'.$row->PresionChaqueta.'</td>';
                        $rt =$rt.'<td id="preseTemp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->SCR,$p->PresecadoMinSCR,$p->PresecadoMaxSCR,$p->PresecadoSCRAlertaNaranja).' >'.$row->SCR.'</td>';
                        $rt =$rt.'<td id="preseHum'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PorcentajeHumedad,$p->PresecadoMinPorcentajeHumedad,$p->PresecadoMaxPorcentajeHumedad,$p->PresecadoPorcentajeHumedadAlertaNaranja).' >'.$row->PorcentajeHumedad.'</td>';
                        $contadortabla++;
                } 
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardarpresecado(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                if($this->idrol()==2):
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,7);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                endif;
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,7);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                $rt =$rt.'</tr>';
            endif;
            break;
        case 8:
            $cf =date('ymdHms'); //codigo de fila
            $sql = "insert into Secado (Fecha,codigofila,RotatuboID) select '".$fecha."','".$cf."',ID from Rotatubo where Estado=1 order by ID";
            $r = Yii::app()->db->createCommand($sql)->execute();
            if($r >0):
                $result = Secado::model()->findAll('codigofila='.$cf);
                $rotatubo = Rotatubo::model()->findAll('Estado = 1 order by ID');
                $contadortabla=1;
                $count =count($result);
                foreach($result as $row){
                    $p = $rotatubo[$contadortabla-1];
                    $status = $row->IdUsuario;
                    if ($status == Yii::app()->user->id){$ban = 1;}  else {$ban =0;}
                    if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                        $img = 'images/candado.png';
                    }else {$img = 'images/candadoabierto.jpeg';}
                    if($contadortabla==1){
                         $rt =$rt.'<tr class ="odd" style="font-size:11px;height:10px;">';
                         $hora=  explode(':', $row->Hora);
                         $rt =$rt.'<th ><input  id="horasecado'.$row->codigofila.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutossecado'.$row->codigofila.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->codigofila.'" onclick="horaActual(this,8)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
                    }
                        $rt =$rt.'<td id="seAmp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->Amperaje,$p->RotatuboMinAmperaje,$p->RotatuboMaxAmperaje,$p->RotatuboAmperajeAlertaNaranja).' >'.$row->Amperaje.'</td>';
                        $rt =$rt.'<td id="sePreEje'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PresionEje,$p->RotatuboMinPresionEje,$p->RotatuboMaxPresionEje,$p->RotatuboPresionEjeAlertaNaranja).' >'.$row->PresionEje.'</td>';
                        $rt =$rt.'<td id="seTemp'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->SRC,$p->RotatuboMinSRC,$p->RotatuboMaxSRC,$p->RotatuboSRCAlertaNaranja).' >'.$row->SRC.'</td>';
                        $rt =$rt.'<td id="seHum'.$contadortabla.''.$row->codigofila.'" '.$this->clases($row->PorcentajeHumedad,$p->RotatuboMinHumedad,$p->RotatuboMaxHumedad,$p->RotatuboHumedadAlertaNaranja).' >'.$row->PorcentajeHumedad.'</td>';
                        $contadortabla++;
                }
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardarsecado(this)','rel'=>$row->codigofila,'total'=>$count,'style'=>'margin:px;')).'</th>';
                if($this->idrol()==2):
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,8);','rel'=>$row->codigofila,'style'=>'margin:px;')).'</th>';
                endif;
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,8);','rel'=>$row->codigofila,'style'=>'margin:0px;')).'</th>';
                $rt =$rt.'</tr>';
            endif;
        break;
        case 9:
            $row = new Harina();
            $row->Fecha=$fecha;
            if($row->save()):
                $status = $row->IdUsuario;
                if ($status == Yii::app()->user->id){$ban = 1; }  else { $ban =0;  }
                if($status and $ban==0){$img = 'images/candadocerrado.png';} elseif ($status and $ban==1) {
                    $img = 'images/candado.png';
                }else {$img = 'images/candadoabierto.jpeg';}
                $rt = $rt.'<tr class ="odd" style="font-size:10px;">';
                $hora=  explode(':', $row->Hora);
                $rt =$rt.'<th ><input  id="horahari'.$row->ID.'"  class="hora" size="4" value="'.$hora[0].'"> : <input id="minutoshari'.$row->ID.'" class="minutos" size="4" value="'.$hora[1].'"><button rel ="'.$row->ID.'" onclick="horaActual(this,9)" style=";margin-left:4px;"><span class="glyphicon glyphicon-time"></span></button></th>';
                $rt =$rt.'<td id="hariTem'.$row->ID.'" >'.$row->Temperatura.'</td>';
                $rt =$rt.'<td id="hariPorH'.$row->ID.'" >'.$row->PorcentajeHumedad.'</td>';
                $rt =$rt.'<td id="hariAO'.$row->ID.'" >'.$row->AO.'</td>';
                $rt =$rt.'<td id="hariPeso'.$row->ID.'" >'.$row->Peso.'</td>';
                $rt =$rt.'<td id="hariTBVN'.$row->ID.'" >'.$row->TBVN.'</td>';
                $rt =$rt.'<td id="hariPorP'.$row->ID.'" >'.$row->PorcentajeProteina.'</td>';
                $rt =$rt.'<td id="hariNumLot'.$row->ID.'" >'.$row->NumeroLote.'</td>';
                $rt =$rt.'<td id="hariSacos'.$row->ID.'" >'.$row->SacoProducidos.'</td>';
                $rt =$rt.'<th>'.CHtml::dropDownList('Lista',$row->ClasificacionID,CHtml::listData(Clasificacion::model()->findAll('Estado=1'),'ID','Nombre'),array('empty'=>' ','class'=>'form-control','style'=>"height:25px; font-size:12px;padding:4px;",'id'=>'hariClasif'.$row->ID)).'</th>';
                $rt =$rt.'<th>'.CHtml::dropDownList('Lista',$row->ObservacionID,CHtml::listData(Observacion::model()->findAll('Estado=1'),'ID','Descripcion'),array('empty'=>'','class'=>'form-control','style'=>"height:25px; font-size:12px;padding:4px;",'id'=>'hariObser'.$row->ID)).'</th>';
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardarharina(this)','rel'=>$row->ID,'style'=>'margin:0px;')).'</th>';
                if($this->idrol()==2):
                    $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image($img, 'Bloquear', array ('onclick'=>'bloquear(this,9);','rel'=>$row->ID,'style'=>'margin:px;')).'</th>';
                endif;
                $rt =$rt.'<th style="width:30px;padding:2px;">'.CHtml::image('images/eliminar.png', 'eliminar', array ('onclick'=>'eliminar(this,9);','rel'=>$row->ID,'style'=>'margin:0px;')).'</th>';
                $rt = $rt.'</tr>';
        endif;
    endswitch;
    echo $rt;
}
public function cla($val,$min,$max,$naranja){
   $retoro_clase="";
    $p_naramax = ($max*$naranja)/100;
    $p_naramin = ($min*$naranja)/100;
   if($val>($min+$p_naramax) && $val<($max-$p_naramax) || $val==0){
       $retoro_clase ="pnormal";
   }else if($val>=$min && $val<=$max) {
       $retoro_clase ="pnaranja";
   }else{
       $retoro_clase ="projo";
   }

   return 'class="'.$retoro_clase.'" ';
}

public function actionSecuencia($hora,$fecha){
    $sql="select Secuencia as Hora from [InforFish.IngreMatPri] where  CAST(Fecha as DATE)='".$fecha."' and DATEPART(HOUR,Fecha)= ".$hora;
    $res = Yii::app()->sqldb->createCommand($sql)->queryColumn();
    $r="";
    $r.='<option value="0"></option>';
    foreach($res as $key => $value){
        $r.='<option value="'.$value.'">'.$value.'</option>';
    }
    echo $r;
}
public function Secuencia($hora,$fecha,$secuencia){
    $sql="select Secuencia as Hora from [InforFish.IngreMatPri] where  CAST(Fecha as DATE)='".$fecha."' and DATEPART(HOUR,Fecha)= ".$hora;
    $res = Yii::app()->sqldb->createCommand($sql)->queryColumn();
    $r="";
    $r.='<option value="0"></option>';
    foreach($res as $key => $value){
        if($value==$secuencia):
        $r.='<option value="'.$value.'" selected >'.$value.'</option>';
        else:
        $r.='<option value="'.$value.'">'.$value.'</option>';    
        endif;
    }
    return $r;
}
public function actionGetEspecie($hora,$fecha, $sec){
    $sql = "select ImgresoMatPriId as Id,TipoMateriaPrima as Especie, CantidadSG as Peso from [InforFish.IngreMatPri] where  CAST(Fecha as DATE)='".$fecha."' and DATEPART(HOUR,Fecha)= ".$hora." and Secuencia = ".$sec;
    $res = Yii::app()->sqldb->createCommand($sql)->queryRow();
    $sql="select Secuencia from recepcion where RecepcionID =".$res['Id'];
    $existe = Yii::app()->db->createCommand($sql)->queryColumn();
    $exi = count($existe)>0 ? $existe[0]: 'no';
    $r="";
    $r=$res['Id'].'_'.$res['Especie'].'_'.$res['Peso'].'_'.$exi;
    echo $r;
}
public function actionEliminarregistro($id, $pest){
    $sql="";
    $rol = $this->idrol();
    $result = 1;
    switch ($pest):
        case 1:
            $sql = "delete from recepcion where ID=".$id;
            $sqluser = "select IdUsuario from recepcion where ID=".$id;
            break;
        case 2:
            $sql = "delete from coccion where codigofila=".$id;
            $sqluser = "select IdUsuario from coccion where codigofila=".$id;
            break;
        case 3:
            $sql = "delete from licor where codigofila=".$id;
            Yii::app()->db->createCommand($sql)->execute();
            $sql = "delete from prensado where codigofila=".$id;
            $sqluser = "select IdUsuario from coccion where codigofila=".$id;
            break;
        case 4:
            $sql = "delete from decantacion where codigofila=".$id;
            $sqluser = "select IdUsuario from decantacion where codigofila=".$id;
            break;
        case 5:
            $sql = "delete from refinacion where codigofila=".$id;
            $sqluser = "select IdUsuario from refinacion where codigofila=".$id;
            break;
        case 6:
            $sql = "delete from tratamiento where codigofila=".$id;
            $sqluser = "select IdUsuario from tratamiento where codigofila=".$id;
            break;
        case 7:
            $sql = "delete from presecado where codigofila=".$id;
            $sqluser = "select IdUsuario from presecado where codigofila=".$id;
            break;
        case 8:
            $sql = "delete from secado where codigofila=".$id;
            $sqluser = "select IdUsuario from secado where codigofila=".$id;
            break;
        case 9:
            $sql = "delete from harina where ID=".$id;
            $sqluser = "select IdUsuario from harina where ID=".$id;
            break;
    endswitch;
    if($rol==1):
        Yii::app()->db->createCommand($sql)->execute();
    elseif($rol ==2):
        $user = Yii::app()->db->createCommand($sqluser)->queryScalar();
        if($user == Yii::app()->user->id || $user == null):
            Yii::app()->db->createCommand($sql)->execute();
        else:
            $result =0;
        endif;
    endif;
    echo $result;
}
}
