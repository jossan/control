<?php

class ParametrosController extends Controller
{
/**
* @var string the default layout for the views. Defaults to '//layouts/column2', meaning
* using two-column layout. See 'protected/views/layouts/column2.php'.
*/
public $layout='//layouts/column2SinOperaciones';

/**
* @return array action filters
*/
public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}

/**
* Specifies the access control rules.
* This method is used by the 'accessControl' filter.
* @return array access control rules
*/
public function accessRules()
{
return array(
array('allow',  // allow all users to perform 'index' and 'view' actions
'actions'=>array('index','admin','view','coccionrpm','coccionPresioneje','coccionPresionchaq','coccionTemperatura','prensaAmp','prensadoH','licorS','licorG','decantasionTem','decantasionHum','decantasionaguaS','decantasionaguaG','refinacionTem','aceiteHum','aceiteSol','aceiteA','tratamientoAlimTem','tratamientoAlimSol','tratamientoVahTemp','tratamientoVacPeso','tratamientoTemperatura1','tratamientoTemperatura2','tratamientoTemperatura3','tratamientoSalSol','presecadoMinHum','presecadoMaxPresion','presecadoAmp','presecadoPresEje','presecadoPresChq','presecadoTempSrc','presecadoHumedad','secadoMinHum','secadoMaxPresion','secadoAmp','secadoPresPsi','secadoSrc','secadoHumedad','general','anual','anuala1','anuala2','anualf1','anualf2'),
'users'=>CHtml::listData(Usuarios::model()->findAll("TipoRolID=1"),'Usuario','Usuario'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}

/**
* Displays a particular model.
* @param integer $id the ID of the model to be displayed
*/
public function actionView($id)
{
$this->render('view',array(
'model'=>$this->loadModel($id),
));
}

/**
* Creates a new model.
* If creation is successful, the browser will be redirected to the 'view' page.
*/
public function actionCreate()
{
$model=new Parametros;

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Parametros']))
{
$model->attributes=$_POST['Parametros'];
if($model->save())
$this->redirect(array('view','id'=>$model->ID));
}

$this->render('create',array(
'model'=>$model,
));
}

/**
* Updates a particular model.
* If update is successful, the browser will be redirected to the 'view' page.
* @param integer $id the ID of the model to be updated
*/
public function actionUpdate($id)
{
$model=$this->loadModel($id);

// Uncomment the following line if AJAX validation is needed
// $this->performAjaxValidation($model);

if(isset($_POST['Parametros']))
{
$model->attributes=$_POST['Parametros'];
if($model->save())
$this->redirect(array('view','id'=>$model->ID));
}

$this->render('update',array(
'model'=>$model,
));
}

/**
* Deletes a particular model.
* If deletion is successful, the browser will be redirected to the 'admin' page.
* @param integer $id the ID of the model to be deleted
*/
public function actionDelete($id)
{
if(Yii::app()->request->isPostRequest)
{
// we only allow deletion via POST request
$this->loadModel($id)->delete();

// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
if(!isset($_GET['ajax']))
$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
}
else
throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
}

/**
* Lists all models.
*/
public function actionIndex()
{
$dataProvider=new CActiveDataProvider('Parametros');
$this->render('index',array(
'dataProvider'=>$dataProvider,
));
}

/**
* Manages all models.
*/
public function actionAdmin()
{
    $anio2 =date('Y')-2;
$anio1 =date('Y')-1;
$actual = date('Y');
$aniof1 =date('Y')+1;
$aniof2 =date('Y')+2;
$model=new Parametros('search');
$model->unsetAttributes();  // clear any default values
if(isset($_GET['Parametros']))
$model->attributes=$_GET['Parametros'];

$valora =Yii::app()->db->createCommand("Select Objetivo from rendimientosemanal where Anio=".$actual." LIMIT 1")->queryScalar();
$valora1 =Yii::app()->db->createCommand("Select Objetivo from rendimientosemanal where Anio=".$anio1." LIMIT 1")->queryScalar();
$valora2 =Yii::app()->db->createCommand("Select Objetivo from rendimientosemanal where Anio=".$anio2." LIMIT 1")->queryScalar();
$valorf1 =Yii::app()->db->createCommand("Select Objetivo from rendimientosemanal where Anio=".$aniof1." LIMIT 1")->queryScalar();
$valorf2 =Yii::app()->db->createCommand("Select Objetivo from rendimientosemanal where Anio=".$aniof2." LIMIT 1")->queryScalar();

$this->render('admin',array(
'model'=>$model,'anio2'=>$anio2,'anio1'=>$anio1,'aniof2'=>$aniof2,'aniof1'=>$aniof1,'actual'=>$actual,'valora'=>$valora,'valora1'=>$valora1,'valora2'=>$valora2,'valorf1'=>$valorf1,'valorf2'=>$valorf2
));
}

/**
* Returns the data model based on the primary key given in the GET variable.
* If the data model is not found, an HTTP exception will be raised.
* @param integer the ID of the model to be loaded
*/
public function loadModel($id)
{
$model=Parametros::model()->findByPk($id);
if($model===null)
throw new CHttpException(404,'The requested page does not exist.');
return $model;
}

/**
* Performs the AJAX validation.
* @param CModel the model to be validated
*/
protected function performAjaxValidation($model)
{
if(isset($_POST['ajax']) && $_POST['ajax']==='parametros-form')
{
echo CActiveForm::validate($model);
Yii::app()->end();
}
}
public function actionCoccionrpm(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set CoccionMinRmp = '.$c1.',CoccionMaxRmp = '.$c2.',CoccionRpmAlertaNaranja = '.$c3.',CoccionRpmAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionCoccionPresioneje(){
    $c5= $_GET['c5']; $c6=$_GET['c6'];/*Recibo los valores*/
    $c7= $_GET['c7']; $c8=$_GET['c8'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set CoccionMinPresionEje = '.$c5.',CoccionMaxPresionEje = '.$c6.',CoccionPresionEjeAlertaNaranja = '.$c7.',CoccionPresionEjeAlertaRoja = '.$c8)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionCoccionPresionchaq(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set CoccionMinPresionChaqueta = '.$c1.',CoccionMaxPresionChaqueta = '.$c2.',CoccionPresionChaquetaAlertaNaranja = '.$c3.',CoccionPresionChaquetaAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}
public function actionCoccionTemperatura(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set CoccionMinTemperatura = '.$c1.',CoccionMaxTemperatura = '.$c2.',CoccionPresionTemperaturaAlertaNaranja = '.$c3.',CoccionPresionTemperaturaAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionPrensaAmp(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set PrensadoMinAmperaje = '.$c1.',PrensadoMaxAmperaje = '.$c2.',PrensadoAmperajeAlertaNaranja = '.$c3.',PrensadoAmperajeAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}
public function actionPrensadoH(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set PrensadoMinPorcentajeHumedad = '.$c1.',PrensadoMaxPorcentajeHumedad = '.$c2.',PrensadoPorcentajeHumedadAlertaNaranja = '.$c3.',PrensadoPorcentajeHumedadAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionLicorS(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set LicorMinPorcentajeSolidos = '.$c1.',LicorMaxPorcentajeSolidos = '.$c2.',LicorPorcentajeSolidosAlertaNaranja = '.$c3.',LicorPorcentajeSolidosAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}
public function actionLicorG(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set LicorMinPorcentajeGrasas = '.$c1.',LicorMaxPorcentajeGrasas = '.$c2.',LicorPorcentajeGrasasAlertaNaranja = '.$c3.',LicorPorcentajeGrasasAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionDecantasionTem(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set DecantacionMinAlimentacionTemperatura = '.$c1.',DecantacionMaxAlimentacionTemperatura = '.$c2.',DecantacionAlimentacionTemperaturaAlertaNaranja = '.$c3.',DecantacionAlimentacionTemperaturaAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionDecantasionHum(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set DecantacionMinAlimentacionPorcentajeHumedad = '.$c1.',DecantacionMaxAlimentacionPorcentajeHumedad = '.$c2.',DecantacionAlimentacionPorcentajeHumedadAlertaNaranja = '.$c3.',DecantacionAlimentacionPorcentajeHumedadAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionDecantasionaguaS(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set DecantacionMinAguaColaPorcentajeSolidos = '.$c1.',DecantacionMaxAguaColaPorcentajeSolidos = '.$c2.',DecantacionAguaColaPorcentajeSolidosAlertaNaranja = '.$c3.',DecantacionAguaColaPorcentajeSolidosAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}
public function actionDecantasionaguaG(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set DecantacionMinAguaColaPorcentajeGrasas = '.$c1.',DecantacionMaxAguaColaPorcentajeGrasas = '.$c2.',DecantacionAguaColaPorcentajeGrasasAlertaNaranja = '.$c3.',DecantacionAguaColaPorcentajeGrasasAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionRefinacionTem(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set RefinacionMinAlimentacionTemperatura = '.$c1.',RefinacionMaxAlimentacionTemperatura = '.$c2.',RefinacionTemperaturaAlertaNaranja = '.$c3.',RefinacionTemperaturaAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionAceiteHum(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set RefinacionMinAceitePorcentajeHumedad = '.$c1.',RefinacionMaxAceitePorcentajeHumedad = '.$c2.',RefinacionAceitePorcentajeHumedadAlertaNaranja = '.$c3.',RefinacionAceitePorcentajeHumedadAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionAceiteSol(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set RefinacionMinAceitePorcentajeSolidos = '.$c1.',RefinacionMaxAceitePorcentajeSolidos = '.$c2.',RefinacionAceitePorcentajeSolidosAlertaNaranja = '.$c3.',RefinacionAceitePorcentajeSolidosAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionAceiteA(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set RefinacionMinAceitePorcentajeAcidez = '.$c1.',RefinacionMaxAceitePorcentajeAcidez = '.$c2.',RefinacionAceitePorcentajeAcidezAlertaNaranja = '.$c3.',RefinacionAceitePorcentajeAcidezAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}
public function actionTratamientoAlimTem(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set TratamientoMinAlimentacionAguaColaTemperatura = '.$c1.',TratamientoMaxAlimentacionAguaColaTemperatura = '.$c2.',TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja = '.$c3.',TratamientoAlimentacionAguaColaTemperaturaAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionTratamientoAlimSol(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set TratamientoMinAlimentacionAguaColaPorcentajeSolidos = '.$c1.',TratamientoMaxAlimentacionAguaColaPorcentajeSolidos = '.$c2.',TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja = '.$c3.',TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionTratamientoVahTemp(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set TratamientoMinVahTemperatura = '.$c1.',TratamientoMaxVahTemperatura = '.$c2.',TratamientoVahTemperaturaAlertaNaranja = '.$c3.',TratamientoVahTemperaturaAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionTratamientoVacPeso(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set TratamientoMinVacPeso = '.$c1.',TratamientoMaxVacPeso = '.$c2.',TratamientoVacPesoAlertaNaranja = '.$c3.',TratamientoVacPesoAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionTratamientoTemperatura1(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set TratamientoMinTemperatura1 = '.$c1.',TratamientoMaxTemperatura1 = '.$c2.',TratamientoTemperatura1AlertaNaranja = '.$c3.',TratamientoTemperatura1AlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionTratamientoTemperatura2(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set TratamientoMinTemperatura2 = '.$c1.',TratamientoMaxTemperatura2 = '.$c2.',TratamientoTemperatura2AlertaNaranja = '.$c3.',TratamientoTemperatura2AlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionTratamientoTemperatura3(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set TratamientoMinTemperatura3 = '.$c1.',TratamientoMaxTemperatura3 = '.$c2.',TratamientoTemperatura3AlertaNaranja = '.$c3.',TratamientoTemperatura3AlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionTratamientoSalSol(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set TratamientoMinSalidaPorcentajeSolidos = '.$c1.',TratamientoMaxSalidaPorcentajeSolidos = '.$c2.',TratamientoSalidaPorcentajeSolidosAlertaNaranja = '.$c3.',TratamientoSalidaPorcentajeSolidosAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionPresecadoMinHum(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set PresecadoMinMinimoPorcentajeHumedad = '.$c1.',PresecadoMaxMinimoPorcentajeHumedad = '.$c2.',PresecadoMinimoPorcentajeHumedadAlertaNaranja = '.$c3.',PresecadoMinimoPorcentajeHumedadAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionPresecadoMaxPresion(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set PresecadoMinMaximoPresion = '.$c1.',PresecadoMaxMaximoPresion = '.$c2.',PresecadoMaximoPresionAlertaNaranja = '.$c3.',PresecadoMaximoPresionAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionPresecadoAmp(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set PresecadoMinAmperaje = '.$c1.',PresecadoMaxAmperaje = '.$c2.',PresecadoAmperajeAlertaNaranja = '.$c3.',PresecadoAmperajeAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionPresecadoPresEje(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set PresecadoMinPresionEje = '.$c1.',PresecadoMaxPresionEje = '.$c2.',PresecadoPresionEjeAlertaNaranja = '.$c3.',PresecadoPresionEjeAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionPresecadoPresChq(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set PresecadoMinPresionChaqueta = '.$c1.',PresecadoMaxPresionChaqueta = '.$c2.',PresecadoPresionChaquetaAlertaNaranja = '.$c3.',PresecadoPresionChaquetaAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionPresecadoTempSrc(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set PresecadoMinSCR = '.$c1.',PresecadoMaxSCR = '.$c2.',PresecadoSCRAlertaNaranja = '.$c3.',PresecadoSCRAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionPresecadoHumedad(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set PresecadoMinPorcentajeHumedad = '.$c1.',PresecadoMaxPorcentajeHumedad = '.$c2.',PresecadoPorcentajeHumedadAlertaNaranja = '.$c3.',PresecadoPorcentajeHumedadAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionSecadoMinHum(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set RotatuboMinMinimoPorcentajeHumedad = '.$c1.',RotatuboMaxMinimoPorcentajeHumedad = '.$c2.',RotatuboMinimoPorcentajeHumedadAlertaNaranja = '.$c3.',RotatuboMinimoPorcentajeHumedadAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionSecadoMaxPresion(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set RotatuboMinMaximoPresion = '.$c1.',RotatuboMaxMaximoPresion = '.$c2.',RotatuboMaximoPresionAlertaNaranja = '.$c3.',RotatuboMaximoPresionAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionSecadoAmp(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set RotatuboMinAmperaje = '.$c1.',RotatuboMaxAmperaje = '.$c2.',RotatuboAmperajeAlertaNaranja = '.$c3.',RotatuboAmperajeAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionSecadoPresPsi(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set RotatuboMinPresionEje = '.$c1.',RotatuboMaxPresionEje = '.$c2.',RotatuboPresionEjeAlertaNaranja = '.$c3.',RotatuboPresionEjeAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionSecadoSrc(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set RotatuboMinSRC = '.$c1.',RotatuboMaxSRC = '.$c2.',RotatuboSRCAlertaNaranja = '.$c3.',RotatuboSRCAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionSecadoHumedad(){
    $c1= $_GET['c1']; $c2=$_GET['c2'];/*Recibo los valores*/
    $c3= $_GET['c3']; $c4=$_GET['c4'];
    $resul = 0;/*Valor a retornar -> falso*/
    $cont = 0;/*Declaro contado en 0, para verificar si todos los datos se ingresan corretamente*/
    $cont = Yii::app()->db->createCommand( 'update Parametros set RotatuboMinHumedad = '.$c1.',RotatuboMaxHumedad = '.$c2.',RotatuboHumedadAlertaNaranja = '.$c3.',RotatuboHumedadAlertaRoja = '.$c4)->execute();
    /*si los campos afectado fueron 4 entonces estamos bien
     * puse 4 xq le envio 4 parametros a actualizar*/
    if($cont>=4){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionGeneral(){
    $valor=$_GET['gene'];
    $resul = 0;
    $con =0;
    $con =Yii::app()->db->createCommand( 'update RendimientoSemanal set Objetivo='.$valor)->execute();
    if($con>=1){$resul=1;}
    print $resul;/*Retorno "0" o "1"*/
}
public function actionAnual(){
$resul = 0;
    $con =0;
$actual = date('Y');
$vactual=$_GET['anioactual'];
        
$con =Yii::app()->db->createCommand( 'update RendimientoSemanal set Objetivo='.$vactual.' where Anio='.$actual)->execute();
if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
             
}

public function actionAnuala1(){
$resul = 0;
    $con =0;
$anio1 =date('Y')-1;
$vanio1=$_GET['anioa1'];
        
$con =Yii::app()->db->createCommand( 'update RendimientoSemanal set Objetivo='.$vanio1.' where Anio='.$actual)->execute();
             if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/
}

public function actionAnuala2(){
    $resul = 0;
    $con =0;
 $anio2 =date('Y')-2;
 $vanio2=$_GET['anioa2'];
$con =Yii::app()->db->createCommand( 'update RendimientoSemanal set Objetivo='.$vanio2.' where Anio='.$actual)->execute();
      if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/       
}

public function actionAnualf1(){
    $resul = 0;
    $con =0;
$aniof1 =date('Y')+1;
$vaniof1=$_GET['aniof1'];
      
$con =Yii::app()->db->createCommand( 'update RendimientoSemanal set Objetivo='.$vaniof1.' where Anio='.$actual)->execute();
       if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/      
}
public function actionAnualf2(){
$resul = 0;
    $con =0;
$aniof2 =date('Y')+2;
$vaniof2= $_GET['aniof2'];
        
$con =Yii::app()->db->createCommand( 'update RendimientoSemanal set Objetivo='.$vaniof2.' where Anio='.$actual)->execute();
     if($con>=1){$resul=1;}
    
    print $resul;/*Retorno "0" o "1"*/        
}

}
