<?php

class ControlSemanalController extends Controller
{
    
    public function filters()
{
return array(
'accessControl', // perform access control for CRUD operations
);
}
    public function accessRules()
{
return array(
array('allow', // allow admin user to perform 'admin' and 'delete' actions
'actions'=>array('index','tabla','produccionSemana','Actualizartabla','VerpdfSemanal','generarExcelprodSemanal','grafico','prueba','guardarimg','guardarimg2','eficienciaPlanta','compMateriaPrimaProcesada','ratioconsumo','variacionHumedad','final'),
'users'=>array('@'),
),
array('deny',  // deny all users
'users'=>array('*'),
),
);
}
	public function actionIndex()
	{
            if(isset($_GET['f1']) and isset($_GET['f2'])):
                $f1= $_GET['f1'];
                $f2= $_GET['f2'];
                $anio = date("Y", strtotime($f1));
                $mes = date("m", strtotime($f1));
            else:    
                $f1= date('Y-m-').'01';
                $f2= date('Y-m-t');
                $anio = date("Y");
                $mes = date("m");
                
            endif;
            $this->render('index',array('f1'=>$f1,'f2'=>$f2,'mes'=>$mes,'anio'=>$anio));
	}
        
        public function actionGrafico(){
            if(isset($_GET['anio']) and isset($_GET['mes1'])and isset($_GET['mes2'])):
                $anio = $_GET['anio'];$anio1 = $_GET['anio1'];$mes = $_GET['mes1'];$mes2= $_GET['mes2'];
            else:
                $anio = date('Y');$anio1 = date('Y');$mes = date('m');$mes2 =date('m');
            endif;
            
            $fechaini = "$anio1-$mes-01";$fehafinini = "$anio-$mes2-01";$fechafin = "$anio-$mes2-".date('t',  strtotime($fehafinini));
            
$arrayValor = Array();$arraySemana = Array();$arrayObjetivo = Array();

$sql = Yii::app()->db->createCommand("select distinct(rendimientosemanal.semana), Valor from rendimientosemanal join periodo on periodo.Semana = rendimientosemanal.Semana and Year(periodo.Fecha) = rendimientosemanal.Anio Where Fecha between '".$fechaini."' and '".$fechafin."' and YEAR(Fecha) between ".$anio1." and ".$anio)->queryAll();
$sqlobjetivo = Yii::app()->db->createCommand("select distinct(rendimientosemanal.semana), Objetivo from rendimientosemanal join periodo on periodo.Semana = rendimientosemanal.Semana and Year(periodo.Fecha) = rendimientosemanal.Anio Where Fecha between '".$fechaini."' and '".$fechafin."' and YEAR(Fecha) between ".$anio1." and ".$anio)->queryAll();
$sql2 =Yii::app()->db->createCommand("Select Distinct Semana from Periodo Where Fecha between '".$fechaini."' and '".$fechafin."' and YEAR(Fecha) between ".$anio1." and ".$anio)->queryAll();
$sql3 = Yii::app()->db->createCommand("Select Sum(MPProcesada) as SumaProcesada from Periodo Where Fecha between '".$fechaini."' and '".$fechafin."' and YEAR(Fecha) between ".$anio1." and ".$anio." group by Semana")->queryAll();
$sql4 = Yii::app()->db->createCommand("Select Sum(SacosProducidos) as SumaSacos from Periodo Where Fecha between '".$fechaini."' and '".$fechafin."' and YEAR(Fecha) between ".$anio1." and ".$anio." group by Semana")->queryAll();
foreach ($sql as $row):
    $arrayValor[] = (float)$row['Valor'];
endforeach;
foreach ($sqlobjetivo as $row):
    $arrayObjetivo[] = (float)$row['Objetivo'];
endforeach;
foreach ($sql2 as $row):
    $arraySemana[] = (float)$row['Semana'];
endforeach;
            $this->render('grafico',array('arrayObjetivo'=>$arrayObjetivo,'arrayValor'=>$arrayValor,'arraySemana'=>$arraySemana,'sql'=>$sql,'sql2'=>$sql2,'sql3'=>$sql3,'sql4'=>$sql4,'anio'=>$anio,'mes'=>$mes,'mes2'=>$mes2,'sqlobjetivo'=>$sqlobjetivo,'anio1'=>$anio1));
        }
        
        public function actionEficienciaPlanta(){
            $this->render('eficienciaPlanta');
        }
        public function actionCompMateriaPrimaProcesada(){
            if(isset($_GET['anio']) and isset($_GET['mes1'])and isset($_GET['mes2'])):
            $anio = $_GET['anio'];$anio1 = $_GET['anio1'];$mes = $_GET['mes1'];$mes2= $_GET['mes2'];
            $fechaini = "$anio1-$mes-01";$fehafinini = "$anio-$mes2-01";$fechafin = "$anio-$mes2-".date('t',  strtotime($fehafinini));
            else:
            $anio = date('Y');$anio1 = date('Y');$mes = date('m');$mes2 =date('m');
            endif;
            $fechaini = "$anio1-$mes-01";$fehafinini = "$anio-$mes2-01";$fechafin = "$anio-$mes2-".date('t',  strtotime($fehafinini));
            $arraySemana = Array();$rendimiento = Array();
            
            $sql = Yii::app()->db->createCommand("select distinct(rendimientosemanal.semana), Valor from rendimientosemanal join periodo on periodo.Semana = rendimientosemanal.Semana and Year(periodo.Fecha) = rendimientosemanal.Anio Where Fecha between '".$fechaini."' and '".$fechafin."' and YEAR(Fecha) between ".$anio1." and ".$anio)->queryAll();
            $semana =Yii::app()->db->createCommand("Select Distinct Semana from Periodo Where Fecha between '".$fechaini."' and '".$fechafin."' and YEAR(Fecha) between ".$anio1." and ".$anio)->queryAll();
            foreach ($semana as $row):
    $arraySemana[] = (float)$row['Semana'];
endforeach;
foreach ($sql as $row):
    $rendimiento[] = (float)$row['Valor'];
endforeach;

            $this->render('compMateriaPrimaProcesada',array('fechaini'=>$fechaini,'fechafin'=>$fechafin,'rendimiento'=>$rendimiento,'arraySemana'=>$arraySemana,'anio'=>$anio,'mes'=>$mes,'mes2'=>$mes2,'anio1'=>$anio1));
        }
       
public function actionRatioconsumo(){
            $this->render('ratioConsumo');
        }
        
        public function actionVariacionHumedad(){
            if(isset($_GET['anio']) and isset($_GET['mes1'])and isset($_GET['mes2'])):
            $anio = $_GET['anio'];$anio1 = $_GET['anio1'];$mes = $_GET['mes1'];$mes2= $_GET['mes2'];
            $fechaini = "$anio1-$mes-01";$fehafinini = "$anio-$mes2-01";$fechafin = "$anio-$mes2-".date('t',  strtotime($fehafinini));
            else:
            $anio = date('Y');$anio1 = date('Y');$mes = date('m');$mes2 =date('m');
            endif;
            $fechaini = "$anio1-$mes-01";$fehafinini = "$anio-$mes2-01";$fechafin = "$anio-$mes2-".date('t',  strtotime($fehafinini));
            $porcentajearr = Array();$arraySemana =Array(); $aux = Array();
            
            $semana =Yii::app()->db->createCommand("Select Distinct Semana from Periodo Where Fecha between '".$fechaini."' and '".$fechafin."' and YEAR(Fecha) between ".$anio1." and ".$anio)->queryAll();
            $semanayporcentaje =Yii::app()->db->createCommand("select ((Sum(PorcentajeHumedad))/(Count(PorcentajeHumedad))) as porcentaje, Week(Fecha,1) as Semana from harina where Fecha between '".$fechaini."' and '".$fechafin."'  group by Week(Fecha,1) order by Fecha")->queryAll();
            foreach ($semana as $row):
            $arraySemana[] = (float)$row['Semana'];
            $porcentajearr[$row['Semana']] = 0;
            endforeach;
            foreach ($semanayporcentaje as $row):
                $porcentajearr[$row['Semana']] = (float)$row['porcentaje'];
            endforeach;
            foreach ($porcentajearr as $key=>$value):
            $aux[]=$value;
                
            endforeach;

            $this->render('variacionHumedad',array('arraySemana'=>$arraySemana,'arrayPorcentaje'=>$aux,'anio'=>$anio,'mes'=>$mes,'mes2'=>$mes2,'anio1'=>$anio1));
            
        }
        public function actionFinal(){
            if(isset($_GET['anio']) and isset($_GET['mes1'])and isset($_GET['mes2'])):
            $anio = $_GET['anio'];$anio1 = $_GET['anio1'];$mes = $_GET['mes1'];$mes2= $_GET['mes2'];
            $fechaini = "$anio1-$mes-01";$fehafinini = "$anio-$mes2-01";$fechafin = "$anio-$mes2-".date('t',  strtotime($fehafinini));
            else:
            $anio = date('Y');$anio1 = date('Y');$mes = date('m');$mes2 =date('m');
            endif;
            $fechaini = "$anio1-$mes-01";$fehafinini = "$anio-$mes2-01";$fechafin = "$anio-$mes2-".date('t',  strtotime($fehafinini));
            $arraympprocesada = Array();$arraySemana =Array(); $arraytnproducida = Array();$aux=Array();$acumulada=Array();
            $mpproce=0;$tnprodu=0;
            $semana =Yii::app()->db->createCommand("Select Distinct Semana from Periodo Where Fecha between '".$fechaini."' and '".$fechafin."' and YEAR(Fecha) between ".$anio1." and ".$anio)->queryAll();
            $mpprocesadaytn =Yii::app()->db->createCommand("select Sum(MPProcesada) as mpprocesada,((Sum(MPProcesada) * Valor)/20) as tnproducida from periodo join rendimientoSemanal on periodo.Semana = rendimientoSemanal.Semana and Year(Fecha) = rendimientoSemanal.Anio where periodo.Fecha between '".$fechaini."' and '".$fechafin."' group by rendimientoSemanal.Anio,rendimientoSemanal.Semana order by periodo.Fecha")->queryAll();
            foreach ($semana as $row):
            $arraySemana[] = (float)$row['Semana'];
            $acumulada[$row['Semana']] = '';
            endforeach;
            $arraySemana[]= '. acumulada';
            foreach ($mpprocesadaytn as $row):
            $arraympprocesada[] = (float)$row['mpprocesada'];
            $mpproce =$mpproce+(float)$row['mpprocesada'];
            $arraytnproducida[]=(float)$row['tnproducida'];
            $tnprodu =$tnprodu+(float)$row['tnproducida'];
            endforeach;
            $arraympprocesada[] = $mpproce;
            foreach ($acumulada as $key=>$value):
            $aux[]=$value;
            endforeach;
            $aux[]=$tnprodu;
            $this->render('final',array('acumulada'=>$aux,'arraympprocesada'=>$arraympprocesada,'arraytnproducida'=>$arraytnproducida,'arraySemana'=>$arraySemana,'anio'=>$anio,'mes'=>$mes,'mes2'=>$mes2,'anio1'=>$anio1));
            
        }
        

	// Uncomment the following methods and override them if needed
	/*
         * items table table-striped table-bordered table-condensed
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
        public function actionTabla($a,$m)
	{
            $fechaini = "$a-$m-01";
            $fechafin = "$a-$m-".date('t',  strtotime($fechaini));
            //echo $fechaini.' . '.$fechafin;
            echo $this->tabladatos($fechaini,$fechafin);
	}
        
        public function actionProduccionSemana()
	{
            if(isset($_GET['fechaini']) && isset($_GET['fechafin'])):
                $fechaini=$_GET['fechaini'];
                $fechafin=$_GET['fechafin'];
            else:
                $fechaini=date('Y-m-').'01';
                $fechafin=date('Y-m-t');
            endif;

            $this->render('Rango_Semanal',array('f1'=>$fechaini,'f2'=>$fechafin));
	}
        public function tabladatos($f1,$f2){
            /*CARGAR DATOS*/
            $periodo = Periodo::model()->findAll("Fecha BETWEEN '".$f1."' AND '".$f2."';");
            //$especie=Especie::model()->findAll('Estado=1');
            $sql ="select Distinct(TipoMateriaPrima) from [InforFish.IngreMatPri] where TipoMateriaPrima is not NULL and TipoMateriaPrima!='' order by TipoMateriaPrima";
            $especie = Yii::app()->sqldb->createCommand($sql)->queryAll();
            $clientes= Clientes::model()->findAll('Estado=1 order by Exportacion Desc, Nombre');
            $cliventa=new CDbCriteria();
            $cliventa->select="*";
            $cliventa->alias="t1";
            $cliventa->with = array ('periodo','cliente');
            $cliventa->addBetweenCondition('periodo.Fecha', $f1, $f2);
            $cliventa->order = 'PeriodoID, cliente.Exportacion Desc,Nombre';
            $ptventa = PTVenta::model()->findAll($cliventa);
            $anio = strftime("%Y", strtotime($f1));
            $sql = "select count(*) as total from Periodo where YEAR(Fecha)=".$anio." and Fecha between '".$f1."' and '".$f2."' group by Semana order by Semana";
            $modelPeriodo = Yii::app()->db->createCommand($sql)->queryColumn();
            $totalClientes = count($clientes);
            $contadorPTVenta=0;
            $tsemana = -1;
            $sql ="select ID, Fecha,EspecieID,IFNULL(sum(PesoTonelada),0)as Peso from Recepcion where RecepcionID is not null and Fecha between '".$f1."' and '".$f2."' group by Fecha,EspecieID order by ID";
            $recepcion = Yii::app()->db->createCommand($sql)->queryAll();
            $recep = array();
            foreach($recepcion as $r):
                $recep[$r['Fecha']][$r['EspecieID']] = $r['Peso'];
            endforeach;
            $peri=0;
            /*Fin de cargar datos*/

            /*COMENZAR TABLA*/
            $tabla = "";
            $tabla = $tabla.'<table id="gridedi" class="items table table-bordered" ><tr >';
            /*PERIODO*/
            $tabla = $tabla.'<th style="background:Turquoise;">FECHA</th><th style="background:Turquoise;">DÍA</th><th style="background:Turquoise;">SEM</th><th></th>';
            /*materia prima*/
            foreach($especie as $row){$tabla = $tabla.'<th class="mp" style="background:Turquoise;" >'.$row['TipoMateriaPrima'].'</th>';}
            $tabla = $tabla.'<th style="background:Turquoise;"><a href="javascript:visible(\'.mp\',visiblemp);">MP_PROCESADA<a></th>';
            $tabla = $tabla.'<th class="tr"  style="background:NavajoWhite;">INICIO_PRODUCCIÓN<br >Cocinador</th>'
                    . '<th style="background:NavajoWhite;" class="tr" >FECHA_FIN PRODUCCIÓN</th>'
                    . '<th style="background:NavajoWhite;" class="tr" >FIN_PRODUCCIÓN<BR>Cocinador</th>'
                    . '<th style="background:NavajoWhite;" class="tr" >PARADA NO_PROGRAMADA</th>'
                    . '<th style="background:NavajoWhite;" class="tr" >PARADA_PROGRAMADA</th>'
                    . '<th style="background:NavajoWhite;" class="tr" >PARADA_TERCEROS</th>'
                    . '<th style="background:NavajoWhite;"><a href="javascript:visible(\'.tr\',visibletr)" >T.R. TRABAJO</a></th>';
            foreach($clientes as $row):
                if($row->Exportacion):
                $tabla = $tabla.'<th class="sp"  style="background:CornflowerBlue;">'.$row->Nombre.'</th>';
                else:
                $tabla = $tabla.'<th class="sp"  style="background:DarkGray;">'.$row->Nombre.'</th>';
                endif;
            endforeach;
            $tabla = $tabla.'<th style="background:LightSalmon;"><a href="javascript:visible(\'.sp\', visiblesp)"> SACOS PRODUCIDOS<a></th>';
            $tabla = $tabla.'<th style="background:Peru;">Sacos Humedos</th>';
            $tabla = $tabla.'<th style="background:LightSalmon;">Calidad</th>';
            $tabla = $tabla.'<th style="background:LightSalmon;">Rendimiento diario</th>';
            $tabla = $tabla.'<th style="background:LightSalmon;">Rendimiento Semanal</th>';
            $tabla = $tabla.'</tr>';
            /*cOPIA*/
            $tabla = $tabla.'<tr width="0px;"><th ></th><th ></th><th ></th><th></th>';
            /*materia prima*/
            foreach($especie as $row){$tabla = $tabla.'<th class="mp" ></th>';}
            $tabla = $tabla.'<th ></th>';
            $tabla = $tabla.'<th class="tr" ></th><th class="tr" ></th><th class="tr" ></th><th class="tr" ></th><th class="tr" ></th><th class="tr" ></th><th></th>';
            foreach($clientes as $row):
                $tabla = $tabla.'<th class="sp" ></th>';
            endforeach;
            $tabla = $tabla.'<th ></th>';
            $tabla = $tabla.'<th ></th>';
            $tabla = $tabla.'<th ></th>';
            $tabla = $tabla.'<th ></th>';
            $tabla = $tabla.'<th ></th>';
            $tabla = $tabla.'</tr>';
            /*fin DE COPIA*/
            
            /*DATOS*/
            foreach ($periodo as $row):
                if($row->Dia=='DO'):
                    $tabla = $tabla.'<tr style="padding:0px;background-color:#EEEEEE;" >';
                else:
                    $tabla = $tabla.'<tr style="padding:0px;" >';
                endif; 
                $tabla = $tabla.'<td style="padding:0px;" >'.$row->Fecha.'</td>';
                $tabla = $tabla.'<td style="padding:0px;">'.$row->Dia.'</td>';
                $tabla = $tabla.'<td style="padding:0px;" id="Semana'.$row->ID.'" >'.$row->Semana.'</td>';
                $tabla =$tabla.'<th style="padding:0px;padding-right:5.5px;" >'.CHtml::image('images/save.png', 'Actualizar', array ('onclick'=>'guardar(this)','rel'=>$row['ID'],'total'=>$totalClientes,'style'=>'margin:0px;')).'</th>';
                $c=0;
                foreach($especie as $espe):
                    if(isset($recep[$row->Fecha][$espe['TipoMateriaPrima']])):
                        $val = $recep[$row->Fecha][$espe['TipoMateriaPrima']];
                    else:
                        $val="0.0";
                    endif;
                    $tabla = $tabla.'<th class="mp" style="padding:0px;" >'.$val.'</th>';
                endforeach;
                $tabla = $tabla.'<th style="padding:0px;" id="MP'.$row->ID.'" >'.$row->MPProcesada.'</th>';
                    $InicioProd=  explode(':', $row->InicioProd);
                    $tabla =$tabla.'<th class="tr"  style="padding:0px;width:130px;"><input  id="inicioprodh'.$row->ID.'" class="hora" size="2" value="'.$InicioProd[0].'"> <input id="inicioprodm'.$row->ID.'" class="minutos" size="2" value="'.$InicioProd[1].'"></th>';
                    if($row->Dia=='DO'):
                        $tabla = $tabla.'<th class="tr" style="padding:0px;" ><input id="fechafin'.$row->ID.'" style="box-shadow:none;text-align:center;background-color:#EEEEEE;" value="'.$row->FechaFinProd.'" class="fechafin"></th>';
                    else:
                        $tabla = $tabla.'<th class="tr" style="padding:0px;" ><input id="fechafin'.$row->ID.'" style="box-shadow:none;text-align:center;" value="'.$row->FechaFinProd.'" class="fechafin"></th>';
                    endif;
                    
                    $FinProd=  explode(':', $row->FinProd);
                    $tabla =$tabla.'<th class="tr"  style="padding:0px;width:130px;"><input  id="finprodh'.$row->ID.'" class="hora" size="2" value="'.$FinProd[0].'"> <input id="finprodm'.$row->ID.'" class="minutos" size="2" value="'.$FinProd[1].'"></th>';
                    $ParadanoProgramada=  explode(':', $row->ParadanoProgramada);
                    $tabla =$tabla.'<th class="tr"  style="padding:0px;width:130px;"><input  id="noprogh'.$row->ID.'" class="hora" size="2" value="'.$ParadanoProgramada[0].'"> <input id="noprogm'.$row->ID.'" class="minutos" size="2" value="'.$ParadanoProgramada[1].'"></th>';
                    
                    $ParadaProgramada=  explode(':', $row->ParadaProgramada);
                    $tabla =$tabla.'<th class="tr"  style="padding:0px;width:130px;"><input  id="progh'.$row->ID.'" class="hora" size="2" value="'.$ParadaProgramada[0].'"> <input id="progm'.$row->ID.'" class="minutos" size="2" value="'.$ParadaProgramada[1].'"></th>';
                    
                    $ParadaTerceros=  explode(':', $row->ParadaTerceros);
                    $tabla =$tabla.'<th class="tr"  style="padding:0px;width:130px;" ><input  id="tercerh'.$row->ID.'" class="hora" size="2" value="'.$ParadaTerceros[0].'"> <input id="tercerm'.$row->ID.'" class="minutos" size="2" value="'.$ParadaTerceros[1].'"></th>';
                    
                $tabla = $tabla.'<th style="padding:0px;" id="TR'.$row->ID.'" >'.$row->TRTrabajo.'</th>';
                for($i = 0;$i<($totalClientes);$i++):
                    $tabla = $tabla.'<td class="sp"  style="padding:0px;" id="valorc'.$i.''.$ptventa[$contadorPTVenta]->PeriodoID.'" >'.$ptventa[$contadorPTVenta]->Valor.'</td>';
                    $contadorPTVenta++;
                endfor;
                $tabla = $tabla.'<th style="padding:0px;" id="SP'.$row->ID.'" >'.$row->SacosProducidos.'</th>';
                $tabla = $tabla.'<td style="padding:0px;" id="sachumed'.$row->ID.'" >'.$row->SacosHumedos.'</td>';
                $tabla = $tabla.'<th style="padding:0px;" id="CA'.$row->ID.'">'.$row->Calidad.'</th>';
                $anio = strftime("%Y", strtotime($row->Fecha));
                $mes = strftime("%m", strtotime($row->Fecha));
                
                $tabla = $tabla.'<th style="padding:0px;" id="RD'.$row->ID.'">'.$row->RendimientoDiario.'</th>';               
                
                
                $fechaini = "$anio-$mes-01";
                $fechafin = "$anio-$mes-".date('t',  strtotime($fechaini));
                //$modelPeriodo = Periodo::model()->findAll("YEAR(Fecha)=".$año." and Semana=".$semana." and Fecha between '".$fechaini."' and '".$fechafin."' group by Semana order by Semana");
                if($tsemana!=$row->Semana):                    
                    $tsemana = $row->Semana;
                    $num = $modelPeriodo[$peri++];
                    $modeloRendimiento = RendimientoSemanal::model()->find("Anio=".$anio." and Semana=".$tsemana);
                    $tabla = $tabla.'<th id="RS'.$tsemana.'" style="padding:0px;vertical-align:middle;" rowspan="'.$num.'">'.$modeloRendimiento->Valor.'</th>';
                endif;

                $tabla = $tabla.'</tr>';
            endforeach;
            $tabla = $tabla.'</table>';
            return $tabla;
        }
        
        public function actionActualizartabla(){
            $idperiodo= $_GET['idper'];
            $inicioprod=$_GET['inicioprod']=='nulo' ? '00:00':$_GET['inicioprod'];
            $fechafin = $_GET['fechafin']=='nulo' ? 'NULL': $_GET['fechafin'];
            $finprod=$_GET['finprod']=='nulo' ? '00:00':$_GET['finprod'];
            $noprog=$_GET['noprog']=='nulo' ? '00:00':$_GET['noprog'];
            $prog=$_GET['prog']=='nulo' ? '00:00':$_GET['prog'];
            $tercer=$_GET['tercer']=='nulo' ? '00:00':$_GET['tercer'];
            $sachumed = $_GET['sachumed']=='nulo'?'0.0':$_GET['sachumed'];
            $valores = $_GET["valores"];
            $total = $_GET["total"];
            $count=0;
             $sql='update Periodo set InicioProd="'.$inicioprod.'",FechaFinProd="'.$fechafin.'" ,FinProd="'.$finprod.'",ParadanoProgramada="'.$noprog.'",ParadaProgramada="'.$prog.'",ParadaTerceros="'.$tercer.'",SacosHumedos='.$sachumed.' where ID='.$idperiodo;
             $count =$count +Yii::app()->db->createCommand($sql)->execute();
            $clientesn= Clientes::model()->findAll('Estado=1 order by Exportacion Desc, Nombre');
             for($i=0;$i<$total;$i++){
             $valor = $valores[$i]=='nulo'?'0.0':$valores[$i];
             $sql='update PTVenta set Valor='.$valor.' where ClienteID='.$clientesn[$i]->Id.' and PeriodoID='.$idperiodo;
             $count =$count +Yii::app()->db->createCommand($sql)->execute();
             }
             
             if($count>0){
                 $count=1;
             }
             //Yii::app()->db->createCommand("call  actualizacalidad(".$idperiodo.")")->execute();
             Yii::app()->db->createCommand("call actualizatiempos (".$idperiodo.")")->execute();
             Yii::app()->db->createCommand("call  RendimientoSemanal(".$idperiodo.")")->execute();
             //Yii::app()->db->createCommand("call  actualizacalidad(".$idperiodo.")")->execute();
             $sql = 'select MPProcesada as Mp, TRTrabajo as Tr, SacosProducidos as Sp, Calidad as Ca, RendimientoDiario as Rd , Valor as Rs from Periodo join RendimientoSemanal on RendimientoSemanal.Anio = YEAR(Periodo.Fecha) and RendimientoSemanal.Semana = Periodo.Semana where Periodo.ID = '.$idperiodo;
             $arr = Yii::app()->db->createCommand($sql)->queryRow();
             $arr ['status']=$count;
             echo json_encode($arr);
        }
        
        
        public function actionVerpdfSemanal($f1,$f2){
         
            if(isset($_GET['f1']) and isset($_GET['f2'])):
                $f1= $_GET['f1'];
                $f2= $_GET['f2'];
             else:
                $f1= date('Y-m-').'01';
                $f2= date('Y-m-t');
             endif;
            $mPDF1 = Yii::app()->ePdf->mpdf('utf-8','A4-L','','',5,5,10,10,5,5,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
            $mPDF1->useOnlyCoreFonts = true;
            $mPDF1->SetTitle("Reporte");
            $mPDF1->SetAuthor("TADEL");
            $mPDF1->showWatermarkText = true;
            $mPDF1->watermark_font = 'DejaVuSansCondensed';
            $mPDF1->watermarkTextAlpha = 0.1;
            $mPDF1->SetDisplayMode('fullpage');
            $mPDF1->WriteHTML($this->renderPartial('pdfRangoSemanal', array('f1'=>$f1,'f2'=>$f2), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
            $mPDF1->Output('pdfSemanal'.date('Y-m-d H:i'),'I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
            exit;
}

public function actionGenerarExcelprodSemanal($f1,$f2){
    
            if(isset($_GET['f1']) and isset($_GET['f2'])):
                $f1= $_GET['f1'];
                $f2= $_GET['f2'];
             else:
                $f1= date('Y-m-').'01';
                $f2= date('Y-m-t');
             endif;
            $contenido = $this->renderPartial("ExcelRangoSemanal", array('f1'=>$f1,'f2'=>$f2), true);
          Yii::app()->request->sendFile('ReporteRangoSemanalExcel'.date('Y-m-d H:m').'.xls', $contenido);
          exit;
            }
            
            public function actionPrueba(){
                $this->render('prueba');
            }
            
            public function actionGuardarimg2(){

$data = str_replace(' ', '+', $_POST['bin_data']);
$data = base64_decode($data);
$fileName = 'images/'.date('ymdhis').'.png';
$im = imagecreatefromstring($data);
 
if ($im !== false) {
     //Save image in the specified location
    imagepng($im, $fileName);
    imagedestroy($im);
    echo $fileName;
            }
            }
            
            public function actionGuardarimg(){
                                
                if(isset($_GET['anio']) and isset($_GET['mes1'])and isset($_GET['mes2'])):
                $anio = $_GET['anio'];
            $mes = $_GET['mes1'];
            $mes2= $_GET['mes2'];
            else:
                $anio = date('Y');
            $mes = 1;
            $mes2 =1;
            endif;
            
            $filename = $_GET['filename'];
            
            $fechaini = "$anio-$mes-01";
            $fehafinini = "$anio-$mes2-01";
                $fechafin = "$anio-$mes2-".date('t',  strtotime($fehafinini));
                
                $sql = Yii::app()->db->createCommand("select distinct(rendimientosemanal.semana), Valor from rendimientosemanal join periodo on periodo.Semana = rendimientosemanal.Semana Where Fecha between '".$fechaini."' and '".$fechafin."' and YEAR(Fecha)=".$anio)->queryAll();
$sqlobjetivo = Yii::app()->db->createCommand("select distinct(rendimientosemanal.semana), Objetivo from rendimientosemanal join periodo on periodo.Semana = rendimientosemanal.Semana Where Fecha between '".$fechaini."' and '".$fechafin."' and YEAR(Fecha)=".$anio)->queryAll();
$sql2 =Yii::app()->db->createCommand("Select Distinct Semana from Periodo Where Fecha between '".$fechaini."' and '".$fechafin."' and YEAR(Fecha)=".$anio)->queryAll();
$sql3 = Yii::app()->db->createCommand("Select Sum(MPProcesada) as SumaProcesada from Periodo Where Fecha between '".$fechaini."' and '".$fechafin."' and YEAR(Fecha)=".$anio." group by Semana")->queryAll();
$sql4 = Yii::app()->db->createCommand("Select Sum(SacosProducidos) as SumaSacos from Periodo Where Fecha between '".$fechaini."' and '".$fechafin."' and YEAR(Fecha)=".$anio." group by Semana")->queryAll();
                   
         $mPDF1 = Yii::app()->ePdf->mpdf('utf-8','A4','','',15,15,35,25,9,9,'P'); //Esto lo pueden configurar como quieren, para eso deben de entrar en la web de MPDF para ver todo lo que permite.
         $mPDF1->useOnlyCoreFonts = true;
         $mPDF1->SetTitle("Reporte");
         $mPDF1->SetAuthor("TADEL");
         //$mPDF1->SetWatermarkText("Pacientes");
         $mPDF1->showWatermarkText = true;
         $mPDF1->watermark_font = 'DejaVuSansCondensed';
         $mPDF1->watermarkTextAlpha = 0.1;
         $mPDF1->SetDisplayMode('fullpage');
         $mPDF1->WriteHTML($this->renderPartial('pdfGrafico', array('filename'=>$filename,'sql'=>$sql,'sql2'=>$sql2,'sql3'=>$sql3,'sql4'=>$sql4,'sqlobjetivo'=>$sqlobjetivo,'fechaini'=>$fechaini,'fechafin'=>$fechafin), true)); //hacemos un render partial a una vista preparada, en este caso es la vista pdfReport
         $mPDF1->Output('RendimientoSemanal'.date('Y-m-d H:i'),'I');  //Nombre del pdf y parámetro para ver pdf o descargarlo directamente.
         exit;
    

            }
}

