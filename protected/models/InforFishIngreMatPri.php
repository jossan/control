<?php

/**
 * This is the model class for table "InforFishIngreMatPri".
 *
 * The followings are the available columns in table 'InforFishIngreMatPri':
 * @property integer $ImgresoMatPriId
 * @property string $TipoDocumento
 * @property string $Fecha
 * @property string $TipoItem
 * @property string $Item
 * @property string $TipoMateriaPrima
 * @property string $PesoIngreso
 * @property string $PesoSalida
 * @property string $CantidadSG
 * @property string $CantidadSGProveedor
 * @property string $RUCProveedor
 * @property string $Comentario
 * @property string $PLacaCamion
 * @property string $GuiaProveedor
 * @property string $Estado
 * @property string $DescripcionItems
 * @property boolean $Completado
 * @property integer $Secuencia
 * @property string $FechaSalida
 * @property string $Descarga
 * @property string $Chofer
 * @property integer $Usuario
 * @property double $Porcentaje
 * @property integer $Linea
 * @property string $TipoPeso
 * @property string $FicheroCam1
 * @property string $FicheroCam2
 * @property string $FicheroCam3
 * @property string $FicheroCam4
 */
class InforFishIngreMatPri extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '[InforFish.IngreMatPri]';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ImgresoMatPriId', 'required'),
			array('ImgresoMatPriId, Secuencia, Usuario, Linea', 'numerical', 'integerOnly'=>true),
			array('Porcentaje', 'numerical'),
			array('TipoDocumento, TipoItem, Item, TipoMateriaPrima, CantidadSGProveedor, RUCProveedor, PLacaCamion, GuiaProveedor, Estado, Descarga, Chofer, TipoPeso', 'length', 'max'=>50),
			array('PesoIngreso, PesoSalida, CantidadSG', 'length', 'max'=>18),
			array('Comentario', 'length', 'max'=>300),
			array('DescripcionItems', 'length', 'max'=>200),
			array('Fecha, Completado, FechaSalida, FicheroCam1, FicheroCam2, FicheroCam3, FicheroCam4', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ImgresoMatPriId, TipoDocumento, Fecha, TipoItem, Item, TipoMateriaPrima, PesoIngreso, PesoSalida, CantidadSG, CantidadSGProveedor, RUCProveedor, Comentario, PLacaCamion, GuiaProveedor, Estado, DescripcionItems, Completado, Secuencia, FechaSalida, Descarga, Chofer, Usuario, Porcentaje, Linea, TipoPeso, FicheroCam1, FicheroCam2, FicheroCam3, FicheroCam4', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ImgresoMatPriId' => 'Imgreso Mat Pri',
			'TipoDocumento' => 'Tipo Documento',
			'Fecha' => 'Fecha',
			'TipoItem' => 'Tipo Item',
			'Item' => 'Item',
			'TipoMateriaPrima' => 'Tipo Materia Prima',
			'PesoIngreso' => 'Peso Ingreso',
			'PesoSalida' => 'Peso Salida',
			'CantidadSG' => 'Cantidad Sg',
			'CantidadSGProveedor' => 'Cantidad Sgproveedor',
			'RUCProveedor' => 'Rucproveedor',
			'Comentario' => 'Comentario',
			'PLacaCamion' => 'Placa Camion',
			'GuiaProveedor' => 'Guia Proveedor',
			'Estado' => 'Estado',
			'DescripcionItems' => 'Descripcion Items',
			'Completado' => 'Completado',
			'Secuencia' => 'Secuencia',
			'FechaSalida' => 'Fecha Salida',
			'Descarga' => 'Descarga',
			'Chofer' => 'Chofer',
			'Usuario' => 'Usuario',
			'Porcentaje' => 'Porcentaje',
			'Linea' => 'Linea',
			'TipoPeso' => 'Tipo Peso',
			'FicheroCam1' => 'Fichero Cam1',
			'FicheroCam2' => 'Fichero Cam2',
			'FicheroCam3' => 'Fichero Cam3',
			'FicheroCam4' => 'Fichero Cam4',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ImgresoMatPriId',$this->ImgresoMatPriId);
		$criteria->compare('TipoDocumento',$this->TipoDocumento,true);
		$criteria->compare('Fecha',$this->Fecha,true);
		$criteria->compare('TipoItem',$this->TipoItem,true);
		$criteria->compare('Item',$this->Item,true);
		$criteria->compare('TipoMateriaPrima',$this->TipoMateriaPrima,true);
		$criteria->compare('PesoIngreso',$this->PesoIngreso,true);
		$criteria->compare('PesoSalida',$this->PesoSalida,true);
		$criteria->compare('CantidadSG',$this->CantidadSG,true);
		$criteria->compare('CantidadSGProveedor',$this->CantidadSGProveedor,true);
		$criteria->compare('RUCProveedor',$this->RUCProveedor,true);
		$criteria->compare('Comentario',$this->Comentario,true);
		$criteria->compare('PLacaCamion',$this->PLacaCamion,true);
		$criteria->compare('GuiaProveedor',$this->GuiaProveedor,true);
		$criteria->compare('Estado',$this->Estado,true);
		$criteria->compare('DescripcionItems',$this->DescripcionItems,true);
		$criteria->compare('Completado',$this->Completado);
		$criteria->compare('Secuencia',$this->Secuencia);
		$criteria->compare('FechaSalida',$this->FechaSalida,true);
		$criteria->compare('Descarga',$this->Descarga,true);
		$criteria->compare('Chofer',$this->Chofer,true);
		$criteria->compare('Usuario',$this->Usuario);
		$criteria->compare('Porcentaje',$this->Porcentaje);
		$criteria->compare('Linea',$this->Linea);
		$criteria->compare('TipoPeso',$this->TipoPeso,true);
		$criteria->compare('FicheroCam1',$this->FicheroCam1,true);
		$criteria->compare('FicheroCam2',$this->FicheroCam2,true);
		$criteria->compare('FicheroCam3',$this->FicheroCam3,true);
		$criteria->compare('FicheroCam4',$this->FicheroCam4,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * @return CDbConnection the database connection used for this class
	 */
	public function getDbConnection()
	{
		return Yii::app()->sqldb;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return InforFishIngreMatPri the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
