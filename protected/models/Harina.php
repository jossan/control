<?php

/**
 * This is the model class for table "Harina".
 *
 * The followings are the available columns in table 'Harina':
 * @property string $ID
 * @property string $NumeroLote
 * @property double $Temperatura
 * @property double $PorcentajeHumedad
 * @property double $AO
 * @property double $Peso
 * @property double $TBVN
 * @property double $PorcentajeProteina
 * @property integer $ClasificacionID
 * @property integer $Estado
 * @property double $SacoProducidos
 *
 * The followings are the available model relations:
 * @property Recepcion $revision
 * @property Clasificacion $clasificacion
 */
class Harina extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Harina';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ClasificacionID, Estado', 'numerical', 'integerOnly'=>true),
			array('Temperatura, PorcentajeHumedad, AO, Peso, TBVN, PorcentajeProteina, SacoProducidos', 'numerical'),
			array('NumeroLote', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, ObservacionID,Fecha,Hora,IdUsuario NumeroLote, Temperatura, PorcentajeHumedad, AO, Peso, TBVN, PorcentajeProteina, ClasificacionID, Estado, SacoProducidos', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'clasi' => array(self::BELONGS_TO, 'Clasificacion', 'ClasificacionID'),
                        'obser' => array(self::BELONGS_TO, 'Observacion', 'ObservacionID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'NumeroLote' => 'Numero Lote',
			'Temperatura' => 'Temperatura',
			'PorcentajeHumedad' => 'Porcentaje Humedad',
			'AO' => 'Ao',
			'Peso' => 'Peso',
			'TBVN' => 'Tbvn',
			'PorcentajeProteina' => 'Porcentaje Proteina',
			'ClasificacionID' => 'Clasificacion',
			'Estado' => 'Estado',
			'SacoProducidos' => 'Saco Producidos',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter  
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID,true);
		$criteria->compare('NumeroLote',$this->NumeroLote,true);
		$criteria->compare('Temperatura',$this->Temperatura);
		$criteria->compare('PorcentajeHumedad',$this->PorcentajeHumedad);
		$criteria->compare('AO',$this->AO);
		$criteria->compare('Peso',$this->Peso);
		$criteria->compare('TBVN',$this->TBVN);
		$criteria->compare('PorcentajeProteina',$this->PorcentajeProteina);
		$criteria->compare('ClasificacionID',$this->ClasificacionID);
		$criteria->compare('Estado',$this->Estado);
		$criteria->compare('SacoProducidos',$this->SacoProducidos);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Harina the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
