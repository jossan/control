<?php

/**
 * This is the model class for table "Cocina".
 *
 * The followings are the available columns in table 'Cocina':
 * @property integer $ID
 * @property string $Nombre
 * @property string $Descripcion
 * @property integer $EstadoCocina
 * @property integer $Estado
 *
 * The followings are the available model relations:
 * @property Coccion[] $coccions
 */
class Cocina extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Cocina';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('Nombre, EstadoCocina, Estado', 'required'),
                    array('Nombre','unique', 'message' => 'Nombre del cocinador ya existe'),
			array('EstadoCocina, Estado', 'numerical', 'integerOnly'=>true),
			array('Nombre', 'length', 'max'=>200),
			array('Descripcion', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, Nombre, Descripcion, EstadoCocina, Estado', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'coccions' => array(self::HAS_MANY, 'Coccion', 'CocinaID'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'Nombre' => 'Nombre',
			'Descripcion' => 'Descripción',
			'EstadoCocina' => 'Estado',
			'Estado' => 'Estado Eliminación',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('Nombre',$this->Nombre,true);
		$criteria->compare('Descripcion',$this->Descripcion,true);
		$criteria->compare('EstadoCocina',$this->EstadoCocina);
		$criteria->compare('Estado',1);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Cocina the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
