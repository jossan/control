<?php
$this->breadcrumbs=array(
	'Prensas'=>array('admin'),
	'Lista',
);

$this->menu=array(

array('label'=>'Crear Prensa','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('prensa-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Prensas</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'prensa-grid',
    'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
//'filter'=>$model,
'columns'=>array(
		//'ID',
		'Nombre',
		'Descripcion',
		array('name'=>'EstadoPrensa',
                    'value'=>'$data->EstadoPrensa?"Activo":"Inactivo"'
                    ),
		//'Estado',
array(
'class'=>'booster.widgets.TbButtonColumn',

),
),
)); ?>
