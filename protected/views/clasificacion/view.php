<?php
$this->breadcrumbs=array(
	'Clasificaciones'=>array('admin'),
	$model->ID,
);

$this->menu=array(
array('label'=>'Lista de Clasificación de Harinas','url'=>array('admin')),
array('label'=>'Crear Clasificación de Harina','url'=>array('create')),
array('label'=>'Actualizar Clasificación de Harina','url'=>array('update','id'=>$model->ID)),
array('label'=>'Borrar Clasificación de Harina','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Esta seguro que desea borrarlo?')),

);
?>

<h2>Ver Clasificación de Harina #<?php echo $model->ID; ?></h2>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'ID',
		'Nombre',
		'Descripcion',
		//'Estado',
),
)); ?>
