<?php
$this->breadcrumbs=array(
	'Clasificación de Harinas'=>array('admin'),
	$model->ID=>array('view','id'=>$model->ID),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Clasificaciones','url'=>array('admin')),
	array('label'=>'Crear Clasificación','url'=>array('create')),
	
	
	);
	?>

	<h2>Actualizar Clasificación de Harina<?php echo $model->ID; ?></h2>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
