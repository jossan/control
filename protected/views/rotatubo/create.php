<?php
$this->breadcrumbs=array(
	'Rotatubos'=>array('admin'),
	'Crear',
);

$this->menu=array(
array('label'=>'Lista de Rotatubo','url'=>array('admin')),

);
?>

<h3>Nuevo Rotatubo</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
