<?php
$this->breadcrumbs=array(
	
	'Lista',
);

$this->menu=array(

array('label'=>'Crear Rotatubo','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('rotatubo-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Rotatubos</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'rotatubo-grid',
    'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
//'filter'=>$model,
'columns'=>array(
		//'ID',
		'Nombre',
		'Descripcion',
    array('name'=>'EstadoRotatubo',
        'value'=>'$data->EstadoRotatubo?"Activo":"Inactivo"',
        ),
		
		//'Estado',
array(
'class'=>'booster.widgets.TbButtonColumn',
    
),
),
)); ?>
