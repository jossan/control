<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="<?php echo Yii::app()->language;?>">
<meta charset="utf-8">
<head>
<title><?php echo CHtml::encode($this->pageTitle); ?></title>
<meta charset="<?php echo Yii::app()->charset;?>">
<link rel="shortcut icon" type="image/x-icon" href="images/5969LogoTADEL.ico">
<link href="css/main.css" type="text/css" rel="stylesheet">
</head>
<body>

<header>
    <!--<div id="logo">IMAGEN TADEL</div>-->
    <?php
$admin = (isset(Yii::app()->user->idrol) and Yii::app()->user->idrol == 1) ? true : false ;
if(!Yii::app()->user->isGuest){
    
$nombre = Usuarios::model()->find('ID='.Yii::app()->user->Id);
$valor = $nombre->NombreCompleto;
}else{
    $valor = 'Opciones';
}

$this->widget(
    'booster.widgets.TbNavbar',
    array(
    'type' => null, // null or 'inverse'
    //'brand' => 'TADEL',
    'brand' => CHtml::image('images/logotadel.png','TADEL_LOGO'),
    'brandUrl' => '#',
    'collapse' => true, // requires bootstrap-responsive.css
    'fixed' => false,
    'fluid' => true,
    'items' => array(
    array(
    'class' => 'booster.widgets.TbMenu',
    'type' => 'navbar',
    'items' => array(
    array('label'=>'Inicio', 'url'=>array('/site/index'), 'icon' => 'home',),
    array('label' => 'Nosotros','url' =>array( '/site/page&view=about'),'icon' => 'briefcase',),
    array(
    'label' => 'Producción',
    'url' => '#',
        'icon' => 'cog',
    'visible'=>!Yii::app()->user->isGuest,
    'items' => array(
    array('label'=>'Control del proceso de producción', 'url'=>array('/control')),
    array('label'=>'Control de producción semanal', 'url'=>array('/controlSemanal')),
    )
    ),
    array(
    'label' => 'Informes',
    'url' => '#',
        'icon' => 'list-alt',
    'visible'=>!Yii::app()->user->isGuest,
    'items' => array(
    array('label'=>'Control de proceso de producción', 'url'=>array('/control/produccionRango')),
        '---',    
    array('label'=>'Control de producción semanal', 'url'=>array('/controlSemanal/produccionSemana')),
        array('label'=>'Rendimiento Semanal', 'url'=>array('/controlSemanal/grafico'),'visible'=>$admin,),
    '---',
        array('label'=>'Eficiencia planta', 'url'=>array('/controlSemanal/eficienciaPlanta'),'visible'=>$admin,),
        array('label'=>'Comp. de la materia prima procesada', 'url'=>array('/controlSemanal/compMateriaPrimaProcesada'),'visible'=>$admin,),
        array('label'=>'Ratio de consumo', 'url'=>array('/controlSemanal/ratioconsumo'),'visible'=>$admin,),
        array('label'=>'Variación de la humedad', 'url'=>array('/controlSemanal/variacionHumedad'),'visible'=>$admin,),
        array('label'=>'Final', 'url'=>array('/controlSemanal/final'),'visible'=>$admin,),
    )
    ),

    array(
    'label' => 'Parámetros',
    'url' => '#'
        
              ,'visible'=>$admin ,
        'icon' => 'tasks',
    'items' => array(
    array('label'=>'Parámetros', 'url'=>array('/parametros/')),
        '---',
        array('label'=>'Valor Objetivo', 'url'=>array('/parametros/admin')),
    '---',
    array('label'=>'Tolva', 'url'=>array('/tolva/admin')),
    array('label'=>'Especie', 'url'=>array('/especie/admin')),
        '---',
    array('label'=>'Clientes', 'url'=>array('/clientes/admin')),
        '---',
    array('label'=>'Clasificación de Harinas', 'url'=>array('/clasificacion/admin')),
    array('label'=>'Observaciones', 'url'=>array('/observacion/admin')),
    )
    ),

            array(
    'label' => 'Equipos',
    'url' => '#'
                ,'visible'=>$admin ,
                'icon' => 'wrench',
    'items' => array(
        array('label'=>'Cocinador', 'url'=>array('/cocina/admin')),
        array('label'=>'Prensa', 'url'=>array('/prensa/admin')),
        array('label'=>'Tricanter', 'url'=>array('/tricanter/admin')),
        array('label'=>'Pulidora', 'url'=>array('/pulidora/admin')),
        array('label'=>'Planta Evaporadora', 'url'=>array('/planta/admin')),
        array('label'=>'Rotadisk', 'url'=>array('/rotadisk/admin')),
        array('label'=>'Rotatubo', 'url'=>array('/rotatubo/admin')),
        ))
        
        ,)
        
        ,),

    array(
    'class' => 'booster.widgets.TbMenu',
    'type' => 'navbar',
    'htmlOptions' => array('class' => 'pull-right'),
    'items' => array(

    array(
    'label' => 'Usuarios',
    'url' => '#'
        
        ,'visible'=>$admin ,'icon' => 'user',
    'items' => array(
    array('label' => 'Crear usuarios', 'url' =>array( '/usuarios/create')),
    array('label' => 'Lista usuarios', 'url' => array('/usuarios/admin')),
        '---',
    
        array('label' => 'Lista  de rol de usuarios', 'url' => array('/usuarioRol/admin')),
    )
    ),
        
        
array(
    'label' =>$valor,
    'url' => '#',
    'visible'=> !Yii::app()->user->isGuest,
     'icon' => 'asterisk',
    'items' => array(
        array('label'=>'Editar perfil', 'url'=>array('/usuarios/update&id='.Yii::app()->user->Id)),
        array('label'=>'Cambiar contraseña', 'url'=>array('/usuarios/Contrasenia&id='.Yii::app()->user->Id)),
        '---',
        array('label'=>'Cerrar sesión('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest,'icon' => 'off',),
        )),        
        array('label'=>'Ingresar', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest,'icon' => 'log-in',),
    ),
    ),
    ),

    )
    );?>
    </header>


<div class="container" id="page">


 <?php if(isset($this->breadcrumbs)):?>
 <?php $this->widget('booster.widgets.TbBreadcrumbs', array(
 'links'=>$this->breadcrumbs,
 )); ?>
 <?php endif?>
 <?php echo $content; ?>
 <hr>
 <footer>
 Copyright &copy; <?php echo date('Y'); ?> <?php echo CHtml::encode(Yii::app()->params['empresa']); ?> | <?php echo CHtml::encode((Yii::app()->name).' '.Yii::app()->params['version']); ?> - All Rights Reserved.<br/>
 TADEL
 </footer>

</div><!-- page -->

</body>
</html>
