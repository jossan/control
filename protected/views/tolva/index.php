<?php
$this->breadcrumbs=array(
	'Tolvas',
);

$this->menu=array(
array('label'=>'Crear Tolva','url'=>array('create')),
array('label'=>'Administrar Tolva','url'=>array('admin')),
);
?>

<h2>Tolvas</h2>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
