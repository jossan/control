<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<?php
?>
 
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'column',
            backgroundColor: '#E4C0C0',
        },
        title: {
            text: 'Ratio de consumo : Bunker (GL / Tn)'
        },
        
         xAxis: {
               labels: {
                format: 'S{value}'
                
            },
            categories: ['1', '2', '3', '4', '5', '6',
                '7', '8', '9', '10', '11', '12']
        },
        yAxis: {
            min: 0,
            title: {
                text: null
            }
        },
        legend: {
            enabled: true
        },
                plotOptions: {
            series: {
                borderColor: '#BA6161'
            }
        },

        
        series: [{
            name: 'GL BUNKER / TnM Producida',
           data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
           color: '#C99E9E',
            dataLabels: {
                enabled: true,
                
                
               
            }
        }]
    });
});
</script>