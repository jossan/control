<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<?php
?>
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        title: {
            text: 'TVC: Eficiencia Planta',
            x: -20 //center
        },
       
        xAxis: {
            labels: {
                format: 'Sem{value}'
                
            },
            categories: ['1', '2', '3', '4', '5', '6',
                '7', '8', '9', '10', '11', '12']
        },
        yAxis: {
              labels: {
                format: '{value} %',
                
            },
            title: {
                text: null
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valueSuffix: '%'
            ,shared: true,
            useHTML: true,
            headerFormat: '<small>Semana {point.key}</small><table>',
            pointFormat: '<tr><td style="color: {series.color}">{series.name}: </td>' +
                '<td style="text-align: right"><b>{point.y} </b></td></tr>',
            footerFormat: '</table>',
            crosshairs: true,
        },
        legend: {
            
            borderWidth: 0
        },
        series: [ {
            name: 'T',
            type: 'spline',
            data: [-0.2, 0.8, 5.7, 11.3, 17.0, 22.0, 24.8, 24.1, 20.1, 14.1, 8.6, 2.5],
            dashStyle: 'shortdot',
                       marker: {
                enabled: false
            }
        }, {
            name: 'V',
            type: 'spline',
            data: [-0.9, 0.6, 3.5, 8.4, 13.5, 17.0, 18.6, 17.9, 14.3, 9.0, 3.9, 1.0],
            dashStyle: 'shortdot',
                       marker: {
                enabled: false
            }
        }, 
        {
            name: 'C',
            type: 'spline',
            data:  [3.9, 4.2, 5.7, 8.5, 11.9, 15.2, 17.0, 16.6, 14.2, 10.3, 6.6, 4.8],
           dashStyle: 'shortdot',
                       marker: {
                enabled: false
            }
        },{
            name: 'TVC',
            data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6],
             
                dataLabels: {
                    enabled: true,
                     format: '{y} %',
                }
           
           
        }
    
            
            ]
    });
});
</script>