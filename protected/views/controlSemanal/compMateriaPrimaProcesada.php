<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<?php
$sql ="select Distinct(TipoMateriaPrima) from [InforFish.IngreMatPri] where TipoMateriaPrima is not NULL and TipoMateriaPrima!='' order by TipoMateriaPrima";
$especie = Yii::app()->sqldb->createCommand($sql)->queryColumn();
$rt = "";
$periodo = "select Semana, Sum(Mpprocesada) as mp from periodo where Fecha between '".$fechaini."' and '".$fechafin."' group by Year(Fecha), Semana order by Fecha,Semana";
$periodosql= Yii::app()->db->createCommand($periodo)->queryAll();
$sqlrecepcion ="select Week(Fecha,1) as Semana,EspecieID,IFNULL(sum(Pesoneto),0)as Peso from Recepcion where RecepcionID is not null and Fecha between '".$fechaini."' and '".$fechafin."'  group by  Year(Fecha),Week(Fecha,1),EspecieID order by EspecieID,Week(Fecha,1),Fecha";
$recepcion = Yii::app()->db->createCommand($sqlrecepcion)->queryAll();
$recep = array(); 

    foreach($especie as $espe=>$nameespecie):                        
    foreach($periodosql as $per):
    $recep[$nameespecie][$per['Semana']] =0;
endforeach;
endforeach;

   foreach($recepcion as $r):
   $recep[$r['EspecieID']][$r['Semana']] = (float)$r['Peso'];
endforeach;

foreach($especie as $espe=>$nameespecie):                        
    foreach($periodosql as $per):
    if($recep[$nameespecie][$per['Semana']] >0){
    $recep[$nameespecie][$per['Semana']] =  ceil((float)($recep[$nameespecie][$per['Semana']]/(float)$per['mp'])*100);    
    }
endforeach;
endforeach;


$cont=0;
foreach($recep as $espe){
$e=$especie[$cont++];
//
$valor = Array();    
    foreach ($espe as $val=>$value):
        $valor[]=$value;
    endforeach;
    
$rt = $rt."{
            name: '".$e."',
            data: ".json_encode($valor).",

        },";
}
?>
<div style="margin-left: 40px">
<?php
$meses= ['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
$selectmes = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="meses1" id="meses1" >';
for($i = 1 ;$i<=12;$i++):
    if($mes==$i)
        $selectmes.='<option value="'.$i.'" selected>'.$meses[$i].' </option>';
    else
        $selectmes .='<option value="'.$i.'">'.$meses[$i].' </option>';
endfor;
$selectmes.='</select>';
$selectanios1 = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="anios1" id="anios1" >';
for($i = 2005 ;$i<=2020;$i++):
    if($anio1==$i)
        $selectanios1.='<option value="'.$i.'" selected>'.$i.' </option>';
    else
        $selectanios1 .='<option value="'.$i.'">'.$i.' </option>';
endfor;
$selectanios1.='</select>';
$selectmes2 = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="meses2" id="meses2" >';
for($i = 1 ;$i<=12;$i++):
    if($mes2==$i)
        $selectmes2.='<option value="'.$i.'" selected>'.$meses[$i].' </option>';
    else
        $selectmes2 .='<option value="'.$i.'">'.$meses[$i].' </option>';
endfor;
$selectmes2.='</select>';
$selectanios = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="anios" id="anios" >';
for($i = 2005 ;$i<=2020;$i++):
    if($anio==$i)
        $selectanios.='<option value="'.$i.'" selected>'.$i.' </option>';
    else
        $selectanios .='<option value="'.$i.'">'.$i.' </option>';
endfor;
$selectanios.='</select>';
print 'Mes Inicial '.$selectmes;
print 'Año '.$selectanios1;
print 'Mes Final '.$selectmes2;
print 'Año '.$selectanios;

?>
    &nbsp;&nbsp;&nbsp;
    <?php 
    echo CHtml::link('Actualizar','index.php?r=/controlSemanal/compMateriaPrimaProcesada',array('id'=>'btnact','class'=>'btn btn-info'));
    echo CHtml::link('Pdf','index.php?r=/controlSemanal/guardarimg',array('id'=>'btnpdf','class'=>'btn btn-primary','target'=>'_blank'));
    ?>
    &nbsp;&nbsp;&nbsp;
    <button onclick="imapdf()" class="btn btn-info">Generar PDF</button>
    

</div>
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<script type="text/javascript">

$(function () {
    $('#container').highcharts({

        chart: {
            type: 'column',
          
                    },

        title: {
             text: 'Composición de la Materia Prima Procesada',
              x: -20 //center
        },

        xAxis: {
               labels: {
                format: 'S{value}'
                
            },
            categories: <?php echo json_encode($arraySemana);?>
        },

        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value}%'
              
            },
            title: {
                text: null

            }
        }, { // Secondary yAxis
            title: {
                text: 'Linea',
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            labels: {
                
                style: {
                    color: Highcharts.getOptions().colors[1]
                }
            },
            opposite: true
        }],

        tooltip: {
            shared: true,
            useHTML: true,
            headerFormat: '<small>Semana {point.key}</small><table>',
            pointFormat: '<tr><td style="color: {series.color}">{series.name}: </td>' +
                '<td style="text-align: right"><b>{point.y}</b></td></tr>',
            footerFormat: '</table>',
            crosshairs: true
        },

        plotOptions: {
                column: {
                stacking: 'normal',
                dataLabels: {
                    align: 'right',
                    enabled: true,
                    //inside :false,
                    //y:19,
                    format: '{y}%',
                    color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',
                    style: {
                        textShadow: '0 0 3px black'
                    }
                                        ,
                                        rotation: 270,
                    x: 2,
                    y: -10
                }
            }
        },

        series: [
    <?php echo $rt;?>
                        {
            type: 'line',
            name: 'Rendimiento',
            data: <?php echo json_encode($rendimiento);?>,
                    yAxis: 1,
                
    dataLabels: {
                   enabled: true,
                    borderRadius: 5,
                    backgroundColor: 'rgba(252, 255, 197, 0.7)',
                    borderWidth: 1,
                    borderColor: '#AAA',
                    y: 19,x:-36
                    ,inside :true
                }
    }
                         
            ]
        
    });
});
$("#btnact").click(function (){
        
        var anio =$('#anios').val();
        var anio1 =$('#anios1').val();
        var mes1 =$('#meses1').val();
        var mes2 =$('#meses2').val();
        var enlace=  'index.php?r=/controlSemanal/compMateriaPrimaProcesada';
        enlace = enlace +'&anio1='+anio1+'&anio='+anio+'&mes1='+mes1+'&mes2='+mes2;
        $(this).attr('href',enlace);
    } );
</script>