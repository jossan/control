<fieldset>
<legend>Control de producción semanal</legend>
<center>
<table>
    <tr>
        <td>
            Fecha Inicio<br>
            <?php

            $this->widget(
    'booster.widgets.TbDatePicker',
    array(
    'name' => 'inicio',
    'value'=>$f1,
    'options' => array(
    'language' => 'es',
     'format'=>'yyyy-mm-dd'
    )
    )
    );
?>
        </td>
        <td>

            Fecha Fin<br>
     <?php
        $this->widget(
    'booster.widgets.TbDatePicker',
    array(
    'name' => 'fin',
    'value'=>$f2,
    'options' => array(
    'language' => 'es',
        'format'=>'yyyy-mm-dd'
    )
    )
    );?>
        </td>
        <td>
            <br>
            <?php
            //$m = $model->search($f1,$f2);
            echo CHtml::link('Actualizar','index.php?r=/controlSemanal/produccionSemana',array('id'=>'btnact','class'=>'btn btn-primary'));

        

            ?>
            &nbsp;&nbsp;
            <?php echo CHtml::link(
                CHtml::image(Yii::app()->baseUrl."/images/excel.png","Generar reporte EXCEL",array("title"=>"Exportar a EXCEL",'height'=>'40px')),
                Yii::app()->createUrl('/controlSemanal/generarExcelprodSemanal', array('f1'=>$f1,'f2'=>$f2,))
        );?>
        </td>
    </tr>
</table>
</center>

<iframe src="index.php?r=/controlSemanal/verpdfSemanal<?php print '&f1='.$f1.'&f2='.$f2?>" width="100%" height="450px;">
  <p>Your browser does not support iframes.</p>
</iframe>
</fieldset>
<script type="text/javascript">
    $("#btnact").click(function (){
        
        var ini =$('#inicio').val();
        var fin =$('#fin').val();
        var enlace=  'index.php?r=/controlSemanal/produccionSemana';
        enlace = enlace +'&fechaini='+ini+'&fechafin='+fin;
        $(this).attr('href',enlace);
    } );
    
    $("#btnexcel").click(function (){
        
        var ini =$('#inicio').val();
        var fin =$('#fin').val();
        var enlace=  'index.php?r=/controlSemanal/produccionSemana';
        enlace = enlace +'&fechaini='+ini+'&fechafin='+fin;
        $(this).attr('href',enlace);
    } );
</script>