<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<link href="css/GridviewScroll.css" rel="stylesheet" type="text/css" />
<!-- Javascript SVG parser and renderer on Canvas, used to convert SVG tag to Canvas -->
<script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/rgbcolor.js"></script>
<script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/StackBlur.js"></script>
<script type="text/javascript" src="http://canvg.googlecode.com/svn/trunk/canvg.js"></script>
<!-- Hightchart Js -->
<script src="http://code.highcharts.com/highcharts.js"></script>

<!-- GRIDVIEW -->
    <link href="css/GridviewScroll.css" rel="stylesheet" type="text/css" />
    <script src="js/gridviewScroll.min.js" type="text/javascript"></script>

<!-- Additional files for the Highslide popup effect -->
<script type="text/javascript" src="http://www.highcharts.com/media/com_demo/highslide-full.min.js"></script>
<script type="text/javascript" src="http://www.highcharts.com/media/com_demo/highslide.config.js" charset="utf-8"></script>
<link rel="stylesheet" type="text/css" href="http://www.highcharts.com/media/com_demo/highslide.css" />


<style type="text/css">
    td{
        font-size: 9px;
    }
     td{
        font-size: 10px;
    }
    .obj{
        width: 100%;
        margin: 0px;
        padding: 2px;
        text-align: center;
        background-color:#B2F0B2;
        height: 36px;
    }
</style>
<div style="margin-left: 40px">
<?php
$meses= ['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
$selectmes = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="meses1" id="meses1" >';
for($i = 1 ;$i<=12;$i++):
    if($mes==$i)
        $selectmes.='<option value="'.$i.'" selected>'.$meses[$i].' </option>';
    else
        $selectmes .='<option value="'.$i.'">'.$meses[$i].' </option>';
endfor;
$selectmes.='</select>';
$selectanios1 = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="anios1" id="anios1" >';
for($i = 2005 ;$i<=2020;$i++):
    if($anio1==$i)
        $selectanios1.='<option value="'.$i.'" selected>'.$i.' </option>';
    else
        $selectanios1 .='<option value="'.$i.'">'.$i.' </option>';
endfor;
$selectanios1.='</select>';
$selectmes2 = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="meses2" id="meses2" >';
for($i = 1 ;$i<=12;$i++):
    if($mes2==$i)
        $selectmes2.='<option value="'.$i.'" selected>'.$meses[$i].' </option>';
    else
        $selectmes2 .='<option value="'.$i.'">'.$meses[$i].' </option>';
endfor;
$selectmes2.='</select>';
$selectanios = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="anios" id="anios" >';
for($i = 2005 ;$i<=2020;$i++):
    if($anio==$i)
        $selectanios.='<option value="'.$i.'" selected>'.$i.' </option>';
    else
        $selectanios .='<option value="'.$i.'">'.$i.' </option>';
endfor;
$selectanios.='</select>';
print 'Mes Inicial '.$selectmes;
print 'Año '.$selectanios1;
print 'Mes Final '.$selectmes2;
print 'Año '.$selectanios;

?>
    &nbsp;&nbsp;&nbsp;
    <?php 
    echo CHtml::link('Actualizar','index.php?r=/controlSemanal/grafico',array('id'=>'btnact','class'=>'btn btn-info'));
    echo CHtml::link('Pdf','index.php?r=/controlSemanal/guardarimg',array('id'=>'btnpdf','class'=>'btn btn-primary','target'=>'_blank'));
    ?>
    &nbsp;&nbsp;&nbsp;
    <button onclick="imapdf()" class="btn btn-info">Generar PDF</button>
    

</div>
<!-- Highchart container -->
<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
 
<!-- canvas tag to convert SVG -->
<canvas id="canvas" style="display:none;"></canvas>

<br>
<center>
<table id="tabla" class="items table table-bordered">
    
    <tr>

  <th>Semana&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
  <?php foreach($sql2 as $row):?>
<td><?php print $row['Semana'];?></td>
<?php endforeach; ?>
  </tr>
  <tr>
  <th>Rendimiento</th>
  <?php foreach($sql as $row):?>
<td><?php print $row['Valor'];?></td>
<?php endforeach; ?>
  </tr>
  <tr style="background-color:#B2F0B2;">
      <th>Objetivo </th>
      
  <?php $c=0;foreach($sqlobjetivo as $row):?>
  <td style="padding: 0px;"><input sem="<?php print $sql2[$c++]['Semana'];?>" anio="<?php print $anio;?>" type="text" value="<?php print $row['Objetivo'];?>" class="obj" ></td>
<?php endforeach; ?>
  </tr>
  <tr>
  <th>Sacos Producidos</th>
  <?php foreach($sql4 as $row):?>
<td><?php print $row['SumaSacos'];?></td>
<?php endforeach; ?>
  </tr>
  <tr>
  <th>Materia Prima Procesada</th>
  <?php foreach($sql3 as $row):?>
<td><?php print $row['SumaProcesada'];?></td>
<?php endforeach; ?>
  </tr>
</table>
</center>
<script type="text/javascript">
    var objetivo=<?php echo json_encode($arrayObjetivo);?>;
    var chart
    $(function () {
    chart = new Highcharts.Chart({
    chart: {
        renderTo: 'container'
    },
        title: {
            text: 'RendimientoSemanal',
            x: -20 //center
        },
         xAxis: {
             labels: {
                format: 'Sem{value}'
                
            },
            categories: <?php echo json_encode($arraySemana);?>
            
        },
        yAxis: {
            title: {
                text: 'Valores'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
       tooltip: {
                shared: true,
                crosshairs: true
            },
        legend: {
            
            borderWidth: 1
        },
                plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function (e) {
                                hs.htmlExpand(null, {
                                    pageOrigin: {
                                        x: e.pageX || e.clientX,
                                        y: e.pageY || e.clientY
                                    },
                                    headingText: this.series.name,
                                    maincontentText:  'Semana' + this.x+ ':<br/> ' +
                                        ' Rendimiento :'+this.y,
                                    width: 200
                                });
                            }
                        }
                    },
                    marker: {
                        lineWidth: 1
                    }
                }
            },
        series: [{
            name: 'Objetivo',
            data: objetivo,
        }, {
            name: 'Rendimiento',
            data: <?php echo json_encode($arrayValor);?>
        },]
    });
});
        $(document).ready(function(){
            gridviewScroll();;
            document.getElementById('btnpdf').style.display = 'none';
        });

        function gridviewScroll() {
            gridView1 = $('#tabla').gridviewScroll({
                width:1000,
                height:450,
//                 railcolor: "#F0F0F0",
//                  barcolor: "#CDCDCD",
//                 barhovercolor: "#606060",
//                 bgcolor: "#F0F0F0",
                freezesize: 1,
                arrowsize: 30,
                varrowtopimg: "images/arrowvt.png",
                varrowbottomimg: "images/arrowvb.png",
                harrowleftimg: "images/arrowhl.png",
                harrowrightimg: "images/arrowhr.png",
                headerrowcount: 1,
                railsize: 15,
                barsize: 8
            });
        }
        
            $("#btnact").click(function (){
        
        var anio =$('#anios').val();
        var anio1 =$('#anios1').val();
        var mes1 =$('#meses1').val();
        var mes2 =$('#meses2').val();
        var enlace=  'index.php?r=/controlSemanal/grafico';
        enlace = enlace +'&anio1='+anio1+'&anio='+anio+'&mes1='+mes1+'&mes2='+mes2;
        $(this).attr('href',enlace);
    } );
        
   
        function imapdf(){
        var anio =$('#anios').val();
        var anio1 =$('#anios1').val();
        var mes1 =$('#meses1').val();
        var mes2 =$('#meses2').val();
        var svg = document.getElementById('container').children[0].innerHTML;
        canvg(document.getElementById('canvas'),svg);
        var img = canvas.toDataURL("image/png"); //img is data:image/png;base64
        img = img.replace('data:image/png;base64,', '');
        //var url= 'index.php?r=/controlSemanal/guardarimg&bin_data='+img+'&anio='+anio+'&mes1='+mes1+'&mes2='+mes2;
        //window.location= url;
        //var data = "bin_data=" + img;
        $.ajax({
          url:'index.php?r=/controlSemanal/guardarimg2',
          type:'POST',
          data:{'bin_data':img},
          success: function(data){
              var link = document.getElementById('btnpdf');
              $(link).attr('href','index.php?r=/controlSemanal/guardarimg&filename='+data+'&anio1'+anio1+'&anio='+anio+'&mes1='+mes1+'&mes2='+mes2);
            document.getElementById('btnpdf').click();
          },
        });
    }
    
    $('.obj').change(function(){
        var mat = new Array();
        $('.obj').each(function( index ) {
            mat.push(Number($(this).val()));
        });
        chart.series[0].setData(mat);
    })
</script>
