<script src="js/jquery-1.10.2.js"></script>
<script src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/mindmup-editabletable.js"></script>
<script type="text/javascript" src="js/numeric-input-example.js"></script>
<script type="text/javascript" src="js/pnotify.custom.min.js"></script>
<script src="js/gridviewScroll.min.js" type="text/javascript"></script>
<link rel="stylesheet" media="all" href="css/jquery-ui.css">
<link href="css/pnotify.custom.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="css/GridviewScroll.css" rel="stylesheet" type="text/css" />
<link href="css/index.css" rel="stylesheet">

<style type="text/css">
    th,td{
        font-size: 8.5px;
    }
     .hora{
        box-shadow: 0px #ffffff;
        margin: 2px;
        padding-left: 2px;
        padding-right: 2px;
    }
    .minutos{
        box-shadow: 0px #ffffff;
        margin: 2px;
        padding-left: 2px;
        padding-right: 2px;
    }
    tbody > tr > td{
        padding: 3px;
    }
    img:hover{
        opacity: 0.5;
    }
</style>
<?php
?>
<!--<ul class="pager wizard">
<li class="next">
    <a href="#">Siguiente</a>
</li>
<li class="previous">
    <a href="#">Anterior</a>
</li>
</ul>-->
<script type="text/javascript">
        var resolucion=((screen.width)*85)/100;
        $(document).ready(function(){
            gridviewScroll();
            $('#gridedi').editableTableWidget().numericInputExample();
        });
        var visiblemp = true;
        var visibletr =true;
        var visiblesp =true;
        function gridviewScroll() {
            
             gridView1 = $('#gridedi').gridviewScroll({
                width:resolucion,
                height:425,
                freezesize: 4,
                arrowsize: 30,
                varrowtopimg: "images/arrowvt.png",
                varrowbottomimg: "images/arrowvb.png",
                harrowleftimg: "images/arrowhl.png",
                harrowrightimg: "images/arrowhr.png",
                headerrowcount:1,
                railsize: 15,
                barsize: 8
            });
        }
        $(function($){
        $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
    $( ".fechafin" ).datepicker();
    $(".hora").spinner( {min:0,max:23,step:1, });    
    $(".minutos").spinner( {min:0,max:59,step:1, });    
    $(".hora").blur(function(){
        if(this.value>23){
            this.value=23;
        }  
        else if(this.value<0){
            this.value=0;
        }
        $(this).parent().parent().parent().css("background-color",'#CDF473');
    });
    $(".minutos").blur(function(){
      if(this.value>59){
          this.value=59;
      }  
      else if(this.value<0){
          this.value=0;
      }
      $(this).parent().parent().parent().css("background-color",'#CDF473');
    });
    $(".panel-default").attr("style","width:"+resolucion+"px;");
    });
</script>
<center>
    
<?php
$box = $this->beginWidget(
'booster.widgets.TbPanel',
array(
'title' => 'Control de producción semanal',
'headerIcon' => 'th-list',
'padContent' => false,
'headerHtmlOptions' => array('style'=>'text-align: left;'),
'headerButtons'=>array(   array(
     'class' => 'booster.widgets.TbButtonGroup',
     'context' => 'primary',

     'buttons' => array(
array('label' => 'Action', 'url' => '#'))
) ),
)
);
echo $this->tabladatos($f1, $f2);

$this->endWidget();
?>
</center>

<?php
$meses= ['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
$selectmes = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="meses" id="meses" onchange="actualiza();" >';
for($i = 1 ;$i<=12;$i++):
    if($mes==$i)
        $selectmes.='<option value="'.$i.'" selected>'.$meses[$i].' </option>';
    else
        $selectmes .='<option value="'.$i.'">'.$meses[$i].' </option>';
endfor;
$selectmes.='</select>';
$selectanios = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="anios" id="anios" onchange="actualiza();" >';
for($i = 2005 ;$i<=2020;$i++):
    if($anio==$i)
        $selectanios.='<option value="'.$i.'" selected>'.$i.' </option>';
    else
        $selectanios .='<option value="'.$i.'">'.$i.' </option>';
endfor;
$selectanios.='</select>';
?>


<script type="text/javascript">
    $('table td').on('change', function(evt, newValue) {
      $(this).parent().css( "background-color",'#CDF473');
    });
    
        $('.panel-heading .pull-right').html('<?php print $selectmes.' '.$selectanios; ?>');
        function actualiza(){
            var a = $('#anios').val();
            var m = $('#meses').val();
            var htmlBef = '<img src="images/loader.gif">';
            $.ajax({
                type:"GET",
                url:"index.php?r=/controlSemanal/tabla&a="+a+"&m="+m,
                 beforeSend: function( ){
                    $('#yw0').html(htmlBef);
                 }
            }).done(function (resp){
                $('#yw0').html(resp);
                gridviewScroll();
                $('#gridedi').editableTableWidget().numericInputExample().find('td:first').focus();
                $(".hora").spinner( {min:0,max:23,step:1, });    
                $(".minutos").spinner( {min:0,max:59,step:1, });
            });
        }
        
        
function guardar(obj){
    var idper = $(obj).attr('rel');
    var total = parseInt($(obj).attr('total'));
    
    var h1= $('#inicioprodh'+idper).val() ? $('#inicioprodh'+idper).val() : '00';
    var m1 = $('#inicioprodm'+idper).val() ? $('#inicioprodm'+idper).val() : '00';
    
    var fechafin = $('#fechafin'+idper).val() ? $('#fechafin'+idper).val() : 'nulo';
    
    var inicioprod = h1+':'+m1;
    var h2= $('#finprodh'+idper).val() ? $('#finprodh'+idper).val() : '00';
    var m2 = $('#finprodm'+idper).val() ? $('#finprodm'+idper).val() : '00';
    var finprod = h2+':'+m2; 
    var h3= $('#noprogh'+idper).val() ? $('#noprogh'+idper).val() : '00';
    var m3 = $('#noprogm'+idper).val() ? $('#noprogm'+idper).val() : '00';
    var noprog = h3+':'+m3;    
     var h4= $('#progh'+idper).val() ? $('#progh'+idper).val() : '00';
    var m4 = $('#progm'+idper).val() ? $('#progm'+idper).val() : '00';
    var prog = h4+':'+m4;    
     var h5= $('#tercerh'+idper).val() ? $('#tercerh'+idper).val() : '00';
    var m5 = $('#tercerm'+idper).val() ? $('#tercerm'+idper).val() : '00';
    var tercer = h5+':'+m5;    
    var sachumed = $('#sachumed'+idper).html() ? $('#sachumed'+idper).html() : 'nulo';
    var valores = Array();
    
    for(var i = 0; i<total;i++){
        valores.push($('#valorc'+i+idper).html() ? $('#valorc'+i+idper).html():'nulo');
    }
    
    $.ajax({
            url:'index.php?r=/controlSemanal/actualizartabla',
            type:'GET',
            dataType: "json",
            data:{'idper':idper,'total':total,'inicioprod':inicioprod,'fechafin':fechafin,'finprod':finprod,'noprog':noprog,'prog':prog,'tercer':tercer,'sachumed':sachumed,'valores':valores},
            success:function (resp){
                verificar(resp.status);
                var sem= $('#Semana'+idper).html();
                $('#MP'+idper).html(resp.Rp);
                $('#TR'+idper).html(resp.Tr);
                $('#SP'+idper).html(resp.Sp);
                $('#CA'+idper).html(resp.Ca);
                $('#RD'+idper).html(resp.Rd);
                $('#RS'+sem).html(resp.Rs);
                $(obj).parent().parent().css( "background-color",'');
                $('#MP'+idper).parent().css( "background-color",'');
                $('#fechafin'+idper).css( "background-color",'');
            },
        });
}

/* Funcion para verificar si actualiza corrrectamente (notificacion)*/
function verificar(resp){
    if(resp=='1')
        new PNotify({
        title: 'Actualizado',
        type: 'success'
        });
    else if(resp=='0')
        new PNotify({
        title: 'Sin cambios en actualización',
        });
    else if(resp=='2'){
        new PNotify({
        title: 'Sin permiso al guardar',
        type: 'error'
        });
    }
}
function visible(opc,validar){
    if (validar){
        $(opc).hide();
    }else{
        $(opc).show();
    }
    gridviewScroll();
    switch(opc){
        case '.mp':
            visiblemp = !visiblemp;
            break;
        case '.tr':
            visibletr = !visibletr;
            break;
        case '.sp':
            visiblesp = !visiblesp;
            break;
    }
}
visible('.mp',visiblemp);
visible('.tr',visibletr);
visible('.sp',visiblesp);
$( ".fechafin" ).change(function(){
    $(this).parent().parent().css("background-color",'#CDF473');
    $(this).css("background-color",'#CDF473');
});
</script>