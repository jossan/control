<?php
/*CARGAR DATOS*/
            $periodo = Periodo::model()->findAll("Fecha BETWEEN '".$f1."' AND '".$f2."';");
            $sql ="select Distinct(TipoMateriaPrima) from [InforFish.IngreMatPri] where TipoMateriaPrima is not NULL and TipoMateriaPrima!='' order by TipoMateriaPrima";
            $especie = Yii::app()->sqldb->createCommand($sql)->queryAll();
            $clientes= Clientes::model()->findAll('Estado=1 order by Exportacion Desc');
            $cliventa=new CDbCriteria();
            $cliventa->select="*";
            $cliventa->alias="t1";
            $cliventa->with = array ('periodo');
            $cliventa->addBetweenCondition('periodo.Fecha', $f1, $f2);
            $ptventa = PTVenta::model()->findAll($cliventa);
            $totalClientes = count($clientes);
            $contadorPTVenta=0;
            $contador=count($periodo);
            $tsemana = -1;
            $sql ="select ID, Fecha,EspecieID,IFNULL(sum(PesoTonelada),0)as Peso from Recepcion where RecepcionID is not null and Fecha between '".$f1."' and '".$f2."' group by Fecha,EspecieID order by ID";
            $recepcion = Yii::app()->db->createCommand($sql)->queryAll();
            $recep = array();
            foreach($recepcion as $r):
                $recep[$r['Fecha']][$r['EspecieID']] = $r['Peso'];
            endforeach;
 /*Fin de cargar datos*/
?>

<html>
<head>
<style>
 body {font-family: sans-serif;
 font-size: 7pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 .items td {
 border-left: 0.1mm solid #000000;
 border-right: 0.1mm solid #000000;
 }
 table tdead td { background-color: #EEEEEE;
 text-align: center;
 border: 0.1mm solid #000000;
 }
 .items tr {
 background-color: #FFFFFF;
 border: 0mm none #000000;
 border-top: 0.08mm solid gray;
 }
 .items td.totals {
 text-align: right;
 border: 0.1mm solid #000000;
 }
</style>
</head>
<body>
 <table width="100%"><tr>
         <td width="50%" style="color:#0000BB;"><span style="font-weight: bold; font-size: 14pt;">&nbsp;&nbsp;&nbsp;<b style="color:blue;">TADEL S.A</b></span><br />Manabí-Ecuador</td>
<td width="50%" style="text-align: center;"><span style="font-weight: bold; font-size: 16pt;">Control de producción semanal</span></td>
<td width="50%" style="text-align: right;"><b>Desde: </b><?php echo $f1; ?>, <b>Hasta: </b><?php echo $f2; ?> <br> <b>Total: </b> <?php echo $contador; ?></td>
</tr>
</table>
<br>
 <table class="items" widtd="100%" style="font-size: 7pt; border-collapse: collapse;" cellpadding="5">
     <thead>
     <tr>
<th style="background:Turquoise;">FECHA</th><th style="background:Turquoise;">DÍA</th><th style="background:Turquoise;">SEMANA</th>
<?php foreach($especie as $row):?>
<th style="background:Turquoise;"><?php print $row->Nombre;?></th>
<?php endforeach; ?>
<th style="background:Turquoise;">MP_PROCESADA</th>
<th style="background:NavajoWhite;">INICIO_PROD<br style="background:NavajoWhite;">Cocinador</th>
<th style="background:NavajoWhite;">FIN_PROD<BR>Cocinador</th>
<th style="background:NavajoWhite;">PARADA NO PROGRAMADA</th>
<th style="background:NavajoWhite;">PARADA PROGRAMADA</th>
<th style="background:NavajoWhite;">T.R. TRABAJO</th>
<?php foreach($clientes as $row):
    if($row->Exportacion): ?>
    <th style="background:CornflowerBlue;"><?php print $row->Nombre; ?>'</th>
    <?php else:?>
    <td style="background:DarkGray;"><?php print $row->Nombre ?>'</td>
<?php 
endif;
endforeach;?>
<td style="background:Peru;">Sacos Producidos</td>
<td style="background:LightSkyBlue;">Sacos Humedos Reprocesados</td>
<td style="background:Peru;">Sacos de Baja Calidad</td>
<td style="background:Peru;">Rendimiento diario</td>
<td style="background:Peru;">Rendimiento Semanal</td>
</tr>
</thead>
<tbody>
<?php  foreach ($periodo as $row):?>
    <tr>
    <td><?php print $row->Fecha; ?></td>
    <td><?php print $row->Dia; ?></td>
    <td><?php print $row->Semana; ?></td>
    <?php  $c=0;
        ?>
    <!-- Nueva Especie -->
         <?php 
         foreach($especie as $espe):
                    if(isset($recep[$row->Fecha][$espe['TipoMateriaPrima']])):
                        $val = $recep[$row->Fecha][$espe['TipoMateriaPrima']];
                    else:
                        $val="0.0";
                    endif;
                    ?>
         <td><?php print $val; ?></td>';
                    <?php
                endforeach;
                ?>
           <!--Fin Nueva Especie -->
    
    <td><?php print $row->MPProcesada; ?></td>
    <td><?php print $row->InicioProd; ?></td>
    <td><?php print $row->FinProd; ?></td>
    <td><?php print $row->ParadanoProgramada; ?></td>
    <td><?php print $row->ParadaProgramada ?></td>
    <td><?php print $row->TRTrabajo; ?></td>
    <?php for($i = 0;$i<($totalClientes);$i++):?>
         <td><?php print $ptventa[$contadorPTVenta]->Valor; ?></td>
    <?php $contadorPTVenta++; 
               endfor; ?>
    <td><?php print $row->SacosProducidos;?></td>
    <td><?php print $row->SacosHumedos; ?></td>
    <td><?php print $row->Calidad; ?></td>
    <td><?php print $row->RendimientoDiario; ?></td>
    <?php 
    $año = strftime("%Y", strtotime($row->Fecha));
    $semana = $row->Semana;
    
    $modelPeriodo = Periodo::model()->findAll("YEAR(Fecha)=".$año." and Semana=".$semana." and Fecha BETWEEN '".$f1."' AND '".$f2."';");
     $num = count($modelPeriodo);
     $modeloRendimiento = RendimientoSemanal::model()->find("Anio=".$año." and Semana=".$semana);
     
     ?>
    <?php if($semana != $tsemana):?>
    <td style="vertical-align:middle;" rowspan="<?php print $num?>"><?php print $modeloRendimiento->Valor; ?></td>
    <?php
    $tsemana = $semana;
    endif ?>
    </tr>
<?php endforeach;?>
 </tbody>
 </table>
  
 </body>
 </html>
