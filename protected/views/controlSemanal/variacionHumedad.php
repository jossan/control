<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<?php
?>
 <div style="margin-left: 40px">
<?php
$meses= ['','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
$selectmes = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="meses1" id="meses1" >';
for($i = 1 ;$i<=12;$i++):
    if($mes==$i)
        $selectmes.='<option value="'.$i.'" selected>'.$meses[$i].' </option>';
    else
        $selectmes .='<option value="'.$i.'">'.$meses[$i].' </option>';
endfor;
$selectmes.='</select>';
$selectanios1 = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="anios1" id="anios1" >';
for($i = 2005 ;$i<=2020;$i++):
    if($anio1==$i)
        $selectanios1.='<option value="'.$i.'" selected>'.$i.' </option>';
    else
        $selectanios1 .='<option value="'.$i.'">'.$i.' </option>';
endfor;
$selectanios1.='</select>';
$selectmes2 = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="meses2" id="meses2" >';
for($i = 1 ;$i<=12;$i++):
    if($mes2==$i)
        $selectmes2.='<option value="'.$i.'" selected>'.$meses[$i].' </option>';
    else
        $selectmes2 .='<option value="'.$i.'">'.$meses[$i].' </option>';
endfor;
$selectmes2.='</select>';
$selectanios = '<select style="background:white ;height:18px; font-size:9px;padding:1px;margin:1px;" name="anios" id="anios" >';
for($i = 2005 ;$i<=2020;$i++):
    if($anio==$i)
        $selectanios.='<option value="'.$i.'" selected>'.$i.' </option>';
    else
        $selectanios .='<option value="'.$i.'">'.$i.' </option>';
endfor;
$selectanios.='</select>';
print 'Mes Inicial '.$selectmes;
print 'Año '.$selectanios1;
print 'Mes Final '.$selectmes2;
print 'Año '.$selectanios;

?>
    &nbsp;&nbsp;&nbsp;
    <?php 
    echo CHtml::link('Actualizar','index.php?r=/controlSemanal/variacionHumedad',array('id'=>'btnact','class'=>'btn btn-info'));
    echo CHtml::link('Pdf','index.php?r=/controlSemanal/guardarimg',array('id'=>'btnpdf','class'=>'btn btn-primary','target'=>'_blank'));
    ?>
    &nbsp;&nbsp;&nbsp;
    <button onclick="imapdf()" class="btn btn-info">Generar PDF</button>
    

</div>

<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Variación de la Humedad (P.Terminado)'
        },
        
       xAxis: {
                         labels: {
                format: 'Sem{value}'
                
            },
            categories: <?php echo json_encode($arraySemana);?>
            
        },
        yAxis: {
            title: {
                text: '% HUMEDAD'
            }
        },
        tooltip: {
                shared: true,
                crosshairs: true
            },
        plotOptions: {
            line: {
                
                
            }
        },
        series: [{
            name: 'Humedad',
            data: <?php echo json_encode($arrayPorcentaje);?>,
            dataLabels: {
                    enabled: true
                },
                marker: {
                symbol: 'square'
            },
            color:'#07164C',
        },{
            name: 'Objetivo',
            data: [20, 20, 20, 20],
            marker: {
                symbol: 'diamond'
            },
            color: '#38862A'
            
        }]
    });
});

$("#btnact").click(function (){
        
        var anio =$('#anios').val();
        var anio1 =$('#anios1').val();
        var mes1 =$('#meses1').val();
        var mes2 =$('#meses2').val();
        var enlace=  'index.php?r=/controlSemanal/variacionHumedad';
        enlace = enlace +'&anio1='+anio1+'&anio='+anio+'&mes1='+mes1+'&mes2='+mes2;
        $(this).attr('href',enlace);
    } );
</script>