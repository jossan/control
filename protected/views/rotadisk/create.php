<?php
$this->breadcrumbs=array(
	'Rotadisks'=>array('admin'),
	'Nuevo',
);

$this->menu=array(
array('label'=>'Lista de Rotadisk','url'=>array('admin')),

);
?>

<h3>Nuevo Rotadisk</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
