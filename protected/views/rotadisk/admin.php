<?php
$this->breadcrumbs=array(
	
	'Administración',
);

$this->menu=array(

array('label'=>'Crear Rotadisk','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('rotadisk-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Rotadisk</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'rotadisk-grid',
    'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
//'filter'=>$model,
'columns'=>array(
		//'ID',
		'Nombre',
		'Descripcion',
    array(
        'name'=>'EstadoRotadisk',
        'value'=>'$data->EstadoRotadisk?"Activo":"Inactivo"'
    ),
		
		//'Estado',
array(
'class'=>'booster.widgets.TbButtonColumn',
    
),
),
)); ?>
