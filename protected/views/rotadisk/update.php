<?php
$this->breadcrumbs=array(
	'Rotadisks'=>array('admin'),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Rotadisks','url'=>array('admin')),
	array('label'=>'Crear Rotadisk','url'=>array('create')),
	array('label'=>'Ver Rotadisk','url'=>array('view','id'=>$model->ID)),
	);
	?>

	<h3>Actualizar Rotadisk</h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
