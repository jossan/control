<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>-->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>-->

<!--<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/jquery-1.10.2.js"></script>
<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
-->
<script src="js/jquery-1.10.2.js"></script>
<script src="js/jquery-ui.js"></script>
<script type="text/javascript" src="js/mindmup-editabletable.js"></script>
<script type="text/javascript" src="js/numeric-input-example.js"></script>
<script type="text/javascript" src="js/pnotify.custom.min.js"></script>
<script type="text/javascript" src="js/controlproduccion.js"></script>
<link rel="stylesheet" href="css/jquery-ui.css">
<link href="css/pnotify.custom.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="css/index.css" rel="stylesheet">
<style type="text/css">
    th,td{
        font-size: 9px;
    }
    .hora{
        box-shadow: 0px #ffffff;
        margin: 2px;
        padding-left: 2px;
        padding-right: 2px;
    }
    .minutos{
        box-shadow: 0px #ffffff;
        margin: 2px;
        padding-left: 2px;
        padding-right: 2px;
    }
    img:hover{
        opacity: 0.5;
    }
</style>

<?php if(isset($_GET['fecha']))$fecha=$_GET['fecha'];else $fecha=date('Y-m-d');?>
<script type="text/javascript">
$(function($){
    PNotify.prototype.options.styling = "bootstrap3";
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
        weekHeader: 'Sm',
        dateFormat: 'yy-mm-dd',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['es']);
    $( "#datepicker" ).datepicker();
    $( "#datepicker" ).val('<?php print $fecha;?>');
    $(".hora").spinner( {min:0,max:23,step:1, });    
    $(".minutos").spinner( {min:0,max:59,step:1, });    
});
</script>

<?php
/* @var $this ControlController */

//$this->breadcrumbs=array(
//	'Control',
//);

?>
<div style="text-align: right;display: block;margin-top: 12px;">
    <p>Fecha: <input type="text" id="datepicker" style="height: 22px;width: 100px;text-align: center;display: inline;">
    <?php print CHtml::link('Ir', 'index.php?r=/control/index&fecha='.$fecha, array('class' => 'btn btn-success','style'=>'padding:1px 5px;','id'=>'ir'));?></p>
</div>
<?php
    $user =  Usuarios::model()->find('ID='.Yii::app()->user->id); 
    $this->widget(
    'booster.widgets.TbTabs',
    array(
    'type' => 'tabs',
    'justified' => true,
    'tabs' => array(
	array('label' => 'Recepción MP','content' =>$this->recepcion($fecha),'linkOptions' => array('id'=>'btnrecepcion'),'active' => true),
	array('label' => 'Cocción','content'=>'','visible'=>$user->TipoRolID!=3, 'linkOptions' => array('id'=>'btncocinas')),
	array('label' => 'Prensado', 'content' => '','visible'=>$user->TipoRolID!=3,'linkOptions' => array('id'=>'btnprensas')),
	array('label' => 'Decantación', 'content' => '','visible'=>$user->TipoRolID!=3,'linkOptions' => array('id'=>'btntrincantes')),
	array('label' => 'Refinado', 'content' => '','visible'=>$user->TipoRolID!=3,'linkOptions' => array('id'=>'btnrefinacion')),
	array('label' => 'Concentración', 'content' => '','visible'=>$user->TipoRolID!=3,'linkOptions' => array('id'=>'btntratamiento')),
	array('label' => 'Presecado', 'content' => '','visible'=>$user->TipoRolID!=3,'linkOptions' => array('id'=>'btnrotadisk')),
        array('label' => 'Secado', 'content' => '','visible'=>$user->TipoRolID!=3,'linkOptions' => array('id'=>'btnrotatubo')),
        array('label' => 'P. Terminado', 'content' => '','visible'=>$user->TipoRolID!=3,'linkOptions' => array('id'=>'btnfin'))
       ),
    )
    );
    ?>
<script type="text/javascript">
var Fecha='<?php print $fecha;?>';
var htmlBef = '<img src="images/loader.gif">';
var pestania = $('#yw1').find('li.active a')[0].id;
//$('#tabla_inicio').editableTableWidget();
$('#tabla_inicio').editableTableWidget().numericInputExample().find('td:first').focus();
$('#datepicker').change(function(){
    $('#ir').attr('href','index.php?r=/control/index&fecha='+this.value);
});
$('#btnrecepcion').click(function(){
    if(verificarcambios()){return false;}
    $.ajax({
            url:'index.php?r=/control/recepciones',
            type:'GET',
            data: {'fecha':Fecha},
            beforeSend: function( ){
                    $('#yw0_tab_1').html(htmlBef);
                 },
            success: function (resp) {
                $('#yw0_tab_1').html(resp);
                $('#tabla_inicio').editableTableWidget().numericInputExample().find('td:first').focus();
                $(".hora").spinner( {min:0,max:23,step:1, });    
                $(".minutos").spinner( {min:0,max:59,step:1, });
                controlpestania();
            },
            error:function(e){
                alert('Se produjo un error al intentar obtener los datos');
            }
        });
});
$('#btncocinas').click(function(){
    if(verificarcambios()){return false;}
        $.ajax({
            url:'index.php?r=/control/cocinas',
            type:'GET',
            data: {'fecha':Fecha},
            beforeSend: function( ){
                    $('#yw0_tab_2').html(htmlBef);
                 },
            success: function (resp) {
                $('#yw0_tab_2').html(resp);
                $('#tabla_cocina').editableTableWidget().numericInputExample().find('td:first').focus();
                validaciontabla();
                controlpestania();
            },
            error:function(e){
                alert('Se produjo un error al intentar obtener los datos');
            }
        });
});
$('#btnprensas').click(function(){
    if(verificarcambios()){return false;}
    $.ajax({
        url:'index.php?r=/control/prensado',
        type:'GET',
        data: {'fecha':Fecha},
        beforeSend: function( ){
                    $('#yw0_tab_3').html(htmlBef);
                 },
        success: function (resp) {
            $('#yw0_tab_3').html(resp);
            $('#tabla_prensas').editableTableWidget().numericInputExample().find('td:first').focus();
            validaciontabla();
            controlpestania();
        },
        error:function(e){
                alert('Se produjo un error al intentar obtener los datos');
            }
    });
});
$('#btntrincantes').click(function(){
    if(verificarcambios()){return false;}
    $.ajax({
        url:'index.php?r=/control/decantacion',
        type:'GET',
        data: {'fecha':Fecha},
        beforeSend: function( ){
                    $('#yw0_tab_4').html(htmlBef);
                 },
        success: function (resp) {
            $('#yw0_tab_4').html(resp);
            $('#tabla_decantasion').editableTableWidget().numericInputExample().find('td:first').focus();
            validaciontabla();
            controlpestania();
        },
        error:function(e){
                alert('Se produjo un error al intentar obtener los datos');
            }
    });
});

$('#btnrefinacion').click(function(){
    if(verificarcambios()){return false;}
    $.ajax({
        url:'index.php?r=/control/refinacion',
        type:'GET',
        data: {'fecha':Fecha},
        beforeSend: function( ){
                    $('#yw0_tab_5').html(htmlBef);
                 },
        success: function (resp) {
            $('#yw0_tab_5').html(resp);
            $('#tabla_refinacion').editableTableWidget().numericInputExample().find('td:first').focus();
            validaciontabla();
            controlpestania();
        },
        error:function(e){
                alert('Se produjo un error al intentar obtener los datos');
            }
    });
});
$('#btntratamiento').click(function(){
    if(verificarcambios()){return false;}
       $.ajax({
        url:'index.php?r=/control/tratamiento',
        type:'GET',
        data: {'fecha':Fecha},
        beforeSend: function( ){
                    $('#yw0_tab_6').html(htmlBef);
                 },
        success: function (resp) {
            $('#yw0_tab_6').html(resp);
            $('#tabla_tratamiento').editableTableWidget().numericInputExample().find('td:first').focus();
            validaciontabla();
            controlpestania();
        },
        error:function(e){
                alert('Se produjo un error al intentar obtener los datos');
            }
    });
});
$('#btnrotadisk').click(function(){
    if(verificarcambios()){return false;}
    $.ajax({
        url:'index.php?r=/control/presecado',
        type:'GET',
        data: {'fecha':Fecha},
        beforeSend: function( ){
                    $('#yw0_tab_7').html(htmlBef);
                 },
        success: function (resp) {
            $('#yw0_tab_7').html(resp);
            $('#tabla_presecado').editableTableWidget().numericInputExample().find('td:first').focus();
            validaciontabla();
            controlpestania();
        },
        error:function(e){
                alert('Se produjo un error al intentar obtener los datos');
            }
    });
});
$('#btnrotatubo').click(function(){
    if(verificarcambios()){return false;}
        $.ajax({
        url:'index.php?r=/control/secado',
        type:'GET',
        data: {'fecha':Fecha},
        beforeSend: function( ){
                    $('#yw0_tab_8').html(htmlBef);
                 },
        success: function (resp) {
            $('#yw0_tab_8').html(resp);
            $('#tabla_secado').editableTableWidget().numericInputExample().find('td:first').focus();
            validaciontabla();
            controlpestania();
        },
        error:function(e){
                alert('Se produjo un error al intentar obtener los datos');
            }
    });
});


$('#btnfin').click(function(){
    if(verificarcambios()){return false;}
        $.ajax({
        url:'index.php?r=/control/empaque',
        type:'GET',
        data: {'fecha':Fecha},
        beforeSend: function( ){
                    $('#yw0_tab_9').html(htmlBef);
                 },
        success: function (resp) {
            $('#yw0_tab_9').html(resp);
            $('#tabla_fin').editableTableWidget();
            $(".hora").spinner( {min:0,max:23,step:1, });    
            $(".minutos").spinner( {min:0,max:59,step:1, });   
            validaciontabla();
            controlpestania();
        },
        error:function(e){
                alert('Se produjo un error al intentar obtener los datos');
            }
    });
});

/* Funcion para verificar si actualiza corrrectamente (notificacion)*/
function verificar(resp){
    if(resp=='1')
        new PNotify({
        title: 'Actualizado',
        type: 'success'
        });
    else if(resp=='0')
        new PNotify({
        title: 'Sin cambios en actualización',
        });
    else if(resp=='2'){
        new PNotify({
        title: 'Sin permiso al guardar',
        type: 'error'
        });
    }
}
function controlpestania(){
    pestania = $('#yw1').find('li.active a')[0].id;
}
validaciontabla();
</script>