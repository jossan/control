<?php /*echo $form->dateRangeGroup($model,'Fecha',array('widgetOptions' => array(
'callback' => 'js:function(start, end){console.log(start.toString("MMMM d, yyyy") + " - " + end.toString("MMMM d, yyyy"));}'),
'wrapperHtmlOptions' => array('class' => 'col-sm-5','placeholder'=>'Rango de fecha'),'prepend' => '<i class="glyphicon glyphicon-calendar"></i>'
)); */

?>
<fieldset>
<legend>Control de proceso de producción</legend>
<center>
<table>
    <tr>
        <td>
            Fecha
            
            <br>
         <?php   $this->widget(
    'booster.widgets.TbDatePicker',
    array(
    'name' => 'inicio',
    'value'=>$f1,
        'options'=>array(
            'format'=> 'yyyy-mm-dd',
    'todayBtn'=> 'linked',
        )
    )
    );
 ?>
        </td>
          <td>
            Fecha final
            
            <br>
         <?php   $this->widget(
    'booster.widgets.TbDatePicker',
    array(
    'name' => 'fin',
    'value'=>$f2,
        'options'=>array(
            'format'=> 'yyyy-mm-dd',
    'todayBtn'=> 'linked',
        )
    )
    );
 ?>
        </td>
<!--        <td>

     Hora Inicial <br>
     <?php  /* $this->widget(
    'booster.widgets.TbTimePicker',
    array(
    'name' => 'horainic',
        'id' => 'horainic',
    'value'=>$horaini,
        'noAppend' => true,
        'options'=>array(
          
    'showMeridian'=> false,
        ),
        
    )
    );*/
 ?>
            
            	
        </td>
        <td>
            Hora Final <br>
            <?php   /*$this->widget(
    'booster.widgets.TbTimePicker',
    array(
    'name' => 'horafinc',
        'id'=>'horafinc',
        'noAppend' => true,
    'value'=>$horafin,
        'options'=>array(
          
    'showMeridian'=> false,
        )
    )
    );*/
 ?>
        </td>-->
        <td>
            <br>
            <?php
        echo CHtml::link('Actualizar','index.php?r=/control/produccionRango',array('id'=>'btnact','class'=>'btn btn-primary'));
        
        ?>
            &nbsp;&nbsp;
            <?php echo CHtml::link(
                CHtml::image(Yii::app()->baseUrl."/images/excel.png","Generar reporte EXCEL",array("title"=>"Exportar a EXCEL",'height'=>'40px')),
                'index.php?r=/control/generarExcelprod',array('id'=>'btnexcel')
        );?>
            
        </td>
    </tr>
</table>
</center>
</fieldset>

<?php
//$m = $model->search($f1,$f2);
?>
<iframe src="index.php?r=/control/VerpdfRango&fechaini=<?php print $f1; ?>&fechafin=<?php print $f2; ?>&horaini=<?php print $horaini; ?>&horafin=<?php print $horafin; ?>" width="100%" height="450px;" >
<p>Tu Navegador no soporta iframe</p>
</iframe>

<script type="text/javascript">
    function enlistar(){
        alert($('#rangofecha').val());
    }
    $("#btnact").click(function (){
        var ini =$('#inicio').val();
        var fin =$('#fin').val();
        var horaini = $('#horainic').val();
        var horafin = $('#horafinc').val();
        var enlace=  'index.php?r=/control/produccionRango';
        enlace = enlace +'&fechaini='+ini+'&fechafin='+fin+'&horaini='+horaini+'&horafin='+horafin;
        $(this).attr('href',enlace);
    } );
    
    $("#btnexcel").click(function (){
        
        var ini =$('#inicio').val();
        var fin =$('#fin').val();
        var horaini = $('#horainic').val();
        var horafin = $('#horafinc').val();
        var enlace=  'index.php?r=/control/generarExcelprod';
        enlace = enlace +'&fechaini='+ini+'&fechafin='+fin+'&horaini='+horaini+'&horafin='+horafin;
        $(this).attr('href',enlace);
    } );
</script>
