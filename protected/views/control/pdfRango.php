<?php
$PAGINADO =12;
$LIMIT = $PAGINADO;
$OFFSET = $cont * $PAGINADO;
$LIMITCOCCION = $PAGINADO*$numerococina;
$OFFSETCOCCION = ($cont * $PAGINADO)*$numerococina;
$LIMITPRENSADO = $PAGINADO*$numeroprensas;
$OFFSETPRENSADO = ($cont * $PAGINADO)*$numeroprensas;
$LIMITDECANTACION = $PAGINADO*$numerotricanter;
$OFFSETDECANTACION = ($cont * $PAGINADO)*$numerotricanter;
$LIMITREFINADO = $PAGINADO*$numeropulidora;
$OFFSETREFINADO = ($cont * $PAGINADO)*$numeropulidora;
$LIMITTRATAMIENTO = $PAGINADO*$numeroplantas;
$OFFSETTRATAMIENTO = ($cont * $PAGINADO)*$numeroplantas;
$LIMITPRESECADO = $PAGINADO*$numerorotadisk;
$OFFSETPRESECADO = ($cont * $PAGINADO)*$numerorotadisk;
$LIMITSECADO = $PAGINADO*$numerorotatubo;
$OFFSETSECADO = ($cont * $PAGINADO)*$numerorotatubo;

$model = Recepcion::model()->findAll("concat(Fecha,' ',Hora) between '".$fechaini."' and '".$fechafin."' order by Fecha, Hora, ID LIMIT ".$LIMIT." OFFSET ".$OFFSET);
$cri = new CDbCriteria();
$cri->select = "*";
$cri->addBetweenCondition("concat(Fecha,' ',Hora)", $fechaini, $fechafin);
$cri->order = 'Fecha, Hora, codigofila, ID';
$cri->limit = $LIMIT;
$cri->offset = $OFFSET;
$crili= new CDbCriteria();
$crili->select = "distinct(t.codigofila), t.*";
$crili->join = "join Prensado on t.codigofila = Prensado.codigofila";
$crili->addBetweenCondition("concat(Fecha,' ',Hora)", $fechaini, $fechafin);
$crili->order = 'Fecha, Hora, ID';
$crili->limit = $LIMITCOCCION;
$crili->offset = $OFFSETCOCCION;
$cri->limit = $LIMIT;
$cri->offset = $OFFSET;
$licors = Licor::model()->findAll($crili);
$cri->limit = $LIMITCOCCION;
$cri->offset = $OFFSETCOCCION;
$coccions = Coccion::model()->findAll($cri);
$cri->limit = $LIMITPRENSADO;
$cri->offset = $OFFSETPRENSADO;
$prensados = Prensado::model()->findAll($cri);
$cri->limit = $LIMITDECANTACION;
$cri->offset = $OFFSETDECANTACION;
$decantasions = Decantacion::model()->findAll($cri);
$cri->limit = $LIMITREFINADO;
$cri->offset = $OFFSETREFINADO;
$refinados = Refinacion::model()->findAll($cri);
$cri->limit = $LIMITTRATAMIENTO;
$cri->offset = $OFFSETTRATAMIENTO;
$tratamientos= Tratamiento::model()->findAll($cri);
$cri->limit = $LIMITPRESECADO;
$cri->offset = $OFFSETPRESECADO;
$presecados = Presecado::model()->findAll($cri);
$cri->limit = $LIMITSECADO;
$cri->offset = $OFFSETSECADO;
$secados = Secado::model()->findAll($cri);
$cri = new CDbCriteria();
$cri->select = "*";
$cri->addBetweenCondition("concat(Fecha,' ',Hora)", $fechaini, $fechafin);
$cri->order = 'Fecha, Hora, ID';
$cri->limit = $LIMIT;
$cri->offset = $OFFSET;
$harina = Harina::model()->findAll($cri);
$sql = "SELECT LicorMinPorcentajeSolidos, LicorMaxPorcentajeSolidos, LicorPorcentajeSolidosAlertaNaranja, LicorMinPorcentajeGrasas, LicorMaxPorcentajeGrasas, LicorPorcentajeGrasasAlertaNaranja from Parametros LIMIT 1";
$lic = Yii::app()->db->createCommand($sql)->queryRow();
$dias = array('Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado');
?>

<html>
<head>
<style>
 body {font-family: sans-serif;
 font-size: 7pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 .items td {
 border-left: 0.1mm solid #000000;
 border-right: 0.1mm solid #000000;
 }
 table thead td { background-color: #EEEEEE;
 text-align: center;
 border: 0.1mm solid #000000;
 }
 .items tr {
 background-color: #FFFFFF;
 border: 0mm none #000000;
 border-bottom: 0.08mm solid gray;
 }
 .items td.totals {
 text-align: right;
 border: 0.1mm solid #000000;
 }
 .pnormal{
    background-color: #FFFFFF !important ;
}
.pnaranja{
    background-color: #FFC683 !important ;
}
.projo{
    background-color: #FC8D8D !important ;
}
</style>
</head>
<body>

<!--mpdf
<htmlpageheader name="myheader">
 <table width="100%"><tr>
 <td width="50%" style="color:#0000BB;"><span style="font-weight: bold; font-size: 14pt;">&nbsp;&nbsp;&nbsp;<img src="images/logotadel.png"></span><br />Manabí-Ecuador</td>
<td width="50%" style="text-align: center;font-size: 14pt;"><b>Control de proceso de producción</b></td>
<td width="50%" style="text-align: right;"><b>Fecha: </b><?php echo $dias[date('N', strtotime($fechaini))].', '.$fechaini; ?><br></td>
 

 </tr>

</table>
</htmlpageheader>

<htmlpagefooter name="myfooter">
<div style="border-top: 1px solid #000000; margin-top:0px;font-size: 9pt; text-align: center; ">
Página {PAGENO} de {nb}
</div>
</htmlpagefooter>

<sethtmlpageheader name="myheader" value="on" show-this-page="1" />
 <sethtmlpagefooter name="myfooter" value="on" />
 mpdf-->
<!--<div style="text-align: right"><b>Fecha de informe: </b><?php //echo date("d/m/Y"); ?> </div>-->
 <table class="items" width="100%" style="font-size: 7pt; border-collapse: collapse;" cellpadding="5">
 <thead>
 <tr>
 <!--Pestaña recepcion-->
 <td colspan="8">Recepción</td>
 <td colspan="<?php print ($numerococina*4)+1?>"><center>Cocción</center></td>
 <td colspan="<?php print($numeroprensas*2)+3;?>">Prensado</td>
 <td colspan="<?php print ($numerotricanter*4)+1;?>">Decantación</td> 
 <td colspan="<?php print ($numeropulidora*4)+1;?>">Refinación</td>
 <td colspan="<?php print ($numeroplantas*8)+1;?>">Concentración</td>
 </tr>
 <!--Pestaña Coccion-->
 <tr>
 <td rowspan="4">H<br>O<br>R<br>A</td>
 <td rowspan="4">S<br>E<br>C<br>U</td>
 <td rowspan="4">T<br>O<br>L<br>V<br>A</td>
 <td rowspan="4">Especie</td>
 <td rowspan="4">T<br>B<br>V<br>N</td>
 <td rowspan="4">Peso(T)</td>
 <td rowspan="4">Desc(T)</td>
 <td rowspan="4">Peso Neto(T)</td>
 <td rowspan="4">Hora</td>
 <?php for ($i=0;$i<$numerococina;$i++):?>
 <td colspan="4"><center><?php print $i+1; ?></center></td>
 <?php endfor;?>
 <td rowspan="4">Hora</td>
 <?php for ($i=0;$i<$numeroprensas;$i++):?>
 <td colspan="2"><center><?php print $i+1; ?></center></td>
 <?php endfor;?>
 <td colspan="2"><center>Licor</center></td>
 <td rowspan="4">Hora</td>
 <?php for ($i=0;$i<$numerotricanter;$i++):?>
 <td colspan="4"><center><?php print $i+1; ?></center></td>
 <?php endfor;?>
 <td rowspan="4">Hora</td>
 <?php for ($i=0;$i<$numeropulidora;$i++):?>
 <td colspan="4"><center><?php print $i+1; ?></center></td>
 <?php endfor;?>
 <td rowspan="4">Hora</td>
 <?php for ($i=0;$i<$numeroplantas;$i++):?>
 <td colspan="8"><center><?php print $i+1; ?></center></td>
 <?php endfor;?>
 
 </tr>
 <!--Fila # 3-->
 <tr>
 <?php for ($i=0;$i<$numerococina;$i++):?>
 <td rowspan="2"><center>RPM</center></td><td colspan="2"><center>P. PSI</center></td><td rowspan="2"><center>T°C</center></td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numeroprensas;$i++):?>
 <td rowspan="2"><center>Amp</center></td><td rowspan="2"><center>%H</center></td>
 <?php endfor;?>
 <td rowspan="2"><center>%S</center></td><td rowspan="2"><center>%G</center></td>
 <?php for ($i=0;$i<$numerotricanter;$i++):?>
 <td colspan="2"><center>Alim</center></td><td colspan="2"><center>A cola</center></td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numeropulidora;$i++):?>
 <td ><center>Alim</center></td><td colspan="3"><center>Aceite</center></td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numeroplantas;$i++):?>
 <td colspan="2" ><center>A.Cola</center></td><td colspan="1" ><center>VAH</center></td><td colspan="1" ><center>Presión</center></td><td colspan="1" ><center>1°E</center></td><td colspan="1" ><center>2°E</center></td><td colspan="1" ><center>3°E</center></td><td colspan="1" ><center>CON</center></td>
 <?php endfor;?>
 </tr>
 <!--Fila # 4-->
 <tr>
 <?php for ($i=0;$i<$numerococina;$i++):?>
 <td ><center>Eje</center></td><td ><center>Chaq</center></td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numerotricanter;$i++):?>
 <td ><center>T°C</center></td><td ><center>%H</center></td><td ><center>%S</center></td><td ><center>%G</center></td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numeropulidora;$i++):?>
 <td ><center>T°C</center></td><td ><center>%H</center></td><td ><center>%S</center></td><td ><center>%A</center></td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numeroplantas;$i++):?>
 <td><center>T°C</center></td><td ><center>%S</center></td><td><center>T°C</center></td><td><center>p°</center></td><td><center>T°C</center></td><td ><center>__</center></td><td><center>T°C</center></td><td><center>%S</center></td>
 <?php endfor;?>
 </tr>
  <!--Fila # 5-->
 <tr >
<?php foreach($cocinas as $pa):?>
     <td style="font-size: 7px;"><center><?php print (float)$pa->CoccionMinRmp.'_'.(float)$pa->CoccionMaxRmp?></center></td><td style="font-size: 7px;"><center><?php print (float)$pa->CoccionMinPresionEje.' _ '.(float)$pa->CoccionMaxPresionEje?></center></td><td style="font-size: 7px;"><center><?php print (float)$pa->CoccionMinPresionChaqueta.' _ '.(float)$pa->CoccionMaxPresionChaqueta?></center></td><td style="font-size: 7px;"><center><?php print (float)$pa->CoccionMinTemperatura.' _ '.(float)$pa->CoccionMaxTemperatura?></center></td>
<?php endforeach;?>
 <?php foreach($prensas as $pre):?>
 <td style="font-size: 7px;"><center><?php print (float)$pre->PrensadoMinAmperaje.' _ '.(float)$pre->PrensadoMaxAmperaje?></center></td><td style="font-size: 7px;"><center><?php print (float)$pre->PrensadoMinPorcentajeHumedad.' _ '.(float)$pre->PrensadoMaxPorcentajeHumedad?></center></td>
<?php endforeach;?>
 <td style="font-size: 7px;"><center><?php print (float)$lic['LicorMinPorcentajeSolidos'].' _ '.(float)$lic['LicorMaxPorcentajeSolidos']?></center></td><td style="font-size: 7px;"><center><?php print (float)$lic['LicorMinPorcentajeGrasas'].' _'.(float)$lic['LicorMaxPorcentajeGrasas']?></center></td>
 <?php foreach($tricanter as $tri):?>
 <td style="font-size: 7px;"><center><?php print (float)$tri->DecantacionMinAlimentacionTemperatura.' _ '.(float)$tri->DecantacionMaxAlimentacionTemperatura?></center></td><td style="font-size: 7px;"><center><?php print (float)$tri->DecantacionMinAlimentacionPorcentajeHumedad.' _ '.(float)$tri->DecantacionMaxAlimentacionPorcentajeHumedad?></center></td><td style="font-size: 7px;"><center><?php print (float)$tri->DecantacionMinAguaColaPorcentajeSolidos.' _ '.(float)$tri->DecantacionMaxAguaColaPorcentajeSolidos?></center></td><td style="font-size: 7px;"><center><?php print (float)$tri->DecantacionMinAguaColaPorcentajeGrasas.' _ '.(float)$tri->DecantacionMaxAguaColaPorcentajeGrasas ?></center></td>
<?php endforeach;?>
 <?php foreach($pulidora as $pul):?>
 <td style="font-size: 7px;"><center><?php print (float)$pul->RefinacionMinAlimentacionTemperatura.' _ '.(float)$pul->RefinacionMaxAlimentacionTemperatura?></center></td><td style="font-size: 7px;" ><center><?php print (float)$pul->RefinacionMinAceitePorcentajeHumedad.'_'.(float)$pul->RefinacionMaxAceitePorcentajeHumedad?></center></td><td style="font-size: 7px;"><center><?php print (float)$pul->RefinacionMinAceitePorcentajeSolidos.' _ '.(float)$pul->RefinacionMaxAceitePorcentajeSolidos?></center></td><td style="font-size: 7px;"><center><?php print (float)$pul->RefinacionMinAceitePorcentajeAcidez.' _'.(float)$pul->RefinacionMaxAceitePorcentajeAcidez ?></center></td>
<?php endforeach;?>
 <?php foreach($planta as $pla):?>
 <td style="font-size: 7px;"><center><?php print (float)$pla->TratamientoMinAlimentacionAguaColaTemperatura.' _ '.(float)$pla->TratamientoMaxAlimentacionAguaColaTemperatura?></center></td><td style="font-size: 7px;"><center><?php print (float)$pla->TratamientoMinAlimentacionAguaColaPorcentajeSolidos.' _ '.(float)$pla->TratamientoMaxAlimentacionAguaColaPorcentajeSolidos?></center></td><td style="font-size: 7px;"><center><?php print (float)$pla->TratamientoMinVahTemperatura.' _ '.(float)$pla->TratamientoMaxVahTemperatura?></center></td><td style="font-size: 7px;"><center><?php print (float)$pla->TratamientoMinVacPeso.' _ '.(float)$pla->TratamientoMaxVacPeso?></center></td>
 <td style="font-size: 7px;"><center><?php print (float)$pla->TratamientoMinTemperatura1.' _ '.(float)$pla->TratamientoMaxTemperatura1?></center></td><td style="font-size: 7px;"><center><?php print (float)$pla->TratamientoMinTemperatura2.' _ '.(float)$pla->TratamientoMaxTemperatura2?></center></td><td style="font-size: 7px;"><center><?php print (float)$pla->TratamientoMinTemperatura3.' _ '.(float)$pla->TratamientoMaxTemperatura3?></center></td><td style="font-size: 7px;"><center><?php print (float)$pla->TratamientoMinSalidaPorcentajeSolidos.' _ '.(float)$pla->TratamientoMaxSalidaPorcentajeSolidos?></center></td>
<?php endforeach;?>
  
 </tr>
 </thead>
 <tbody>
     
 <!-- ITEMS -->
 <?php 
 $control =0;
 $countcocina=0;
 $countprensa=0;
 $countlicor=0;
 $countdecan=0;
 $countrefinado=0;
 $counttrata=0;
 $countrotadisk=0;
 $countsecado=0;
 $countharina=0;
 $fin = $limit >= $PAGINADO ? $PAGINADO : $limit;
 for($i=0;$i<$fin;$i++):
    $rowRecepcion = $model[$i] ? $model[$i] : NULL; ?>
    <tr>
    
        <td align="center"><?php echo $rowRecepcion->Hora? substr($rowRecepcion->Hora,0,5) : '-' ;?></td>
    <td align="center"><?php echo $rowRecepcion->Secuencia ? $rowRecepcion->Secuencia : '-';?></td>
    <td align="center"><?php print $rowRecepcion->TolvaID?$rowRecepcion->tolva->Nombre: '-';?> 
    </td><td align="center"><?php print $rowRecepcion->EspecieID ? $rowRecepcion->EspecieID : '-';?> 
    </td><td align="center"><?php print $rowRecepcion->TBVN ? $rowRecepcion->TBVN : '-';?> 
</td><td align="center"><?php print $rowRecepcion->PesoTonelada ? (float)$rowRecepcion->PesoTonelada : '-';?>
</td><td align="center"><?php print $rowRecepcion->Descuento ? (float)$rowRecepcion->Descuento : '-';?>
    </td><td align="center"><?php print $rowRecepcion->Pesoneto ? (float)$rowRecepcion->Pesoneto : '-';?></td>
 <!--Coccion -->
   <?php $x=0;foreach($cocinas as $c):
    $coccion =$coccions[$countcocina] ? $coccions[$countcocina] : NULL;
   $countcocina++;
    if ($x++<=0):?>
    <td align="center" ><?php echo $coccion->Hora ? substr($coccion->Hora,0,5) : '-';?></td>
    <?php endif;?>
    <td align="center" <?php print $this->cla($coccion->RPM, $c->CoccionMinRmp,$c->CoccionMaxRmp, $c->CoccionRpmAlertaNaranja)?>><?php echo $coccion->RPM ? (float)$coccion->RPM : '-';?></td>
    <td align="center" <?php print $this->cla($coccion->PresionEje, $c->CoccionMinPresionEje,$c->CoccionMaxPresionEje, $c->CoccionPresionEjeAlertaNaranja)?> ><?php print $coccion->PresionEje ? (float)$coccion->PresionEje : '-';?> </td>
    <td align="center" <?php print $this->cla($coccion->PresionChaqueta, $c->CoccionMinPresionChaqueta,$c->CoccionMaxPresionChaqueta, $c->CoccionPresionChaquetaAlertaNaranja)?>><?php print $coccion->PresionChaqueta ? (float)$coccion->PresionChaqueta : '-';?> </td>
    <td align="center"  <?php print $this->cla($coccion->Temperatura, $c->CoccionMinTemperatura,$c->CoccionMaxTemperatura, $c->CoccionPresionTemperaturaAlertaNaranja)?> ><?php print $coccion->Temperatura ? (float)$coccion->Temperatura : '-' ;?> </td>
    <?php endforeach; ?>
 
    <!--Prensado -->
    <?php $x =0;
    foreach ($prensas as $pn):
        $prensado = $prensados[$countprensa]? $prensados[$countprensa] : NULL;
        $countprensa++;
        if ($x++<=0): ?>
        <td align="center" ><?php echo $prensado->Hora ? substr($prensado->Hora,0,5) : '-';?></td>
        <?php endif;?>
        <td align="center" <?php print $this->cla($prensado->Amperaje, $pn->PrensadoMinAmperaje,$pn->PrensadoMaxAmperaje, $pn->PrensadoAmperajeAlertaNaranja)?>><?php echo $prensado->Amperaje ? (float)$prensado->Amperaje : '-';?></td>
        <td align="center" <?php print $this->cla($prensado->PorcentajeHumedad, $pn->PrensadoMinPorcentajeHumedad,$pn->PrensadoMaxPorcentajeHumedad, $pn->PrensadoPorcentajeHumedadAlertaNaranja)?>><?php print $prensado->PorcentajeHumedad ? (float)$prensado->PorcentajeHumedad : '-';?> </td>
    <?php endforeach;
    $licor = $licors[$countlicor]? $licors[$countlicor] :NULL; $countlicor++?>
    <!-- Licor -->
    <td align="center" <?php print $this->cla($licor->LicorPorcentajeSolidos, $p->LicorMinPorcentajeSolidos,$p->LicorMaxPorcentajeSolidos, $p->LicorPorcentajeSolidosAlertaNaranja)?>><?php echo $licor->LicorPorcentajeSolidos ? (float)$licor->LicorPorcentajeSolidos : '-';?></td>
    <td align="center" <?php print $this->cla($licor->LicorPorcentajeGrasas, $p->LicorMinPorcentajeGrasas,$p->LicorMaxPorcentajeGrasas, $p->LicorPorcentajeGrasasAlertaNaranja)?>><?php print $licor->LicorPorcentajeGrasas ? (float)$licor->LicorPorcentajeGrasas : '-';?> </td>
    <!--Decantasion -->
    <?php $x = 0;
    foreach ($tricanter as $tc):
        $decantasion =$decantasions[$countdecan] ? $decantasions[$countdecan] : NULL;$countdecan++;
        if ($x++<=0): ?>
        <td align="center" ><?php echo $decantasion->Hora ? substr($decantasion->Hora,0,5) :'-';?></td>
        <?php endif;?>
        <td align="center" <?php print $this->cla($decantasion->AlimentacionTemperatura, $tc->DecantacionMinAlimentacionTemperatura,$tc->DecantacionMaxAlimentacionTemperatura, $tc->DecantacionAlimentacionTemperaturaAlertaNaranja)?>><?php echo $decantasion->AlimentacionTemperatura? (float)$decantasion->AlimentacionTemperatura : '-';?></td>
        <td align="center" <?php print $this->cla($decantasion->AlimentacionPorcentajeHumedad, $tc->DecantacionMinAlimentacionPorcentajeHumedad,$tc->DecantacionMaxAlimentacionPorcentajeHumedad, $tc->DecantacionAlimentacionPorcentajeHumedadAlertaNaranja);?>><?php print $decantasion->AlimentacionPorcentajeHumedad ? (float)$decantasion->AlimentacionPorcentajeHumedad : '-';?> </td>
        <td align="center" <?php print $this->cla($decantasion->AguaColaPorcentajeSolidos, $tc->DecantacionMinAguaColaPorcentajeSolidos,$tc->DecantacionMaxAguaColaPorcentajeSolidos, $tc->DecantacionAguaColaPorcentajeSolidosAlertaNaranja)?>><?php print $decantasion->AguaColaPorcentajeSolidos ? (float)$decantasion->AguaColaPorcentajeSolidos :'-';?> </td>
        <td align="center" <?php print $this->cla($decantasion->AguaColaPorcentajeGrasas, $tc->DecantacionMinAguaColaPorcentajeGrasas,$tc->DecantacionMaxAguaColaPorcentajeGrasas, $tc->DecantacionAguaColaPorcentajeGrasasAlertaNaranja)?>><?php print $decantasion->AguaColaPorcentajeGrasas ? (float)$decantasion->AguaColaPorcentajeGrasas : '-';?> </td>
    <?php endforeach; ?>
    <!--Refinacion -->
    <?php $x = 0;
    foreach($pulidora as $pl):
        $refinado= $refinados[$countrefinado]?$refinados[$countrefinado]:NULL;$countrefinado++;
        if ($x++<=0): ?>
        <td align="center" ><?php echo $refinado->Hora ? substr($refinado->Hora,0,5) : '-';?></td>
        <?php endif;?>
        <td align="center" <?php print $this->cla($refinado->AlimentacionTemperatura, $pl->RefinacionMinAlimentacionTemperatura,$pl->RefinacionMaxAlimentacionTemperatura, $pl->RefinacionTemperaturaAlertaNaranja)?>><?php echo $refinado->AlimentacionTemperatura? (float)$refinado->AlimentacionTemperatura : '-';?></td>
        <td align="center" <?php print $this->cla($refinado->AceitePorcentajeHumedad, $pl->RefinacionMinAceitePorcentajeHumedad,$pl->RefinacionMaxAceitePorcentajeHumedad, $pl->RefinacionAceitePorcentajeHumedadAlertaNaranja)?>><?php print $refinado->AceitePorcentajeHumedad ? (float)$refinado->AceitePorcentajeHumedad : '-';?> </td>
        <td align="center" <?php print $this->cla($refinado->AceitePorcentajeSolidos, $pl->RefinacionMinAceitePorcentajeSolidos,$pl->RefinacionMaxAceitePorcentajeSolidos, $pl->RefinacionAceitePorcentajeSolidosAlertaNaranja)?>><?php print $refinado->AceitePorcentajeSolidos ? (float)$refinado->AceitePorcentajeSolidos : '-';?> </td>
        <td align="center" <?php print $this->cla($refinado->AceitePorcentajeAcidez, $pl->RefinacionMinAceitePorcentajeAcidez,$pl->RefinacionMaxAceitePorcentajeAcidez, $pl->RefinacionAceitePorcentajeAcidezAlertaNaranja)?>><?php print $refinado->AceitePorcentajeAcidez ? (float)$refinado->AceitePorcentajeAcidez : '-';?> </td>
    <?php endforeach; ?>
    <!--Tratamiento -->
    <?php $x=0;
    foreach($planta as $pt):
        $tratamiento = $tratamientos[$counttrata]?$tratamientos[$counttrata]:NULL;$counttrata++;
        if ($x++<=0): ?>
        <td align="center" ><?php echo $tratamiento->Hora ? substr($tratamiento->Hora,0,5) : '-';?></td>
        <?php endif;?>
        <td align="center"  <?php print $this->cla($tratamiento->AlimentacionAguaColaTemperatura, $pt->TratamientoMinAlimentacionAguaColaTemperatura,$pt->TratamientoMaxAlimentacionAguaColaTemperatura, $pt->TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja)?>><?php echo $tratamiento->AlimentacionAguaColaTemperatura ? (float)$tratamiento->AlimentacionAguaColaTemperatura :'-';?></td>
        <td align="center" <?php print $this->cla($tratamiento->AlimentacionAguaColaPorcentajeSolidos, $pt->TratamientoMinAlimentacionAguaColaPorcentajeSolidos,$pt->TratamientoMaxAlimentacionAguaColaPorcentajeSolidos, $pt->TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja)?>><?php print $tratamiento->AlimentacionAguaColaPorcentajeSolidos ? (float)$tratamiento->AlimentacionAguaColaPorcentajeSolidos : '-';?> </td>
        <td align="center" <?php print $this->cla($tratamiento->VahTemperatura, $pt->TratamientoMinVahTemperatura,$pt->TratamientoMaxVahTemperatura, $pt->TratamientoVahTemperaturaAlertaNaranja)?>><?php print $tratamiento->VahTemperatura ? (float)$tratamiento->VahTemperatura :  '-';?> </td>
        <td align="center" <?php print $this->cla($tratamiento->VacPeso, $pt->TratamientoMinVacPeso,$pt->TratamientoMaxVacPeso, $pt->TratamientoVacPesoAlertaNaranja)?>><?php print $tratamiento->VacPeso ? (float)$tratamiento->VacPeso : '-';?> </td>
        <td align="center" <?php print $this->cla($tratamiento->Temperatura1, $pt->TratamientoMinTemperatura1,$pt->TratamientoMaxTemperatura1, $pt->TratamientoTemperatura1AlertaNaranja)?>><?php echo $tratamiento->Temperatura1 ? (float)$tratamiento->Temperatura1 : '-';?></td>
        <td align="center" <?php print $this->cla($tratamiento->Temperatura2, $pt->TratamientoMinTemperatura2,$pt->TratamientoMaxTemperatura2, $pt->TratamientoTemperatura2AlertaNaranja)?>><?php print $tratamiento->Temperatura2 ? (float)$tratamiento->Temperatura2 : '-';?> </td>
        <td align="center" <?php print $this->cla($tratamiento->Temperatura3, $pt->TratamientoMinTemperatura3,$pt->TratamientoMaxTemperatura3, $pt->TratamientoTemperatura3AlertaNaranja)?>><?php print $tratamiento->Temperatura3 ? (float)$tratamiento->Temperatura3 : '-';?> </td>
        <td align="center" <?php print $this->cla($tratamiento->SalidaPorcentajeSolidos, $pt->TratamientoMinSalidaPorcentajeSolidos,$pt->TratamientoMaxSalidaPorcentajeSolidos, $pt->TratamientoSalidaPorcentajeSolidosAlertaNaranja)?> ><?php print $tratamiento->SalidaPorcentajeSolidos ? (float)$tratamiento->SalidaPorcentajeSolidos : '-';?> </td>
    <?php endforeach; ?>
    </tr>
 <?php endfor; ?>
 <!-- FIN ITEMS -->
 </tbody>
 </table>
<!--TABLA 2-->


 <table class="items" width="100%" style="font-size: 7pt; margin-top: 5px; border-collapse: collapse;" cellpadding="5">
 <thead>
 <tr>
 <!--Pestaña recepcion-->
 <td colspan="<?php print ($numerorotadisk*7)+1?>"><center>Presecado</center></td><td colspan="<?php print ($numerorotatubo*4)+1;?>">Secado</td> <td colspan="10"><center>Producto Terminado</center></td><td rowspan="5">Observaciones</td>
 </tr>
 <!--Pestaña Presecado-->
 <tr>
 <td rowspan="4">Hora</td>
 <?php for ($i=0;$i<$numerorotadisk;$i++):?>
 <td colspan="2">ALIM</td>
 <td colspan="5"><center><?php print $i+1; ?></center></td>
 <?php endfor;?>
 <td rowspan="4">Hora</td>
 <?php for ($i=0;$i<$numerorotatubo;$i++):?>
 <td colspan="4"><center><?php print $i+1; ?></center></td>
 <?php endfor;?>
<td rowspan="4">Hora</td><td rowspan="4">Temp</td><td rowspan="4">%H</td><td rowspan="4">A/O</td><td rowspan="4">Peso</td><td rowspan="4">TBVN</td><td rowspan="4">Prot</td><td rowspan="4"> Lote</td><td rowspan="4">Sacos</td><td rowspan="4">Clasif</td>

 </tr>
 <!--Fila # 3-->
 <tr>
 <?php for ($i=0;$i<$numerorotadisk;$i++):?>
 <td >Min</td><td >Max</td>
 <td rowspan="2"><center>Amp</center></td><td colspan="2"><center>P. PSI</center></td><td rowspan="2"><center>Temperatura</center></td><td rowspan="2"><center>%H</center></td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numerorotatubo;$i++):?>
 <td rowspan="2"><center>Amp</center></td><td rowspan="2"><center>PresiónPSI</center></td><td rowspan="2"><center>Temperatura</center></td><td rowspan="2"><center>%H</center></td>
 <?php endfor;?>

 </tr>
 <!--Fila # 4-->
 <tr>
 <?php for ($i=0;$i<$numerorotadisk;$i++):?>
 <td>%H</td><td>Presión</td>
 <td ><center>Eje</center></td><td ><center>Chaq</center></td>
 <?php endfor;?>

 </tr>
 <!--Fila # 5-->
 <tr>
 <?php foreach($rotadisk as $rot):?>
<td style="font-size: 7px;"><center><?php print (float)$rot->PresecadoMinMinimoPorcentajeHumedad.' _ '.(float)$rot->PresecadoMaxMinimoPorcentajeHumedad?></center></td>
 <td style="font-size: 7px;"><center><?php print (float)$rot->PresecadoMinMaximoPresion.' _ '.(float)$rot->PresecadoMaxMaximoPresion?></center></td> <!-- Col.Ali -->
<td style="font-size: 7px;"><center><?php print (float)$rot->PresecadoMinAmperaje.' _ '.(float)$rot->PresecadoMaxAmperaje?></center></td>
 <td style="font-size: 7px;"><center><?php print (float)$rot->PresecadoMinPresionEje.' _ '.(float)$rot->PresecadoMaxPresionEje?></center></td>
 <td style="font-size: 7px;"><center><?php print (float)$rot->PresecadoMinPresionChaqueta.' _ '.(float)$rot->PresecadoMaxPresionChaqueta?></center></td>
 <td style="font-size: 7px;"><center><?php print (float)$rot->PresecadoMinSCR.' _ '.(float)$rot->PresecadoMaxSCR?></center></td>
 <td style="font-size: 7px;"><center><?php print (float)$rot->PresecadoMinPorcentajeHumedad.' _ '.(float)$rot->PresecadoMaxPorcentajeHumedad?></center></td>
<?php endforeach;?>
 <?php foreach($rotatubo as $rotb):?>
 <td style="font-size: 7px;"><center><?php print (float)$rotb->RotatuboMinAmperaje.' _ '.(float)$rotb->RotatuboMaxAmperaje?></center></td>
 <td style="font-size: 7px;"><center><?php print (float)$rotb->RotatuboMinPresionEje.' _ '.(float)$rotb->RotatuboMaxPresionEje?></center></td>
 <td style="font-size: 7px;"><center><?php print (float)$rotb->RotatuboMinSRC.' _ '.(float)$rotb->RotatuboMaxSRC?></center></td>
 <td style="font-size: 7px;"><center><?php print (float)$rotb->RotatuboMinHumedad.' _ '.(float)$rotb->RotatuboMaxHumedad?></center></td>
<?php endforeach;?>
 </tr>
 </thead>
 <tbody>
 <!-- ITEMS -->
 <?php  for($i=0;$i<$fin;$i++): ?>
    <tr>
 <!--Presecado -->
    <?php 
    $x=0;
    foreach ($rotadisk as $rt):
        $presecado = $presecados[$countrotadisk]? $presecados[$countrotadisk] : NULL ;$countrotadisk++;
        if ($x++<=0): ?>
        <td align="center" ><?php echo $presecado->Hora? substr($presecado->Hora,0,5) : '-';?></td>
        <?php endif;?>
        <td align="center" <?php print $this->cla($presecado->MinimoPorcentajeHumedad, $rt->PresecadoMinMinimoPorcentajeHumedad,$rt->PresecadoMaxMinimoPorcentajeHumedad, $rt->PresecadoMinimoPorcentajeHumedadAlertaNaranja)?>><?php echo $presecado->MinimoPorcentajeHumedad ?  (float)$presecado->MinimoPorcentajeHumedad : '-';?></td>
        <td align="center" <?php print $this->cla($presecado->MaximoPresion, $rt->PresecadoMinMaximoPresion,$rt->PresecadoMaxMaximoPresion, $rt->PresecadoMaximoPresionAlertaNaranja)?>><?php print $presecado->MaximoPresion ? (float)$presecado->MaximoPresion : '-';?> </td>
        <td align="center" <?php print $this->cla($presecado->Amperaje, $rt->PresecadoMinAmperaje,$rt->PresecadoMaxAmperaje, $rt->PresecadoAmperajeAlertaNaranja)?>><?php echo $presecado->Amperaje ? (float)$presecado->Amperaje : '-';?></td>
        <td align="center" <?php print $this->cla($presecado->PresionEje, $rt->PresecadoMinPresionEje,$rt->PresecadoMaxPresionEje, $rt->PresecadoPresionEjeAlertaNaranja)?>><?php print $presecado->PresionEje ? (float)$presecado->PresionEje : '-';?> </td>
        <td align="center" <?php print $this->cla($presecado->PresionChaqueta, $rt->PresecadoMinPresionChaqueta,$rt->PresecadoMaxPresionChaqueta, $rt->PresecadoPresionChaquetaAlertaNaranja)?>><?php print $presecado->PresionChaqueta ? (float)$presecado->PresionChaqueta : '-';?> </td>
        <td align="center" <?php print $this->cla($presecado->SCR, $rt->PresecadoMinSCR,$rt->PresecadoMaxSCR, $rt->PresecadoSCRAlertaNaranja)?>><?php print $presecado->SCR ? (float)$presecado->SCR : '-';?> </td>
        <td align="center" <?php print $this->cla($presecado->PorcentajeHumedad, $rt->PresecadoMinPorcentajeHumedad,$rt->PresecadoMaxPorcentajeHumedad, $rt->PresecadoPorcentajeHumedadAlertaNaranja)?>><?php print $presecado->PorcentajeHumedad ? (float)$presecado->PorcentajeHumedad : '-';?> </td>
    <?php endforeach; ?>
    <!--Secado-->
    <?php $x=0;
    foreach($rotatubo as $rtb):
        $row = $secados[$countsecado] ? $secados[$countsecado] : NULL;$countsecado++;
        if ($x++<=0): ?>
        <td align="center" ><?php echo $row->Hora ? substr($row->Hora,0,5) : '-';?></td>
        <?php endif;?>
        <td align="center" <?php print $this->cla($row->Amperaje, $rtb->RotatuboMinAmperaje,$rtb->RotatuboMaxAmperaje, $rtb->RotatuboAmperajeAlertaNaranja)?>><?php echo $row->Amperaje? (float)$row->Amperaje : '-';?></td>
        <td align="center" <?php print $this->cla($row->PresionEje, $rtb->RotatuboMinPresionEje,$rtb->RotatuboMaxPresionEje, $rtb->RotatuboPresionEjeAlertaNaranja)?>><?php print $row->PresionEje ? (float)$row->PresionEje : '-';?> </td>
        <td align="center" <?php print $this->cla($row->SRC, $rtb->RotatuboMinSRC,$rtb->RotatuboMaxSRC, $rtb->RotatuboSRCAlertaNaranja)?>><?php print $row->SRC ? (float)$row->SRC : '-';?> </td>
        <td align="center" <?php print $this->cla($row->PorcentajeHumedad, $rtb->RotatuboMinHumedad,$rtb->RotatuboMaxHumedad, $rtb->RotatuboHumedadAlertaNaranja)?>><?php print $row->PorcentajeHumedad ? (float)$row->PorcentajeHumedad : '-'; ?> </td>
    <?php endforeach; ?>
    <!--ProductoFinal -->
    <?php $har = $harina[$countharina]? $harina[$countharina] : NULL; $countharina++; ?>
    <td align="center"><?php print $har->Hora ? substr($har->Hora,0,5) : '-';?> </td>
    <td align="center" <?php print $this->cla($har->Temperatura, $rtb->RotatuboMinSRC,$rtb->RotatuboMaxSRC, $rtb->RotatuboSRCAlertaNaranja)?> ><?php echo $har->Temperatura ? (float)$har->Temperatura : '-';?></td>
    <td align="center" <?php print $this->cla($har->PorcentajeHumedad, $rtb->RotatuboMinSRC,$rtb->RotatuboMaxSRC, $rtb->RotatuboSRCAlertaNaranja)?> ><?php print $har->PorcentajeHumedad? (float)$har->PorcentajeHumedad : '-';?> </td>
    <td align="center"><?php print $har->AO ? (float)$har->AO : '-';?> </td>
    <td align="center"><?php print $har->Peso? (float)$har->Peso:'-';?> </td>
    <td align="center"><?php echo $har->TBVN ? (float)$har->TBVN : '-';?></td>
    <td align="center"><?php print $har->PorcentajeProteina ? (float)$har->PorcentajeProteina : '-';?> </td>
    <td align="center"><?php print $har->NumeroLote ? $har->NumeroLote : '-';?> </td>
    <td align="center"><?php print $har->SacoProducidos ? (float)$har->SacoProducidos : '-';?> </td>
    <td align="center"><?php print $har->ClasificacionID ? $har->clasi->Nombre:'-';?> </td>
    <td align="center"><?php print $har->ObservacionID ? $har->obser->Descripcion:'-';?> </td>
    </tr>
 <?php endfor; ?>
 <!-- FIN ITEMS -->
 </tbody>
 </table>

 </body>
 </html>
