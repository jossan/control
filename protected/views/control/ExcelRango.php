<html>
<head>
<style>
 body {font-family: sans-serif;
 font-size: 7pt;
 }
 p { margin: 0pt;
 }
 td { vertical-align: top; }
 .items td {
 border-left: 0.1mm solid #000000;
 border-right: 0.1mm solid #000000;
 }
 table thead td { background-color: #EEEEEE;
 text-align: center;
 border: 0.1mm solid #000000;
 }
 .items tr {
 background-color: #FFFFFF;
 border: 0mm none #000000;
 border-bottom: 0.08mm solid gray;
 }
 .items td.totals {
 text-align: right;
 border: 0.1mm solid #000000;
 }
 .pnormal{
    background-color: #FFFFFF !important ;
}
.pnaranja{
    background-color: #FFC683 !important ;
}
.projo{
    background-color: #FC8D8D !important ;
}
</style>
</head>
<body>
    <h3><center>CONTROL DE PROCEDO DE PRODUCCIÓN</center></h3>
    <br>
<?php while($fechainicio<=$fechafinal):?>
<?php
$fechaini = $fechainicio.' 00:00';
$fechafin = $fechafinal.' 23:59';
$r = Yii::app()->db->createCommand("CALL valormayor ('".$fechainicio."','".$fechainicio."')")->queryRow();
$limit = $r['mayor'];
$model = Recepcion::model()->findAll("concat(Fecha,' ',Hora) between '".$fechaini."' and '".$fechafin."' order by Fecha, Hora, ID");
$cri = new CDbCriteria();
$cri->select = "*";
$cri->addBetweenCondition("concat(Fecha,' ',Hora)", $fechaini, $fechafin);
$cri->order = 'Fecha, Hora, codigofila, ID';
$crili= new CDbCriteria();
$crili->select = "distinct(t.codigofila), t.*";
$crili->join = "join Prensado on t.codigofila = Prensado.codigofila";
$crili->addBetweenCondition("concat(Fecha,' ',Hora)", $fechaini, $fechafin);
$crili->order = 'Fecha, Hora, ID';
$licors = Licor::model()->findAll($crili);
$coccions = Coccion::model()->findAll($cri);
$prensados = Prensado::model()->findAll($cri);
$decantasions = Decantacion::model()->findAll($cri);
$refinados = Refinacion::model()->findAll($cri);
$tratamientos= Tratamiento::model()->findAll($cri);
$presecados = Presecado::model()->findAll($cri);
$secados = Secado::model()->findAll($cri);
$cri = new CDbCriteria();
$cri->select = "*";
$cri->addBetweenCondition("concat(Fecha,' ',Hora)", $fechaini, $fechafin);
$cri->order = 'Fecha, Hora, ID';
$harina = Harina::model()->findAll($cri);
$sql = "SELECT LicorMinPorcentajeSolidos, LicorMaxPorcentajeSolidos, LicorPorcentajeSolidosAlertaNaranja, LicorMinPorcentajeGrasas, LicorMaxPorcentajeGrasas, LicorPorcentajeGrasasAlertaNaranja from Parametros LIMIT 1";
$lic = Yii::app()->db->createCommand($sql)->queryRow();
$dias = array('','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo');
?>
    <label>Fecha: <strong><?php print $dias[date('N', strtotime($fechainicio))].', '.$fechainicio;?></strong></label>
 <table class="items" width="100%" style="font-size: 7pt; border-collapse: collapse;" cellpadding="5">
 <thead>
 <tr>
 <!--Pestaña recepcion-->
 <td colspan="8">Recepción</td>
 <td colspan="<?php print ($numerococina*4)+1?>"><center>Cocción</center></td>
 <td colspan="<?php print($numeroprensas*2)+3;?>">Prensado</td>
 <td colspan="<?php print ($numerotricanter*4)+1;?>">Decantación</td> 
 <td colspan="<?php print ($numeropulidora*4)+1;?>">Refinación</td>
 <td colspan="<?php print ($numeroplantas*8)+1;?>">Concentración</td>
 <td colspan="<?php print ($numerorotadisk*7)+1?>"><center>Presecado</center></td>
 <td colspan="<?php print ($numerorotatubo*4)+1;?>">Secado</td> 
 <td colspan="10"><center>Producto Terminado</center></td><td rowspan="5">Observaciones</td>
 </tr>
 
 <!--Pestaña Coccion-->
 <tr>
 <td rowspan="4">HORA</td>
 <td rowspan="4">SECUENCIA</td>
 <td rowspan="4">TOLVA</td>
 <td rowspan="4">Especie</td>
 <td rowspan="4">TBVN</td>
 <td rowspan="4">Peso(T)</td>
 <td rowspan="4">Desc(T)</td>
 <td rowspan="4">Peso Neto(T)</td>
 <td rowspan="4">Hora</td>
 <?php for ($i=0;$i<$numerococina;$i++):?>
 <td colspan="4"><center><?php print $i+1; ?></center></td>
 <?php endfor;?>
 <td rowspan="4">Hora</td>
 <?php for ($i=0;$i<$numeroprensas;$i++):?>
 <td colspan="2"><center><?php print $i+1; ?></center></td>
 <?php endfor;?>
 <td colspan="2"><center>Licor</center></td>
 <td rowspan="4">Hora</td>
 <?php for ($i=0;$i<$numerotricanter;$i++):?>
 <td colspan="4"><center><?php print $i+1; ?></center></td>
 <?php endfor;?>
 <td rowspan="4">Hora</td>
 <?php for ($i=0;$i<$numeropulidora;$i++):?>
 <td colspan="4"><center><?php print $i+1; ?></center></td>
 <?php endfor;?>
 <td rowspan="4">Hora</td>
 <?php for ($i=0;$i<$numeroplantas;$i++):?>
 <td colspan="8"><center><?php print $i+1; ?></center></td>
 <?php endfor;?>
 <td rowspan="4">Hora</td>
 <?php for ($i=0;$i<$numerorotadisk;$i++):?>
 <td colspan="2">ALIM</td>
 <td colspan="5"><center><?php print $i+1; ?></center></td>
 <?php endfor;?>
 <td rowspan="4">Hora</td>
 <?php for ($i=0;$i<$numerorotatubo;$i++):?>
 <td colspan="4"><center><?php print $i+1; ?></center></td>
 <?php endfor;?>
 <td rowspan="4">Hora</td><td rowspan="4">Temp</td><td rowspan="4">%H</td><td rowspan="4">A/O</td><td rowspan="4">Peso</td><td rowspan="4">TBVN</td><td rowspan="4">Prot</td><td rowspan="4"> Lote</td><td rowspan="4">Sacos</td><td rowspan="4">Clasif</td>

 </tr>
 <!--Fila # 3-->
 <tr>
 <?php for ($i=0;$i<$numerococina;$i++):?>
 <td rowspan="2"><center>RPM</center></td><td colspan="2"><center>P. PSI</center></td><td rowspan="2"><center>T°C</center></td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numeroprensas;$i++):?>
 <td rowspan="2"><center>Amp</center></td><td rowspan="2"><center>%H</center></td>
 <?php endfor;?>
 <td rowspan="2"><center>%S</center></td><td rowspan="2"><center>%G</center></td>
 <?php for ($i=0;$i<$numerotricanter;$i++):?>
 <td colspan="2"><center>Alim</center></td><td colspan="2"><center>A cola</center></td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numeropulidora;$i++):?>
 <td ><center>Alim</center></td><td colspan="3"><center>Aceite</center></td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numeroplantas;$i++):?>
 <td colspan="2" ><center>A.Cola</center></td><td colspan="1" ><center>VAH</center></td><td colspan="1" ><center>Presión</center></td><td colspan="1" ><center>1°E</center></td><td colspan="1" ><center>2°E</center></td><td colspan="1" ><center>3°E</center></td><td colspan="1" ><center>CON</center></td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numerorotadisk;$i++):?>
 <td >Min</td><td >Max</td>
 <td rowspan="2"><center>Amp</center></td><td colspan="2"><center>P. PSI</center></td><td rowspan="2"><center>Temperatura</center></td><td rowspan="2"><center>%H</center></td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numerorotatubo;$i++):?>
 <td rowspan="2"><center>Amp</center></td><td rowspan="2"><center>PresiónPSI</center></td><td rowspan="2"><center>Temperatura</center></td><td rowspan="2"><center>%H</center></td>
 <?php endfor;?>
 </tr>
 <!--Fila # 4-->
 <tr>
 <?php for ($i=0;$i<$numerococina;$i++):?>
 <td ><center>Eje</center></td><td ><center>Chaq</center></td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numerotricanter;$i++):?>
 <td ><center>T°C</center></td><td ><center>%H</center></td><td ><center>%S</center></td><td ><center>%G</center></td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numeropulidora;$i++):?>
 <td ><center>T°C</center></td><td ><center>%H</center></td><td ><center>%S</center></td><td ><center>%A</center></td>
 <?php endfor;?>
 <?php for ($i=0;$i<$numeroplantas;$i++):?>
 <td><center>T°C</center></td><td ><center>%S</center></td><td><center>T°C</center></td><td><center>p°</center></td><td><center>T°C</center></td><td ><center>__</center></td><td><center>T°C</center></td><td><center>%S</center></td>
 <?php endfor;?>
  <?php for ($i=0;$i<$numerorotadisk;$i++):?>
 <td>%H</td><td>Presión</td>
 <td ><center>Eje</center></td><td ><center>Chaq</center></td>
 <?php endfor;?>
 </tr>
  <!--Fila # 5-->
 <tr >
<?php foreach($cocinas as $pa):?>
     <td style="font-size: 7px;"><center><?php print (float)$pa->CoccionMinRmp.'_'.(float)$pa->CoccionMaxRmp?></center></td><td style="font-size: 7px;"><center><?php print (float)$pa->CoccionMinPresionEje.' _ '.(float)$pa->CoccionMaxPresionEje?></center></td><td style="font-size: 7px;"><center><?php print (float)$pa->CoccionMinPresionChaqueta.' _ '.(float)$pa->CoccionMaxPresionChaqueta?></center></td><td style="font-size: 7px;"><center><?php print (float)$pa->CoccionMinTemperatura.' _ '.(float)$pa->CoccionMaxTemperatura?></center></td>
<?php endforeach;?>
 <?php foreach($prensas as $pre):?>
 <td style="font-size: 7px;"><center><?php print (float)$pre->PrensadoMinAmperaje.' _ '.(float)$pre->PrensadoMaxAmperaje?></center></td><td style="font-size: 7px;"><center><?php print (float)$pre->PrensadoMinPorcentajeHumedad.' _ '.(float)$pre->PrensadoMaxPorcentajeHumedad?></center></td>
<?php endforeach;?>
 <td style="font-size: 7px;"><center><?php print (float)$lic['LicorMinPorcentajeSolidos'].' _ '.(float)$lic['LicorMaxPorcentajeSolidos']?></center></td><td style="font-size: 7px;"><center><?php print (float)$lic['LicorMinPorcentajeGrasas'].' _'.(float)$lic['LicorMaxPorcentajeGrasas']?></center></td>
 <?php foreach($tricanter as $tri):?>
 <td style="font-size: 7px;"><center><?php print (float)$tri->DecantacionMinAlimentacionTemperatura.' _ '.(float)$tri->DecantacionMaxAlimentacionTemperatura?></center></td><td style="font-size: 7px;"><center><?php print (float)$tri->DecantacionMinAlimentacionPorcentajeHumedad.' _ '.(float)$tri->DecantacionMaxAlimentacionPorcentajeHumedad?></center></td><td style="font-size: 7px;"><center><?php print (float)$tri->DecantacionMinAguaColaPorcentajeSolidos.' _ '.(float)$tri->DecantacionMaxAguaColaPorcentajeSolidos?></center></td><td style="font-size: 7px;"><center><?php print (float)$tri->DecantacionMinAguaColaPorcentajeGrasas.' _ '.(float)$tri->DecantacionMaxAguaColaPorcentajeGrasas ?></center></td>
<?php endforeach;?>
 <?php foreach($pulidora as $pul):?>
 <td style="font-size: 7px;"><center><?php print (float)$pul->RefinacionMinAlimentacionTemperatura.' _ '.(float)$pul->RefinacionMaxAlimentacionTemperatura?></center></td><td style="font-size: 7px;" ><center><?php print (float)$pul->RefinacionMinAceitePorcentajeHumedad.'_'.(float)$pul->RefinacionMaxAceitePorcentajeHumedad?></center></td><td style="font-size: 7px;"><center><?php print (float)$pul->RefinacionMinAceitePorcentajeSolidos.' _ '.(float)$pul->RefinacionMaxAceitePorcentajeSolidos?></center></td><td style="font-size: 7px;"><center><?php print (float)$pul->RefinacionMinAceitePorcentajeAcidez.' _'.(float)$pul->RefinacionMaxAceitePorcentajeAcidez ?></center></td>
<?php endforeach;?>
 <?php foreach($planta as $pla):?>
 <td style="font-size: 7px;"><center><?php print (float)$pla->TratamientoMinAlimentacionAguaColaTemperatura.' _ '.(float)$pla->TratamientoMaxAlimentacionAguaColaTemperatura?></center></td><td style="font-size: 7px;"><center><?php print (float)$pla->TratamientoMinAlimentacionAguaColaPorcentajeSolidos.' _ '.(float)$pla->TratamientoMaxAlimentacionAguaColaPorcentajeSolidos?></center></td><td style="font-size: 7px;"><center><?php print (float)$pla->TratamientoMinVahTemperatura.' _ '.(float)$pla->TratamientoMaxVahTemperatura?></center></td><td style="font-size: 7px;"><center><?php print (float)$pla->TratamientoMinVacPeso.' _ '.(float)$pla->TratamientoMaxVacPeso?></center></td>
 <td style="font-size: 7px;"><center><?php print (float)$pla->TratamientoMinTemperatura1.' _ '.(float)$pla->TratamientoMaxTemperatura1?></center></td><td style="font-size: 7px;"><center><?php print (float)$pla->TratamientoMinTemperatura2.' _ '.(float)$pla->TratamientoMaxTemperatura2?></center></td><td style="font-size: 7px;"><center><?php print (float)$pla->TratamientoMinTemperatura3.' _ '.(float)$pla->TratamientoMaxTemperatura3?></center></td><td style="font-size: 7px;"><center><?php print (float)$pla->TratamientoMinSalidaPorcentajeSolidos.' _ '.(float)$pla->TratamientoMaxSalidaPorcentajeSolidos?></center></td>
<?php endforeach;?>
  <?php foreach($rotadisk as $rot):?>
<td style="font-size: 7px;"><center><?php print (float)$rot->PresecadoMinMinimoPorcentajeHumedad.' _ '.(float)$rot->PresecadoMaxMinimoPorcentajeHumedad?></center></td>
 <td style="font-size: 7px;"><center><?php print (float)$rot->PresecadoMinMaximoPresion.' _ '.(float)$rot->PresecadoMaxMaximoPresion?></center></td> <!-- Col.Ali -->
<td style="font-size: 7px;"><center><?php print (float)$rot->PresecadoMinAmperaje.' _ '.(float)$rot->PresecadoMaxAmperaje?></center></td>
 <td style="font-size: 7px;"><center><?php print (float)$rot->PresecadoMinPresionEje.' _ '.(float)$rot->PresecadoMaxPresionEje?></center></td>
 <td style="font-size: 7px;"><center><?php print (float)$rot->PresecadoMinPresionChaqueta.' _ '.(float)$rot->PresecadoMaxPresionChaqueta?></center></td>
 <td style="font-size: 7px;"><center><?php print (float)$rot->PresecadoMinSCR.' _ '.(float)$rot->PresecadoMaxSCR?></center></td>
 <td style="font-size: 7px;"><center><?php print (float)$rot->PresecadoMinPorcentajeHumedad.' _ '.(float)$rot->PresecadoMaxPorcentajeHumedad?></center></td>
<?php endforeach;?>
 <?php foreach($rotatubo as $rotb):?>
 <td style="font-size: 7px;"><center><?php print (float)$rotb->RotatuboMinAmperaje.' _ '.(float)$rotb->RotatuboMaxAmperaje?></center></td>
 <td style="font-size: 7px;"><center><?php print (float)$rotb->RotatuboMinPresionEje.' _ '.(float)$rotb->RotatuboMaxPresionEje?></center></td>
 <td style="font-size: 7px;"><center><?php print (float)$rotb->RotatuboMinSRC.' _ '.(float)$rotb->RotatuboMaxSRC?></center></td>
 <td style="font-size: 7px;"><center><?php print (float)$rotb->RotatuboMinHumedad.' _ '.(float)$rotb->RotatuboMaxHumedad?></center></td>
<?php endforeach;?>
 </tr>
 </thead>
 <tbody>
     
 <!-- ITEMS -->
 <?php 
 $control =0;
 $countcocina=0;
 $countprensa=0;
 $countlicor=0;
 $countdecan=0;
 $countrefinado=0;
 $counttrata=0;
 $countrotadisk=0;
 $countsecado=0;
 $countharina=0;
 $fin = $limit;
 for($i=0;$i<$fin;$i++):
    $rowRecepcion = isset($model[$i]) ? $model[$i] : NULL; ?>
    <tr>
    <td align="center"><?php echo isset($rowRecepcion->Hora)? substr($rowRecepcion->Hora,0,5) : '-' ;?></td>
    <td align="center"><?php echo isset($rowRecepcion->Secuencia) ? $rowRecepcion->Secuencia : '-';?></td>
    <td align="center"><?php print isset($rowRecepcion->TolvaID) ?$rowRecepcion->tolva->Nombre: '-';?> 
    </td><td align="center"><?php print isset($rowRecepcion->EspecieID) ? $rowRecepcion->EspecieID : '-';?> 
    </td><td align="center"><?php print isset($rowRecepcion->TBVN) ? $rowRecepcion->TBVN : '-';?> 
</td><td align="center"><?php print isset($rowRecepcion->PesoTonelada) ? (float)$rowRecepcion->PesoTonelada : '-';?>
</td><td align="center"><?php print isset($rowRecepcion->Descuento) ? (float)$rowRecepcion->Descuento : '-';?>
    </td><td align="center"><?php print isset($rowRecepcion->Pesoneto) ? (float)$rowRecepcion->Pesoneto : '-';?></td>
 <!--Coccion -->
   <?php $x=0;foreach($cocinas as $c):
    $coccion = isset($coccions[$countcocina]) ? $coccions[$countcocina] : NULL;
   $countcocina++;
    if ($x++<=0):?>
    <td align="center" ><?php echo isset($coccion->Hora) ? substr($coccion->Hora,0,5) : '-';?></td>
    <?php endif;?>
    <td align="center" <?php print $this->cla($coccion->RPM, $c->CoccionMinRmp,$c->CoccionMaxRmp, $c->CoccionRpmAlertaNaranja)?>><?php echo isset($coccion->RPM) ? (float)$coccion->RPM : '-';?></td>
    <td align="center" <?php print $this->cla($coccion->PresionEje, $c->CoccionMinPresionEje,$c->CoccionMaxPresionEje, $c->CoccionPresionEjeAlertaNaranja)?> ><?php print isset($coccion->PresionEje) ? (float)$coccion->PresionEje : '-';?> </td>
    <td align="center" <?php print $this->cla($coccion->PresionChaqueta, $c->CoccionMinPresionChaqueta,$c->CoccionMaxPresionChaqueta, $c->CoccionPresionChaquetaAlertaNaranja)?>><?php print isset($coccion->PresionChaqueta) ? (float)$coccion->PresionChaqueta : '-';?> </td>
    <td align="center"  <?php print $this->cla($coccion->Temperatura, $c->CoccionMinTemperatura,$c->CoccionMaxTemperatura, $c->CoccionPresionTemperaturaAlertaNaranja)?> ><?php print $coccion->Temperatura ? (float)$coccion->Temperatura : '-' ;?> </td>
    <?php endforeach; ?>
 
    <!--Prensado -->
    <?php $x =0;
    foreach ($prensas as $pn):
        $prensado = isset($prensados[$countprensa])? $prensados[$countprensa] : NULL;
        $countprensa++;
        if ($x++<=0): ?>
        <td align="center" ><?php echo isset($prensado->Hora) ? substr($prensado->Hora,0,5) : '-';?></td>
        <?php endif;?>
        <td align="center" <?php print $this->cla( isset($prensado->Amperaje)? $prensado->Amperaje:0, $pn->PrensadoMinAmperaje,$pn->PrensadoMaxAmperaje, $pn->PrensadoAmperajeAlertaNaranja)?>><?php echo isset($prensado->Amperaje) ? (float)$prensado->Amperaje : '-';?></td>
        <td align="center" <?php print $this->cla(isset($prensado->PorcentajeHumedad) ? $prensado->PorcentajeHumedad : 0 , $pn->PrensadoMinPorcentajeHumedad,$pn->PrensadoMaxPorcentajeHumedad, $pn->PrensadoPorcentajeHumedadAlertaNaranja)?>><?php print isset($prensado->PorcentajeHumedad) ? (float)$prensado->PorcentajeHumedad : '-';?> </td>
    <?php endforeach;
    $licor = isset($licors[$countlicor])? $licors[$countlicor] :NULL; $countlicor++?>
    <!-- Licor -->
    <td align="center" <?php print $this->cla(isset($licor->LicorPorcentajeSolidos) ? $licor->LicorPorcentajeSolidos:0, $p->LicorMinPorcentajeSolidos,$p->LicorMaxPorcentajeSolidos, $p->LicorPorcentajeSolidosAlertaNaranja)?>><?php echo isset($licor->LicorPorcentajeSolidos) ? (float)$licor->LicorPorcentajeSolidos : '-';?></td>
    <td align="center" <?php print $this->cla(isset($licor->LicorPorcentajeGrasas)? $licor->LicorPorcentajeGrasas : 0, $p->LicorMinPorcentajeGrasas,$p->LicorMaxPorcentajeGrasas, $p->LicorPorcentajeGrasasAlertaNaranja)?>><?php print isset($licor->LicorPorcentajeGrasas) ? (float)$licor->LicorPorcentajeGrasas : '-';?> </td>
    <!--Decantasion -->
    <?php $x = 0;
    foreach ($tricanter as $tc):
        $decantasion = isset($decantasions[$countdecan]) ? $decantasions[$countdecan] : NULL;$countdecan++;
        if ($x++<=0): ?>
        <td align="center" ><?php echo isset($decantasion->Hora) ? substr($decantasion->Hora,0,5) :'-';?></td>
        <?php endif;?>
        <td align="center" <?php print $this->cla(isset($decantasion->AlimentacionTemperatura) ? $decantasion->AlimentacionTemperatura : 0 , $tc->DecantacionMinAlimentacionTemperatura,$tc->DecantacionMaxAlimentacionTemperatura, $tc->DecantacionAlimentacionTemperaturaAlertaNaranja)?>><?php echo isset($decantasion->AlimentacionTemperatura) ? (float)$decantasion->AlimentacionTemperatura : '-';?></td>
        <td align="center" <?php print $this->cla(isset($decantasion->AlimentacionPorcentajeHumedad) ? $decantasion->AlimentacionPorcentajeHumedad : 0 , $tc->DecantacionMinAlimentacionPorcentajeHumedad,$tc->DecantacionMaxAlimentacionPorcentajeHumedad, $tc->DecantacionAlimentacionPorcentajeHumedadAlertaNaranja);?>><?php print isset($decantasion->AlimentacionPorcentajeHumedad) ? (float)$decantasion->AlimentacionPorcentajeHumedad : '-';?> </td>
        <td align="center" <?php print $this->cla(isset($decantasion->AguaColaPorcentajeSolidos) ? $decantasion->AguaColaPorcentajeSolidos :0, $tc->DecantacionMinAguaColaPorcentajeSolidos,$tc->DecantacionMaxAguaColaPorcentajeSolidos, $tc->DecantacionAguaColaPorcentajeSolidosAlertaNaranja)?>><?php print isset($decantasion->AguaColaPorcentajeSolidos) ? (float)$decantasion->AguaColaPorcentajeSolidos :'-';?> </td>
        <td align="center" <?php print $this->cla(isset($decantasion->AguaColaPorcentajeGrasas) ? $decantasion->AguaColaPorcentajeGrasas : 0, $tc->DecantacionMinAguaColaPorcentajeGrasas,$tc->DecantacionMaxAguaColaPorcentajeGrasas, $tc->DecantacionAguaColaPorcentajeGrasasAlertaNaranja)?>><?php print isset($decantasion->AguaColaPorcentajeGrasas) ? (float)$decantasion->AguaColaPorcentajeGrasas : '-';?> </td>
    <?php endforeach; ?>
    <!--Refinacion -->
    <?php $x = 0;
    foreach($pulidora as $pl):
        $refinado= isset($refinados[$countrefinado])?$refinados[$countrefinado]:NULL;$countrefinado++;
        if ($x++<=0): ?>
        <td align="center" ><?php echo isset($refinado->Hora) ? substr($refinado->Hora,0,5) : '-';?></td>
        <?php endif;?>
        <td align="center" <?php print $this->cla(isset($refinado->AlimentacionTemperatura) ? $refinado->AlimentacionTemperatura : 0, $pl->RefinacionMinAlimentacionTemperatura,$pl->RefinacionMaxAlimentacionTemperatura, $pl->RefinacionTemperaturaAlertaNaranja)?>><?php echo isset($refinado->AlimentacionTemperatura)? (float)$refinado->AlimentacionTemperatura : '-';?></td>
        <td align="center" <?php print $this->cla(isset($refinado->AceitePorcentajeHumedad) ? $refinado->AceitePorcentajeHumedad :0, $pl->RefinacionMinAceitePorcentajeHumedad,$pl->RefinacionMaxAceitePorcentajeHumedad, $pl->RefinacionAceitePorcentajeHumedadAlertaNaranja)?>><?php print isset($refinado->AceitePorcentajeHumedad) ? (float)$refinado->AceitePorcentajeHumedad : '-';?> </td>
        <td align="center" <?php print $this->cla(isset($refinado->AceitePorcentajeSolidos) ? $refinado->AceitePorcentajeSolidos : 0, $pl->RefinacionMinAceitePorcentajeSolidos,$pl->RefinacionMaxAceitePorcentajeSolidos, $pl->RefinacionAceitePorcentajeSolidosAlertaNaranja)?>><?php print isset($refinado->AceitePorcentajeSolidos) ? (float)$refinado->AceitePorcentajeSolidos : '-';?> </td>
        <td align="center" <?php print $this->cla(isset($refinado->AceitePorcentajeAcidez) ? $refinado->AceitePorcentajeAcidez : 0, $pl->RefinacionMinAceitePorcentajeAcidez,$pl->RefinacionMaxAceitePorcentajeAcidez, $pl->RefinacionAceitePorcentajeAcidezAlertaNaranja)?>><?php print isset($refinado->AceitePorcentajeAcidez) ? (float)$refinado->AceitePorcentajeAcidez : '-';?> </td>
    <?php endforeach; ?>
    <!--Tratamiento -->
    <?php $x=0;
    foreach($planta as $pt):
        $tratamiento = isset($tratamientos[$counttrata]) ? $tratamientos[$counttrata]:NULL;$counttrata++;
        if ($x++<=0): ?>
        <td align="center" ><?php echo isset($tratamiento->Hora) ? substr($tratamiento->Hora,0,5) : '-';?></td>
        <?php endif;?>
        <td align="center"  <?php print $this->cla(isset($tratamiento->AlimentacionAguaColaTemperatura) ? $tratamiento->AlimentacionAguaColaTemperatura: 0, $pt->TratamientoMinAlimentacionAguaColaTemperatura,$pt->TratamientoMaxAlimentacionAguaColaTemperatura, $pt->TratamientoAlimentacionAguaColaTemperaturazAlertaNaranja)?>><?php echo isset($tratamiento->AlimentacionAguaColaTemperatura) ? (float)$tratamiento->AlimentacionAguaColaTemperatura :'-';?></td>
        <td align="center" <?php print $this->cla(isset($tratamiento->AlimentacionAguaColaPorcentajeSolidos) ? $tratamiento->AlimentacionAguaColaPorcentajeSolidos : 0, $pt->TratamientoMinAlimentacionAguaColaPorcentajeSolidos,$pt->TratamientoMaxAlimentacionAguaColaPorcentajeSolidos, $pt->TratamientoAlimentacionAguaColaPorcentajeSolidosAlertaNaranja)?>><?php print isset($tratamiento->AlimentacionAguaColaPorcentajeSolidos) ? (float)$tratamiento->AlimentacionAguaColaPorcentajeSolidos : '-';?> </td>
        <td align="center" <?php print $this->cla(isset($tratamiento->VahTemperatura) ? $tratamiento->VahTemperatura : 0, $pt->TratamientoMinVahTemperatura,$pt->TratamientoMaxVahTemperatura, $pt->TratamientoVahTemperaturaAlertaNaranja)?>><?php print isset($tratamiento->VahTemperatura) ? (float)$tratamiento->VahTemperatura :  '-';?> </td>
        <td align="center" <?php print $this->cla(isset($tratamiento->VacPeso) ? $tratamiento->VacPeso :0, $pt->TratamientoMinVacPeso,$pt->TratamientoMaxVacPeso, $pt->TratamientoVacPesoAlertaNaranja)?>><?php print isset($tratamiento->VacPeso) ? (float)$tratamiento->VacPeso : '-';?> </td>
        <td align="center" <?php print $this->cla(isset($tratamiento->Temperatura1) ? $tratamiento->Temperatura1 : 0, $pt->TratamientoMinTemperatura1,$pt->TratamientoMaxTemperatura1, $pt->TratamientoTemperatura1AlertaNaranja)?>><?php echo isset($tratamiento->Temperatura1) ? (float)$tratamiento->Temperatura1 : '-';?></td>
        <td align="center" <?php print $this->cla(isset($tratamiento->Temperatura2) ?$tratamiento->Temperatura2 :0, $pt->TratamientoMinTemperatura2,$pt->TratamientoMaxTemperatura2, $pt->TratamientoTemperatura2AlertaNaranja)?>><?php print isset($tratamiento->Temperatura2) ? (float)$tratamiento->Temperatura2 : '-';?> </td>
        <td align="center" <?php print $this->cla(isset($tratamiento->Temperatura3) ? $tratamiento->Temperatura3 :0, $pt->TratamientoMinTemperatura3,$pt->TratamientoMaxTemperatura3, $pt->TratamientoTemperatura3AlertaNaranja)?>><?php print isset($tratamiento->Temperatura3) ? (float)$tratamiento->Temperatura3 : '-';?> </td>
        <td align="center" <?php print $this->cla(isset($tratamiento->SalidaPorcentajeSolidos) ? $tratamiento->SalidaPorcentajeSolidos :0, $pt->TratamientoMinSalidaPorcentajeSolidos,$pt->TratamientoMaxSalidaPorcentajeSolidos, $pt->TratamientoSalidaPorcentajeSolidosAlertaNaranja)?> ><?php print isset($tratamiento->SalidaPorcentajeSolidos) ? (float)$tratamiento->SalidaPorcentajeSolidos : '-';?> </td>
    <?php endforeach; ?>
 <!--Presecado -->
    <?php 
    $x=0;
    foreach ($rotadisk as $rt):
        $presecado = isset($presecados[$countrotadisk])? $presecados[$countrotadisk] : NULL ;$countrotadisk++;
        if ($x++<=0): ?>
        <td align="center" ><?php echo isset($presecado->Hora)? substr($presecado->Hora,0,5) : '-';?></td>
        <?php endif;?>
        <td align="center" <?php print $this->cla(isset($presecado->MinimoPorcentajeHumedad)? $presecado->MinimoPorcentajeHumedad : 0, $rt->PresecadoMinMinimoPorcentajeHumedad,$rt->PresecadoMaxMinimoPorcentajeHumedad, $rt->PresecadoMinimoPorcentajeHumedadAlertaNaranja)?>><?php echo isset($presecado->MinimoPorcentajeHumedad) ?  (float)$presecado->MinimoPorcentajeHumedad : '-';?></td>
        <td align="center" <?php print $this->cla(isset($presecado->MaximoPresion) ? $presecado->MaximoPresion : 0, $rt->PresecadoMinMaximoPresion,$rt->PresecadoMaxMaximoPresion, $rt->PresecadoMaximoPresionAlertaNaranja)?>><?php print isset($presecado->MaximoPresion) ? (float)$presecado->MaximoPresion : '-';?> </td>
        <td align="center" <?php print $this->cla(isset($presecado->Amperaje) ? $presecado->Amperaje :0, $rt->PresecadoMinAmperaje,$rt->PresecadoMaxAmperaje, $rt->PresecadoAmperajeAlertaNaranja)?>><?php echo isset($presecado->Amperaje) ? (float)$presecado->Amperaje : '-';?></td>
        <td align="center" <?php print $this->cla(isset($presecado->PresionEje) ? $presecado->PresionEje :0, $rt->PresecadoMinPresionEje,$rt->PresecadoMaxPresionEje, $rt->PresecadoPresionEjeAlertaNaranja)?>><?php print isset($presecado->PresionEje) ? (float)$presecado->PresionEje : '-';?> </td>
        <td align="center" <?php print $this->cla(isset($presecado->PresionChaqueta) ? $presecado->PresionChaqueta :0, $rt->PresecadoMinPresionChaqueta,$rt->PresecadoMaxPresionChaqueta, $rt->PresecadoPresionChaquetaAlertaNaranja)?>><?php print isset($presecado->PresionChaqueta) ? (float)$presecado->PresionChaqueta : '-';?> </td>
        <td align="center" <?php print $this->cla(isset($presecado->SCR) ? $presecado->SCR :0, $rt->PresecadoMinSCR,$rt->PresecadoMaxSCR, $rt->PresecadoSCRAlertaNaranja)?>><?php print isset($presecado->SCR) ? (float)$presecado->SCR : '-';?> </td>
        <td align="center" <?php print $this->cla(isset($presecado->PorcentajeHumedad) ? $presecado->PorcentajeHumedad :0, $rt->PresecadoMinPorcentajeHumedad,$rt->PresecadoMaxPorcentajeHumedad, $rt->PresecadoPorcentajeHumedadAlertaNaranja)?>><?php print isset($presecado->PorcentajeHumedad) ? (float)$presecado->PorcentajeHumedad : '-';?> </td>
    <?php endforeach; ?>
    <!--Secado-->
    <?php $x=0;
    foreach($rotatubo as $rtb):
        $row = isset($secados[$countsecado]) ? $secados[$countsecado] : NULL;$countsecado++;
        if ($x++<=0): ?>
        <td align="center" ><?php echo isset($row->Hora) ? substr($row->Hora,0,5) : '-';?></td>
        <?php endif;?>
        <td align="center" <?php print $this->cla(isset($row->Amperaje) ? $row->Amperaje : 0, $rtb->RotatuboMinAmperaje,$rtb->RotatuboMaxAmperaje, $rtb->RotatuboAmperajeAlertaNaranja)?>><?php echo isset($row->Amperaje) ? (float)$row->Amperaje : '-';?></td>
        <td align="center" <?php print $this->cla(isset($row->PresionEje) ? $row->PresionEje :0, $rtb->RotatuboMinPresionEje,$rtb->RotatuboMaxPresionEje, $rtb->RotatuboPresionEjeAlertaNaranja)?>><?php print isset($row->PresionEje) ? (float)$row->PresionEje : '-';?> </td>
        <td align="center" <?php print $this->cla(isset($row->SRC) ? $row->SRC :0, $rtb->RotatuboMinSRC,$rtb->RotatuboMaxSRC, $rtb->RotatuboSRCAlertaNaranja)?>><?php print isset($row->SRC) ? (float)$row->SRC : '-';?> </td>
        <td align="center" <?php print $this->cla(isset($row->PorcentajeHumedad) ? $row->PorcentajeHumedad :0, $rtb->RotatuboMinHumedad,$rtb->RotatuboMaxHumedad, $rtb->RotatuboHumedadAlertaNaranja)?>><?php print isset($row->PorcentajeHumedad) ? (float)$row->PorcentajeHumedad : '-'; ?> </td>
    <?php endforeach; ?>
    <!--ProductoFinal -->
    <?php $har = isset($harina[$countharina])? $harina[$countharina] : NULL; $countharina++; ?>
    <td align="center"><?php print isset($har->Hora) ? substr($har->Hora,0,5) : '-';?> </td>
    <td align="center" <?php print $this->cla(isset($har->Temperatura) ? $har->Temperatura : 0 , $rtb->RotatuboMinSRC,$rtb->RotatuboMaxSRC, $rtb->RotatuboSRCAlertaNaranja)?> ><?php echo isset($har->Temperatura) ? (float)$har->Temperatura : '-';?></td>
    <td align="center" <?php print $this->cla(isset($har->PorcentajeHumedad) ? $har->PorcentajeHumedad :0, $rtb->RotatuboMinSRC,$rtb->RotatuboMaxSRC, $rtb->RotatuboSRCAlertaNaranja)?> ><?php print isset($har->PorcentajeHumedad) ? (float)$har->PorcentajeHumedad : '-';?> </td>
    <td align="center"><?php print isset($har->AO) ? (float)$har->AO : '-';?> </td>
    <td align="center"><?php print isset($har->Peso)? (float)$har->Peso:'-';?> </td>
    <td align="center"><?php echo isset($har->TBVN) ? $har->TBVN : '-';?></td>
    <td align="center"><?php print isset($har->PorcentajeProteina) ? (float)$har->PorcentajeProteina : '-';?> </td>
    <td align="center"><?php print isset($har->NumeroLote) ? $har->NumeroLote : '-';?> </td>
    <td align="center"><?php print isset($har->SacoProducidos) ? (float)$har->SacoProducidos : '-';?> </td>
    <td align="center"><?php print isset($har->ClasificacionID)?$har->clasi->Nombre:'-';?> </td>
    <td align="center"><?php print isset($har->ObservacionID )?$har->obser->Descripcion:'-';?> </td>
    </tr>
 <?php endfor; ?>
 <!-- FIN ITEMS -->
 </tbody>
 </table>
    <br>
    <br>
    <br>
<?php  
$nuevafecha = strtotime ( '+1 day' , strtotime ( $fechainicio ) ) ;
$fechainicio = date ( 'Y-m-d' , $nuevafecha );
endwhile; ?>
 </body>
 </html>
