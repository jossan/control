<?php
$this->breadcrumbs=array(
	'Usuario Rol'=>array('index'),
	'Crear',
);

$this->menu=array(
array('label'=>'Lista de Usuario Rol','url'=>array('index')),
array('label'=>'Administrar Usuario Rol','url'=>array('admin')),
);
?>

<h2>Crear Usuario Rol</h2>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>