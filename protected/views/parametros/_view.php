<script type="text/javascript">
    function justNumbers(e)
            {
          tecla = (document.all) ? e.keyCode : e.which;
   	 if (tecla==8) return true;
         if (tecla==9) return true;
   	 patron =/\d/;
   	 te = String.fromCharCode(tecla);
   	 return patron.test(te);
            }
</script>
<div class="view">
    
        
    <?php $collapse = $this->beginWidget('booster.widgets.TbCollapse'); ?>
    <div class="panel-group" id="accordion">
    
    
    <!--Licor-->
    <div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
    <a data-toggle="collapse" data-parent="#accordion" href="#licor">
    Licor
    </a>
    </h4>
    </div>
    <div id="licor" class="panel-collapse collapse">
    <div class="panel-body">
        <form class="form-horizontal" >
            <div class="licorS">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('licor1',$data->LicorMinPorcentajeSolidos, array('maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>
            </div>
            </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('licor2',$data->LicorMaxPorcentajeSolidos, array('maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('licor3',$data->LicorPorcentajeSolidosAlertaNaranja, array('maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
                 <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'LicorS')));?>
        </div>
            <div class="licorG">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('licor5',$data->LicorMinPorcentajeGrasas, array('maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>
            </div>
            </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('licor6',$data->LicorMaxPorcentajeGrasas, array('maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('licor7',$data->LicorPorcentajeGrasasAlertaNaranja, array('maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
                          <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'LicorG')));?>
        </div>
        </form>
    </div>
    </div>
    </div>
    
    <!-- Harina -->
    <div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
    <a data-toggle="collapse" data-parent="#accordion" href="#harina">
    Harina
    </a>
    </h4>
    </div>
        <div id="harina" class="panel-collapse collapse" style="display: none;">
    <div class="panel-body">
        <form class="form-horizontal" >
            <div class="licorS">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('licor1',$data->LicorMinPorcentajeSolidos, array('maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>
            </div>
            </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('licor2',$data->LicorMaxPorcentajeSolidos, array('maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('licor3',$data->LicorPorcentajeSolidosAlertaNaranja, array('maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
        
                 <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'LicorS')));?>
        </div>
            <div class="licorG">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('licor5',$data->LicorMinPorcentajeGrasas, array('maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>
            </div>
            </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('licor6',$data->LicorMaxPorcentajeGrasas, array('maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('licor7',$data->LicorPorcentajeGrasasAlertaNaranja, array('maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>                
            </div>
        </div>
        <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'LicorG')));?>
        </div>
        </form>
    </div>
    </div>
    </div>
    
    
    
    </div>
    <?php $this->endWidget(); ?>

</div>
<script type="text/javascript">
    
$('#coccionRpm').click(function(){
    var c1 = $('#coccion1').val();
    var c2 = $('#coccion2').val();
    var c3 = $('#coccion3').val();
    var c4 = $('#coccion4').val();
    $.ajax({
            url:'index.php?r=/Parametros/coccionrpm',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#coccionPresioneje').click(function(){
    var c5 = $('#coccion5').val();
    var c6 = $('#coccion6').val();
    var c7 = $('#coccion7').val();
    var c8 = $('#coccion8').val();
    $.ajax({
            url:'index.php?r=/Parametros/coccionPresioneje',
            type:'GET',
            data: {'c5':c5,'c6':c6,'c7':c7,'c8':c8},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#coccionPresionchaq').click(function(){
    var c1 = $('#coccion9').val();
    var c2 = $('#coccion10').val();
    var c3 = $('#coccion11').val();
    var c4 = $('#coccion12').val();
    $.ajax({
            url:'index.php?r=/Parametros/coccionPresionchaq',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#coccionTemperatura').click(function(){
    var c1 = $('#coccion13').val();
    var c2 = $('#coccion14').val();
    var c3 = $('#coccion15').val();
    var c4 = $('#coccion16').val();
    $.ajax({
            url:'index.php?r=/Parametros/coccionTemperatura',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#prensaAmp').click(function(){
    var c1 = $('#prensado1').val();
    var c2 = $('#prensado2').val();
    var c3 = $('#prensado3').val();
    var c4 = $('#prensado4').val();
    $.ajax({
            url:'index.php?r=/Parametros/prensaAmp',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#prensadoH').click(function(){
    var c1 = $('#prensado5').val();
    var c2 = $('#prensado6').val();
    var c3 = $('#prensado7').val();
    var c4 = $('#prensado8').val();
    $.ajax({
            url:'index.php?r=/Parametros/prensadoH',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#LicorS').click(function(){
    var c1 = $('#licor1').val();
    var c2 = $('#licor2').val();
    var c3 = $('#licor3').val();
    var c4 = $('#licor4').val();
    $.ajax({
            url:'index.php?r=/Parametros/licorS',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#LicorG').click(function(){
    var c1 = $('#licor5').val();
    var c2 = $('#licor6').val();
    var c3 = $('#licor7').val();
    var c4 = $('#licor8').val();
    $.ajax({
            url:'index.php?r=/Parametros/licorG',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#DecantasionTem').click(function(){
    var c1 = $('#decantasion1').val();
    var c2 = $('#decantasion2').val();
    var c3 = $('#decantasion3').val();
    var c4 = $('#decantasion4').val();
    $.ajax({
            url:'index.php?r=/Parametros/decantasionTem',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#DecantasionHum').click(function(){
    var c1 = $('#decantasion5').val();
    var c2 = $('#decantasion6').val();
    var c3 = $('#decantasion7').val();
    var c4 = $('#decantasion8').val();
    $.ajax({
            url:'index.php?r=/Parametros/decantasionHum',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#DecantasionaguaS').click(function(){
    var c1 = $('#decantasion9').val();
    var c2 = $('#decantasion10').val();
    var c3 = $('#decantasion11').val();
    var c4 = $('#decantasion12').val();
    $.ajax({
            url:'index.php?r=/Parametros/decantasionaguaS',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#DecantasionaguaG').click(function(){
    var c1 = $('#decantasion13').val();
    var c2 = $('#decantasion14').val();
    var c3 = $('#decantasion15').val();
    var c4 = $('#decantasion16').val();
    $.ajax({
            url:'index.php?r=/Parametros/decantasionaguaG',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#RefinacionTem').click(function(){
    var c1 = $('#refinacion1').val();
    var c2 = $('#refinacion2').val();
    var c3 = $('#refinacion3').val();
    var c4 = $('#refinacion4').val();
    $.ajax({
            url:'index.php?r=/Parametros/refinacionTem',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#aceiteHum').click(function(){
    var c1 = $('#refinacion5').val();
    var c2 = $('#refinacion6').val();
    var c3 = $('#refinacion7').val();
    var c4 = $('#refinacion8').val();
    $.ajax({
            url:'index.php?r=/Parametros/aceiteHum',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});
$('#AceiteSol').click(function(){
    var c1 = $('#refinacion9').val();
    var c2 = $('#refinacion10').val();
    var c3 = $('#refinacion11').val();
    var c4 = $('#refinacion12').val();
    $.ajax({
            url:'index.php?r=/Parametros/aceiteSol',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});
$('#AceiteA').click(function(){
    var c1 = $('#refinacion13').val();
    var c2 = $('#refinacion14').val();
    var c3 = $('#refinacion15').val();
    var c4 = $('#refinacion16').val();
    $.ajax({
            url:'index.php?r=/Parametros/aceiteA',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#TratamientoAlimTem').click(function(){
    var c1 = $('#tratamiento1').val();
    var c2 = $('#tratamiento2').val();
    var c3 = $('#tratamiento3').val();
    var c4 = $('#tratamiento4').val();
    $.ajax({
            url:'index.php?r=/Parametros/tratamientoAlimTem',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#TratamientoAlimSol').click(function(){
    var c1 = $('#tratamiento5').val();
    var c2 = $('#tratamiento6').val();
    var c3 = $('#tratamiento7').val();
    var c4 = $('#tratamiento8').val();
    $.ajax({
            url:'index.php?r=/Parametros/tratamientoAlimSol',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#TratamientoVahTemp').click(function(){
    var c1 = $('#tratamiento9').val();
    var c2 = $('#tratamiento10').val();
    var c3 = $('#tratamiento11').val();
    var c4 = $('#tratamiento12').val();
    $.ajax({
            url:'index.php?r=/Parametros/tratamientoVahTemp',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#TratamientoVacPeso').click(function(){
    var c1 = $('#tratamiento13').val();
    var c2 = $('#tratamiento14').val();
    var c3 = $('#tratamiento15').val();
    var c4 = $('#tratamiento16').val();
    $.ajax({
            url:'index.php?r=/Parametros/tratamientoVacPeso',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#TratamientoTemperatura1').click(function(){
    var c1 = $('#tratamiento17').val();
    var c2 = $('#tratamiento18').val();
    var c3 = $('#tratamiento19').val();
    var c4 = $('#tratamiento20').val();
    $.ajax({
            url:'index.php?r=/Parametros/tratamientoTemperatura1',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#TratamientoTemperatura2').click(function(){
    var c1 = $('#tratamiento21').val();
    var c2 = $('#tratamiento22').val();
    var c3 = $('#tratamiento23').val();
    var c4 = $('#tratamiento24').val();
    $.ajax({
            url:'index.php?r=/Parametros/tratamientoTemperatura2',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#TratamientoTemperatura3').click(function(){
    var c1 = $('#tratamiento25').val();
    var c2 = $('#tratamiento26').val();
    var c3 = $('#tratamiento27').val();
    var c4 = $('#tratamiento28').val();
    $.ajax({
            url:'index.php?r=/Parametros/tratamientoTemperatura3',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#TratamientoSalSol').click(function(){
    var c1 = $('#tratamiento29').val();
    var c2 = $('#tratamiento30').val();
    var c3 = $('#tratamiento31').val();
    var c4 = $('#tratamiento32').val();
    $.ajax({
            url:'index.php?r=/Parametros/tratamientoSalSol',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#PresecadoMinHum').click(function(){
    var c1 = $('#presecado1').val();
    var c2 = $('#presecado2').val();
    var c3 = $('#presecado3').val();
    var c4 = $('#presecado4').val();
    $.ajax({
            url:'index.php?r=/Parametros/presecadoMinHum',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#PresecadoMaxPresion').click(function(){
    var c1 = $('#presecado5').val();
    var c2 = $('#presecado6').val();
    var c3 = $('#presecado7').val();
    var c4 = $('#presecado8').val();
    $.ajax({
            url:'index.php?r=/Parametros/presecadoMaxPresion',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#PresecadoAmp').click(function(){
    var c1 = $('#presecado9').val();
    var c2 = $('#presecado10').val();
    var c3 = $('#presecado11').val();
    var c4 = $('#presecado12').val();
    $.ajax({
            url:'index.php?r=/Parametros/presecadoAmp',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#PresecadoPresEje').click(function(){
    var c1 = $('#presecado13').val();
    var c2 = $('#presecado14').val();
    var c3 = $('#presecado15').val();
    var c4 = $('#presecado16').val();
    $.ajax({
            url:'index.php?r=/Parametros/presecadoPresEje',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#PresecadoPresChq').click(function(){
    var c1 = $('#presecado17').val();
    var c2 = $('#presecado18').val();
    var c3 = $('#presecado19').val();
    var c4 = $('#presecado20').val();
    $.ajax({
            url:'index.php?r=/Parametros/presecadoPresChq',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#PresecadoTempSrc').click(function(){
    var c1 = $('#presecado21').val();
    var c2 = $('#presecado22').val();
    var c3 = $('#presecado23').val();
    var c4 = $('#presecado24').val();
    $.ajax({
            url:'index.php?r=/Parametros/presecadoTempSrc',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#PresecadoHumedad').click(function(){
    var c1 = $('#presecado25').val();
    var c2 = $('#presecado26').val();
    var c3 = $('#presecado27').val();
    var c4 = $('#presecado28').val();
    $.ajax({
            url:'index.php?r=/Parametros/presecadoHumedad',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#SecadoMinHum').click(function(){
    var c1 = $('#secado1').val();
    var c2 = $('#secado2').val();
    var c3 = $('#secado3').val();
    var c4 = $('#secado4').val();
    $.ajax({
            url:'index.php?r=/Parametros/secadoMinHum',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#SecadoMaxPresion').click(function(){
    var c1 = $('#secado5').val();
    var c2 = $('#secado6').val();
    var c3 = $('#secado7').val();
    var c4 = $('#secado8').val();
    $.ajax({
            url:'index.php?r=/Parametros/secadoMaxPresion',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#SecadoAmp').click(function(){
    var c1 = $('#secado9').val();
    var c2 = $('#secado10').val();
    var c3 = $('#secado11').val();
    var c4 = $('#secado12').val();
    $.ajax({
            url:'index.php?r=/Parametros/secadoAmp',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#SecadoPresPsi').click(function(){
    var c1 = $('#secado13').val();
    var c2 = $('#secado14').val();
    var c3 = $('#secado15').val();
    var c4 = $('#secado16').val();
    $.ajax({
            url:'index.php?r=/Parametros/secadoPresPsi',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#SecadoSrc').click(function(){
    var c1 = $('#secado17').val();
    var c2 = $('#secado18').val();
    var c3 = $('#secado19').val();
    var c4 = $('#secado20').val();
    $.ajax({
            url:'index.php?r=/Parametros/secadoSrc',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

$('#SecadoHumedad').click(function(){
    var c1 = $('#secado21').val();
    var c2 = $('#secado22').val();
    var c3 = $('#secado23').val();
    var c4 = $('#secado24').val();
    $.ajax({
            url:'index.php?r=/Parametros/secadoHumedad',
            type:'GET',
            data: {'c1':c1,'c2':c2,'c3':c3,'c4':c4},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

function verificar(valor){
    /* resultado 0 -> es falso entonces significa q ocurrio un error
         * resultado 1 -> es verdadero entonces significa que si guardo */
    if(valor){
        js:$.notify("Actualizado Correctamente", "success");//el valor es 1
    }else{
        js:$.notify("Error al Actualizar", "error")//el valor es 0
    }
}

</script>
