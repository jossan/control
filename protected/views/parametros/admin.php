<?php 

?>
<fieldset>
<legend>Valor Objetivo</legend>    
<?php $collapse = $this->beginWidget('booster.widgets.TbCollapse'); ?>
    <div class="panel-group" id="accordion">
    <div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
    General
    </a>
    </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse">
    <div class="panel-body">
        <form class="form-horizontal" >
            
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Objetivo'); 
            ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('vgeneral','', array('maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>
            </div>
            </div>
        
                 <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'general')));?>
        
            
        </form>
    </div>
    </div>
    </div>
        
        
    <div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
    Anual
    </a>
    </h4>
    </div>
    <div id="collapseTwo" class="panel-collapse collapse">
    <div class="panel-body">
     <form class="form-horizontal" >
            <div class="valor">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode($anio2); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('anioa2',$valora2, array('maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>
            </div>
            
            </div>
        
                 <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'anuala2')));?>
    </div>    
              <div class="valor">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode($anio1); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('anioa1',$valora1, array('maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>
            </div>
            
            </div>
        
                 <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'anuala1')));?>
    </div>    
           <div class="valor">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode($actual); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('anioactual',$valora, array('maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>
            </div>
            
            </div>
        
                 <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'anual')));?>
    </div>    
           <div class="valor">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode($aniof1); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('aniof1',$valorf1, array('maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>
            </div>
            
            </div>
        
                 <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'anualf1')));?>
    </div>    
           <div class="valor">
        <div class="form-group" style="margin: 0px;padding: 0px;">
                        <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode($aniof2); ?>:
            </label>
            <div class="col-sm-2">
               <?php echo CHtml::textField('aniof2',$valorf2, array('maxlength'=>8,'onkeypress' => 'return justNumbers(event);')); ?>
            </div>
            </div>
        
                 <?php $this->widget('booster.widgets.TbButton',array('label' => 'Actualizar','context' => 'primary','buttonType'=>'button','htmlOptions'=>array('id'=>'anualf2')));?>
    </div>    
        </form>
    </div>
    </div>
    </div>
    
    </div>
    <?php $this->endWidget(); ?>
</fieldset>

<script type="text/javascript">
$('#anual').click(function(){
    var anioactual = $('#anioactual').val();
        $.ajax({
            url:'index.php?r=/Parametros/anual',
            type:'GET',
            data: {'anioactual':anioactual},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});   
$('#anuala1').click(function(){
     var anioa1 = $('#anioa1').val();
    
 
    $.ajax({
            url:'index.php?r=/Parametros/anuala1',
            type:'GET',
            data: {'anioa1':anioa1},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});
$('#anuala2').click(function(){
        var anioa2 = $('#anioa2').val();
 
    $.ajax({
            url:'index.php?r=/Parametros/anuala2',
            type:'GET',
            data: {'anioa2':anioa2},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});
$('#anualf1').click(function(){
    
    var aniof1 = $('#aniof1').val();
    
    $.ajax({
            url:'index.php?r=/Parametros/anualf1',
            type:'GET',
            data: {'aniof1':aniof1},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});
$('#anualf2').click(function(){
    
    var aniof2 = $('#aniof2').val();
    
 
    $.ajax({
            url:'index.php?r=/Parametros/anualf2',
            type:'GET',
            data: {'aniof2':aniof2},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});
$('#general').click(function(){
    var gene = $('#vgeneral').val();
    
 
    $.ajax({
            url:'index.php?r=/Parametros/general',
            type:'GET',
            data: {'gene':gene},
            success: function (resp) {
                verificar(resp);/*Todas la funciones llaman a verificar para saber si guardo o no*/
            },
        });
});

function verificar(valor){
    /* resultado 0 -> es falso entonces significa q ocurrio un error
         * resultado 1 -> es verdadero entonces significa que si guardo */
    if(valor){
        js:$.notify("Actualizado Correctamente", "success");//el valor es 1
        
    }else{
        js:$.notify("Error al Actualizar", "error")//el valor es 0
    }
}
</script>