<?php
$this->breadcrumbs=array(
	'Parametroses'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Parametros','url'=>array('index')),
	array('label'=>'Crear Parametros','url'=>array('create')),
	array('label'=>'Ver Parametros','url'=>array('view','id'=>$model->ID)),
	array('label'=>'Administrar Parametros','url'=>array('admin')),
	);
	?>

	<h2>Actualizar Parametros <?php echo $model->ID; ?></h2>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>