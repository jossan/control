<?php
$this->breadcrumbs=array(
	'Especies'=>array('admin'),
	$model->ID=>array('view','id'=>$model->ID),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Especies','url'=>array('admin')),
	array('label'=>'Crear Especie','url'=>array('create')),
	array('label'=>'Ver Especie','url'=>array('view','id'=>$model->ID)),
	
	);
	?>

	<h2>Actualizar Especie <?php echo $model->ID; ?></h2>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
