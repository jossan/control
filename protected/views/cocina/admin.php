<?php
$this->breadcrumbs=array(
	
	'Administración',
);

$this->menu=array(

array('label'=>'Crear Cocinador','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('cocina-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Cocinadores</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'cocina-grid',
             'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
//'filter'=>$model,
'columns'=>array(
	//	'ID',
		'Nombre',
		'Descripcion',
                array('name'=>'EstadoCocina',
                    'value'=>'$data->EstadoCocina?"Activo":"Inactivo"'
                    ),
	//	'Estado',
array(
'class'=>'booster.widgets.TbButtonColumn',

),
),
)); ?>
