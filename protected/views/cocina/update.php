<?php
$this->breadcrumbs=array(
	'Cocinadores'=>array('admin'),
	$model->ID=>array('view','id'=>$model->ID),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Cocinadores','url'=>array('admin')),
	array('label'=>'Crear Cocinador','url'=>array('create')),
	array('label'=>'Ver Cocinador','url'=>array('view','id'=>$model->ID)),
	
	);
	?>

	<h4>Actualizar Cocinador</h4>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
