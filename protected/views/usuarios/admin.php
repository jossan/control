<?php
$this->breadcrumbs=array(
	'Usuarios',
	
);

$this->menu=array(

array('label'=>'Crear Usuarios','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('usuarios-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Usuarios</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'usuarios-grid',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'ID',
		'NombreCompleto',
		'Usuario',
		
    array(
        'name'=>'Contrasenia',
        'value'=>'"*****"',
    ),
		array(
                'name'=>'TipoRolID',
                'value'=>'$data->tipoRol->NombreRol' ,
                ),
		//'Estado',
array(
'class'=>'booster.widgets.TbButtonColumn',
),
),
)); ?>
