<?php
$this->breadcrumbs=array(
	'Clientes'=>array('admin'),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Clientes','url'=>array('admin')),
	array('label'=>'Crear Clientes','url'=>array('create')),
	array('label'=>'Ver Cliente','url'=>array('view','id'=>$model->Id)),
	
	);
	?>

	<h3>Actualizar Cliente</h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>