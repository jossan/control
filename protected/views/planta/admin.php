<?php
$this->breadcrumbs=array(
	'Plantas Evaporadoras'=>array('admin'),
	'Lista',
);

$this->menu=array(

array('label'=>'Crear Planta Evaporadora','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('planta-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Plantas Evaporadoras</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'planta-grid',
    'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
//'filter'=>$model,
'columns'=>array(
		//'ID',
		'Nombre',
		'Descripcion',
                array('name'=>'EstadoPlanta',
                    'value'=>'$data->EstadoPlanta? "Activo":"Inactivo"'
                    ),
		//'Estado',
array(
'class'=>'booster.widgets.TbButtonColumn',

),
),
)); ?>
