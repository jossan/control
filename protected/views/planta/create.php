<?php
$this->breadcrumbs=array(
	'Plantas Evaporadoras'=>array('admin'),
	'Crear',
);

$this->menu=array(
array('label'=>'Lista de Plantas Evaporadoras','url'=>array('admin')),

);
?>

<h3>Crear Planta Evaporadora</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
