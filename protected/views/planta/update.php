<?php
$this->breadcrumbs=array(
	'Plantas Evaporadoras'=>array('admin'),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Plantas Evaporadoras','url'=>array('admin')),
	array('label'=>'Crear Planta Evaporadora','url'=>array('create')),
	array('label'=>'Ver Planta Evaporadora','url'=>array('view','id'=>$model->ID)),
	
	);
	?>

	<h3>Actualizar Planta Evaporadora</h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
