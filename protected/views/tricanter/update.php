<?php
$this->breadcrumbs=array(
	'Tricanter'=>array('admin'),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Tricanter','url'=>array('admin')),
	array('label'=>'Crear Tricanter','url'=>array('create')),
	array('label'=>'Ver Tricanter','url'=>array('view','id'=>$model->ID)),
	
	);
	?>

	<h3>Actualizar Tricanter</h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
