<?php
$this->breadcrumbs=array(
	'Tricanter'=>array('admin'),
	'Nuevo',
);

$this->menu=array(
array('label'=>'Lista de Tricanter','url'=>array('admin')),

);
?>

<h3>Nuevo Tricanter</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
