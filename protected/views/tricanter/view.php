<?php
$this->breadcrumbs=array(
	'Tricanter'=>array('admin'),
	$model->Nombre,
);

$this->menu=array(
array('label'=>'Lista de Tricanter','url'=>array('admin')),
array('label'=>'Crear Tricanter','url'=>array('create')),
array('label'=>'Actualizar Tricanter','url'=>array('update','id'=>$model->ID)),
array('label'=>'Borrar Tricanter','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Esta seguro que desea borrarlo?')),

);
?>

<h3>Detalles</h3>

<?php $this->widget('booster.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'ID',
		'Nombre',
		'Descripcion',
                array('name'=>'EstadoTricanter',
                    'value'=>$model->EstadoTricanter?'Activo':'Inactivo'
                    ),
		
		//'Estado',
),
)); ?>

<?php 
      $model->DecantacionMinAlimentacionTemperatura = (float)$model->DecantacionMinAlimentacionTemperatura;
      $model->DecantacionMaxAlimentacionTemperatura = (float)$model->DecantacionMaxAlimentacionTemperatura;
      $model->DecantacionAlimentacionTemperaturaAlertaNaranja = (float)$model->DecantacionAlimentacionTemperaturaAlertaNaranja;
      $model->DecantacionMinAlimentacionPorcentajeHumedad = (float)$model->DecantacionMinAlimentacionPorcentajeHumedad;
      $model->DecantacionMaxAlimentacionPorcentajeHumedad = (float)$model->DecantacionMaxAlimentacionPorcentajeHumedad;
      $model->DecantacionAlimentacionPorcentajeHumedadAlertaNaranja = (float)$model->DecantacionAlimentacionPorcentajeHumedadAlertaNaranja;
      $model->DecantacionMinAguaColaPorcentajeSolidos = (float)$model->DecantacionMinAguaColaPorcentajeSolidos;
      $model->DecantacionMaxAguaColaPorcentajeSolidos = (float)$model->DecantacionMaxAguaColaPorcentajeSolidos;
      $model->DecantacionAguaColaPorcentajeSolidosAlertaNaranja = (float)$model->DecantacionAguaColaPorcentajeSolidosAlertaNaranja;
      $model->DecantacionMinAguaColaPorcentajeGrasas = (float)$model->DecantacionMinAguaColaPorcentajeGrasas;
      $model->DecantacionMaxAguaColaPorcentajeGrasas = (float)$model->DecantacionMaxAguaColaPorcentajeGrasas;
      $model->DecantacionAguaColaPorcentajeGrasasAlertaNaranja = (float)$model->DecantacionAguaColaPorcentajeGrasasAlertaNaranja;      
?>
<div class="panel panel-default">
    <div class="panel-heading">
    <h4 class="panel-title">
    
    Parámetros
    
    </h4>
    </div>
     
    <div class="panel-body">    
        <form class="form-horizontal" >
            <div class="decantasionTem">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->DecantacionMinAlimentacionTemperatura, array('disabled'=>'disabled')); ?>
            </div>
             </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->DecantacionMaxAlimentacionTemperatura, array('disabled'=>'disabled')); ?>
        </div>
                </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->DecantacionAlimentacionTemperaturaAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
       </div>

        </div>
        <div class="decantasionHum">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
                 <?php echo CHtml::textField('',$model->DecantacionMinAlimentacionPorcentajeHumedad, array('disabled'=>'disabled')); ?>
            </div>
            </div>
        
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->DecantacionMaxAlimentacionPorcentajeHumedad, array('disabled'=>'disabled')); ?>
        </div>
            </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->DecantacionAlimentacionPorcentajeHumedadAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
</div>
                      
        </div>
        <div class="decantasionaguaS">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->DecantacionMinAguaColaPorcentajeSolidos, array('disabled'=>'disabled')); ?>
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->DecantacionMaxAguaColaPorcentajeSolidos, array('disabled'=>'disabled')); ?>
        </div>
            </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->DecantacionAguaColaPorcentajeSolidosAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
        </div>
        
             
        </div>
                <div class="decantasionaguaG">
        <div class="form-group" style="margin: 0px;padding: 0px;">
            <label class="col-sm-6 control-label required">
            <?php echo CHtml::encode('Valor Mínimo'); ?>:
            </label>
             <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->DecantacionMinAguaColaPorcentajeGrasas, array('disabled'=>'disabled')); ?>
            </div>
        </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Valor Máximo'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->DecantacionMaxAguaColaPorcentajeGrasas, array('disabled'=>'disabled')); ?>
        </div>
                    </div>
        <div class="form-group" style="margin: 0px;padding: 0px;">
             <label class="col-sm-6 control-label required" >
            <?php echo CHtml::encode('Porcentaje de Alerta Naranja'); ?>:
            </label>
            <div class="col-sm-2">
            <?php echo CHtml::textField('',$model->DecantacionAguaColaPorcentajeGrasasAlertaNaranja, array('disabled'=>'disabled')); ?>
        </div>
        </div>
        
        </div>
     </form>
    </div>
    
    </div>
