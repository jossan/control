<?php
$this->breadcrumbs=array(
	'Tricanter'=>array('admin'),
	'Lista',
);

$this->menu=array(

array('label'=>'Crear Tricanter','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('tricanter-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Tricanter</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'tricanter-grid',
    'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
//'filter'=>$model,
'columns'=>array(
		//'ID',
		'Nombre',
		'Descripcion',
                array('name'=>'EstadoTricanter',
                    'value'=>'$data->EstadoTricanter?"Activo":"Inactivo"'
                    ),
		
		//'Estado',
array(
'class'=>'booster.widgets.TbButtonColumn',
    
),
),
)); ?>
