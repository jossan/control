<?php
$this->breadcrumbs=array(
	'Lista'=>array('admin'),
	'Nueva',
);

$this->menu=array(
array('label'=>'Lista de Pulidora','url'=>array('admin')),

);
?>

<h3>Nueva Pulidora</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
