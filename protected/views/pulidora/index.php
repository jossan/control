<?php
$this->breadcrumbs=array(
	'Pulidoras',
);

$this->menu=array(
array('label'=>'Crear Pulidora','url'=>array('create')),
array('label'=>'Administrar Pulidora','url'=>array('admin')),
);
?>

<h2>Pulidoras</h2>

<?php $this->widget('booster.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
