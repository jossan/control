<?php
$this->breadcrumbs=array(
	
	'Administración',
);

$this->menu=array(

array('label'=>'Crear Pulidora','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('pulidora-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h2>Pulidoras</h2>

<?php $this->widget('booster.widgets.TbGridView',array(
'id'=>'pulidora-grid',
    'type' => 'striped bordered condensed',
'dataProvider'=>$model->search(),
//'filter'=>$model,
'columns'=>array(
		//'ID',
		'Nombre',
		'Descripcion',
    array('name'=>'EstadoPulidora',
        'value'=>'$data->EstadoPulidora ? "Activo":"Inactivo"'
        ),
	//	'Estado',
array(
'class'=>'booster.widgets.TbButtonColumn',
  
),
),
)); ?>
