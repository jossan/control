<?php
$this->breadcrumbs=array(
	'Pulidoras'=>array('admin'),
	'Actualizar',
);

	$this->menu=array(
	array('label'=>'Lista de Pulidoras','url'=>array('admin')),
	array('label'=>'Crear Pulidora','url'=>array('create')),
	array('label'=>'Ver Pulidora','url'=>array('view','id'=>$model->ID)),
	
	);
	?>

	<h3>Actualizar Pulidora</h3>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
